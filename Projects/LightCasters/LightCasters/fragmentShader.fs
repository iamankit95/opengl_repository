#version 440 core


in vec3 outNormal;
in vec3 outFragPos;
in vec2 outTexcoords;

out vec4 FragColor;

uniform vec3 uViewPos;

struct Material {
    vec3 ambient;
    //vec3 diffuse;
	sampler2D diffuse;
    //vec3 specular;
	sampler2D specular;
    float shininess;
}; 

struct Light {
    vec3 position;
	vec3 direction;
	float cutoff;
	float outerCutoff;
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;

	float constant;
    float linear;
    float quadratic;
};

uniform Material uMaterial;
uniform Light uLight1;


void main()
{
// ambient lighting calculations
vec3  ambient=  uLight1.ambient * vec3(texture(uMaterial.diffuse , outTexcoords));

// diffuse lighting calculations
vec3 norm = normalize(outNormal);
vec3 lightDir = normalize(uLight1.position - outFragPos);
float theta = dot(lightDir, normalize(-uLight1.direction));

vec3 result;
if(theta > uLight1.outerCutoff)
{
	// do lighting calculations
	float epsilon = uLight1.cutoff - uLight1.outerCutoff;
float intensity = clamp((theta - uLight1.outerCutoff) / epsilon, 0.0f, 1.0f);

float diff = max(dot(norm, lightDir), 0.0f);
vec3 diffuse = uLight1.diffuse * (diff * vec3(texture(uMaterial.diffuse , outTexcoords)));

// specular lighting calculations
vec3 viewDir = normalize(uViewPos - outFragPos);
vec3 reflectDir = reflect(-lightDir, norm);
float spec = pow(max(dot(viewDir, reflectDir), 0.0f), uMaterial.shininess);
vec3 specular = uLight1.specular * (spec * vec3(texture(uMaterial.specular , outTexcoords)));

// attenuation calculations
/*float distance = length(uLight1.position - outFragPos);
float attenuation = 1.0f / (uLight1.constant + uLight1.linear * distance + uLight1.quadratic * (distance * distance));
ambient  *= attenuation; 
diffuse  *= attenuation;
specular *= attenuation;  */

diffuse  *= intensity;
specular *= intensity;
	result = ambient + diffuse + specular;
}else
{
	// use ambient lighting here
	result = ambient;
}


FragColor =vec4(result , 1.0f);
};
