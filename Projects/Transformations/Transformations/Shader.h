#ifndef SHADER_H
#define SHADER_H


#include"Base.h"


class Shader
{
public:
	//prpgram ID
	unsigned int ID;

	// constructor declarations
	Shader(const char* vertexPath, const char* fragmentPath);

	// desctructor declarations
	~Shader();

	//
	void use();
	void unUse();

	void setBool(const std::string  &name, bool value) const;
	void setInt(const std::string  &name, int value) const;
	void setFloat(const std::string  &name, float value) const;
	void setMat4(const std::string  &name, glm::mat4 value) const;
};





#endif
