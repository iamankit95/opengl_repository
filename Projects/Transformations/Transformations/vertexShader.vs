#version 440 core

layout (location =0) in vec3 aPos;
layout (location =1) in vec3 aColor;

layout (location =2) in vec2 aTexCoord;

out vec3 outcolor;
out vec2 outtexcoord;

uniform float uOffset ;
uniform mat4 uTransformation;

void main()
{
gl_Position =uTransformation * vec4(aPos.x  , aPos.y , aPos.z , 1.0f) ;
outcolor = aColor;
outtexcoord = aTexCoord;

};
