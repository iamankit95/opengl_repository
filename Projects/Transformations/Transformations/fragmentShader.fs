#version 440 core


in vec3 outcolor;
in vec2 outtexcoord;
out vec4 FragColor;
uniform sampler2D u_sampler;
uniform sampler2D u_samplerSmiley;

void main()
{

vec4 Texture1 = texture(u_sampler , outtexcoord);
vec4 Texture2 = texture(u_samplerSmiley , outtexcoord);


FragColor =  mix(Texture1 , Texture2 , 0.1f);

};
