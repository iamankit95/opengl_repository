#ifndef BASE_H
#define BASE_H

#include<glad\glad.h>
#include<GLFW\glfw3.h>
#include<iostream>
#include<stdio.h>
#include<fstream>
#include<string>
#include<sstream>

#include<glm\glm.hpp>
#include<glm\gtc\matrix_transform.hpp>
#include<glm\gtc\type_ptr.hpp>



extern FILE * fplogfile;

class Base
{
public:
	Base(); // constructor
	~Base(); // destructor
};
#endif
