//Header Files
#include"Shader.h"
#include<stdio.h>
#define STB_IMAGE_IMPLEMENTATION
#include<stb_image.h>
//Global Variables
float vertices[] = {
	// positions          // colors           // texture coords
	0.5f,  0.5f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 1.0f,   // top right
	0.5f, -0.5f, 0.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f,   // bottom right
	-0.5f, -0.5f, 0.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f,   // bottom left
	-0.5f,  0.5f, 0.0f,   1.0f, 1.0f, 0.0f,   0.0f, 1.0f    // top left 
     
};

unsigned int indices[] = {
	0 , 1 , 3, // Frist Triangle
	1 , 2 , 3  // Second Triangle
};
unsigned int EBO;
unsigned int VAO;
unsigned int texture;
unsigned int textureSmiley;


extern FILE * fplogfile;

Shader* ourShader;
Base* baseFramework;

//Function Prototype Declaration
void framebuffer_size_callback(GLFWwindow* window, int width, int height);

void Display(void);

//
void Processinput(GLFWwindow* window);
//Entrypoint Function
int main()
{
	/////////////////////////////////////// Initialization Starts
	baseFramework = new Base(); // initializes GLFW/GLUT/OpenGL

	GLFWwindow* window = glfwCreateWindow(800, 600, "Hello Window", NULL, NULL);
	if (window == NULL)
	{
		fprintf(fplogfile, "Window is not created successfully so exiting program \n");
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	glfwSetFramebufferSizeCallback(window , framebuffer_size_callback);
	//initializing glad
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		fprintf(fplogfile, "Failed to initialize GLAD \n");
		return -1;
	}

	ourShader = new Shader("vertexShader.vs", "fragmentShader.fs");

	glViewport(0, 0, 800, 600);

	
	/////Create VAO
	glGenVertexArrays(1, &VAO);

	glBindVertexArray(VAO);

	//create VBO (Vertex Buffer Object)
	unsigned int VBO;
	glGenBuffers(1, &VBO);
	//Activate VBO now
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	//Pass Data 
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);


	//Vertex data
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	//color data
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);

	//Texture data
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
	glEnableVertexAttribArray(2);


	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Create EBO

	glGenBuffers(1, &EBO);
	//Activate VBO now
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	//Pass Data 
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	glBindVertexArray(0);

	/////////Texture
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	//load img
	int width, height, nrChannels;
	unsigned char* data = stbi_load("wall.jpg", &width, &height, &nrChannels , 0);
	if (data)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else
	{
		fprintf(fplogfile, "Could Not Load Image");

	}

	stbi_image_free(data);



	/////////////////////////Texture simley

	
	glGenTextures(1, &textureSmiley);
	glBindTexture(GL_TEXTURE_2D, textureSmiley);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	stbi_set_flip_vertically_on_load(true);

	
	data = stbi_load("awesomeface.png", &width, &height, &nrChannels, 0);
	if (data)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else
	{
		fprintf(fplogfile, "Could Not Load Image");

	}

	stbi_image_free(data);



	////////////////////////////////Initialization Ends

	//////////////////////////////Game Loop Starts
	while (!glfwWindowShouldClose(window))
	{
		Processinput(window);
		Display();
		glfwSwapBuffers(window);
		glfwPollEvents();
	}
	///////////////////////////Game Loop Ends

	delete(ourShader);
	delete(baseFramework); // it calls destructor of the class

	/////////////////////////Uninitialize End
	return 0;
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
}

void Processinput(GLFWwindow* window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(window, true);
	}
}

void Display(void)
{
	glClearColor(0.1f, 0.3f, 0.4f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	/////use program object
	ourShader->use();

	//Create transformation matrix
	glm::mat4 transformationMatrix = glm::mat4(1.0f);
	transformationMatrix = glm::translate(transformationMatrix, glm::vec3(0.5f, 0.0f, 0.0f));
	transformationMatrix = glm::rotate(transformationMatrix, (float)glfwGetTime(), glm::vec3(0.0f, 0.0f, 1.0f));
	transformationMatrix = glm::scale(transformationMatrix, glm::vec3(0.5f, 0.5f, 0.5f));
	
	//setting uniforms
	ourShader->setFloat("uOffset", 0.5f);
	ourShader->setInt("u_sampler" , 0);
	ourShader->setInt("u_samplerSmiley", 1);
	ourShader->setMat4("uTransformation", transformationMatrix);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);
	
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, textureSmiley);




	glBindVertexArray(VAO);
	//glDrawArrays(GL_TRIANGLES, 0, 3);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
	ourShader->unUse();

}