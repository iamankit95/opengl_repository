#version 440 core


out vec4 FragColor;

uniform vec3 uLightColor;
uniform vec3 uObjectColor;
void main()
{

FragColor =  vec4(uLightColor * uObjectColor,1.0f);

};
