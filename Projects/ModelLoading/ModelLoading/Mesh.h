#ifndef MESH_H
#define MESH_H

#include"Base.h"
#include"Shader.h"

struct Vertex
{
	glm::vec3 position;
	glm::vec3 normal;
	glm::vec2 texCoords;
	glm::vec3 tangent;
	glm::vec3 bitangent;
};

struct Texture
{
	unsigned int id;
	std::string path;
	std::string type;
};

class Mesh
{
public:
	unsigned int VAO;
	// mesh data
	std::vector<Vertex> vertices;
	std::vector<unsigned int> indices;
	std::vector<Texture> textures;
	/*  Functions  */
	Mesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices, std::vector<Texture> textures);
	void Draw(Shader shader);

private:
	// render data
	unsigned int VBO, EBO;
	void setUpMesh();
};
#endif // !MESH_H
