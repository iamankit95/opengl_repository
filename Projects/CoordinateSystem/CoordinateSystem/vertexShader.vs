#version 440 core

layout (location =0) in vec3 aPos;
layout (location =1) in vec3 aColor;

layout (location =2) in vec2 aTexCoord;

out vec3 outcolor;
out vec2 outtexcoord;

uniform float uOffset ;
uniform mat4 uModelMatrix;
uniform mat4 uViewMatrix;
uniform mat4 uProjectionMatrix; 


void main()
{
gl_Position = uProjectionMatrix * uViewMatrix * uModelMatrix * vec4(aPos, 1.0);
outcolor = aColor;
outtexcoord = aTexCoord;

};
