#version 440 core


in vec3 outNormal;
in vec3 outFragPos;

out vec4 FragColor;

uniform vec3 uObjectColor;
uniform vec3 uLightColor;
uniform vec3 uLightPos;
uniform vec3 uViewPos;

void main()
{
// ambient lighting calculations
float ambientStrength = 0.1f;
vec3  ambient= ambientStrength * uLightColor;

// diffuse lighting calculations
vec3 norm = normalize(outNormal);
vec3 lightDir = normalize(uLightPos - outFragPos);
float diff = max(dot(norm, lightDir), 0.0f);
vec3 diffuse = diff * uLightColor;

// specular lighting calculations
float specularStrength = 0.5f;
vec3 viewDir = normalize(uViewPos - outFragPos);
vec3 reflectDir = reflect(-lightDir, norm);
float spec = pow(max(dot(viewDir, reflectDir), 0.0f), 32);
vec3 specular = specularStrength * spec * uLightColor;

vec3 result = (ambient + diffuse + specular) * uObjectColor;
FragColor =vec4(result , 1.0f);
};
