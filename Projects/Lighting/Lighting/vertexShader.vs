#version 440 core

layout (location =0) in vec3 aPos;
layout (location =1) in vec3 aNormal;

out vec3 outNormal;
out vec3 outFragPos;

uniform mat4 uModelMatrix;
uniform mat4 uViewMatrix;
uniform mat4 uProjectionMatrix; 

void main()
{
gl_Position = uProjectionMatrix * uViewMatrix * uModelMatrix * vec4(aPos, 1.0);
// convert to world space coords
outFragPos = vec3(uModelMatrix * vec4(aPos, 1.0));
outNormal =  mat3(transpose(inverse(uModelMatrix))) * aNormal; 
};
