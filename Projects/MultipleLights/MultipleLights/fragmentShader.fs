#version 440 core 

in vec3 outNormal;
in vec3 outFragPos;
in vec2 outTexcoords;

out vec4 FragColor;

uniform vec3 uViewPos;

struct Material {
    vec3 ambient;
    //vec3 diffuse;
	sampler2D diffuse;
    //vec3 specular;
	sampler2D specular;
    float shininess;
}; 

struct SpotLight {
    vec3 position;
	vec3 direction;
	float cutoff;
	float outerCutoff;
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;

};
uniform SpotLight spotLight;

struct DirLight {
    vec3 direction;
  
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};  
uniform DirLight dirLight;

struct PointLight {    
    vec3 position;
    
    float constant;
    float linear;
    float quadratic;  

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};  
#define NR_POINT_LIGHTS 4  
uniform PointLight pointLights[NR_POINT_LIGHTS];


uniform Material uMaterial;

// function prototype declarations
vec3 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir);  
vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir);  
vec3 CalcSpotLight(SpotLight light , vec3 normal, vec3 fragPos, vec3 viewDir);  

// entry point function
void main()
{
vec3 norm = normalize(outNormal);
vec3 viewDir = normalize(uViewPos - outFragPos);

// phase 1: directional lighting
vec3 result = CalcDirLight(dirLight, norm, viewDir);
// phase 2: point lighting
for(int i=0; i<NR_POINT_LIGHTS; i++)
{
	result += CalcPointLight(pointLights[i], norm, outFragPos, viewDir);
}
// phase 3: flash/spot light
result += CalcSpotLight(spotLight, norm, outFragPos, viewDir);
FragColor =vec4(result , 1.0f);
};
vec3 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir)
{
vec3 lightDir = normalize(-light.direction);
//Diffuse Shading
float diff = max(dot(normal, lightDir), 0.0f);
//Specular Shadding
vec3 reflectDir = reflect(-lightDir, normal);

float spec = pow(max(dot(viewDir, reflectDir), 0.0f), uMaterial.shininess);
vec3  ambient=  light.ambient * vec3(texture(uMaterial.diffuse , outTexcoords));
vec3 diffuse = light.diffuse * (diff * vec3(texture(uMaterial.diffuse , outTexcoords)));
vec3 specular = light.specular * (spec * vec3(texture(uMaterial.specular , outTexcoords)));

return(ambient + diffuse + specular);

}

vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
vec3 lightDir = normalize(light.position - fragPos);

float diff = max(dot(normal, lightDir), 0.0f);
vec3 reflectDir = reflect(-lightDir, normal);

float spec = pow(max(dot(viewDir, reflectDir), 0.0f), uMaterial.shininess);
float distance = length(light.position - fragPos);
float attenuation = 1.0f / (light.constant + light.linear * distance + light.quadratic * (distance * distance));

vec3  ambient=  light.ambient * vec3(texture(uMaterial.diffuse , outTexcoords));
vec3 diffuse = light.diffuse * (diff * vec3(texture(uMaterial.diffuse , outTexcoords)));
vec3 specular = light.specular * (spec * vec3(texture(uMaterial.specular , outTexcoords)));
ambient  *= attenuation; 
diffuse  *= attenuation;
specular *= attenuation;

return (ambient + diffuse + specular);

}  

vec3 CalcSpotLight(SpotLight light , vec3 normal, vec3 fragPos, vec3 viewDir)
{
// ambient lighting calculations
vec3  ambient=  light.ambient * vec3(texture(uMaterial.diffuse , outTexcoords));
vec3 lightDir = normalize(light.position - fragPos);
float theta = dot(lightDir, normalize(-light.direction));

vec3 result;
if(theta > light.outerCutoff)
{
	// do lighting calculations
	float epsilon = light.cutoff - light.outerCutoff;
float intensity = clamp((theta - light.outerCutoff) / epsilon, 0.0f, 1.0f);

float diff = max(dot(normal, lightDir), 0.0f);
vec3 diffuse = light.diffuse * (diff * vec3(texture(uMaterial.diffuse , outTexcoords)));

// specular lighting calculations
vec3 reflectDir = reflect(-lightDir, normal);
float spec = pow(max(dot(viewDir, reflectDir), 0.0f), uMaterial.shininess);
vec3 specular = light.specular * (spec * vec3(texture(uMaterial.specular , outTexcoords)));

diffuse  *= intensity;
specular *= intensity;
	result = ambient + diffuse + specular;
}else
{
	// use ambient lighting here
	result = ambient;
}
return result;
}

