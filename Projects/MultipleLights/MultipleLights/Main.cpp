//Header Files
#include"Shader.h"
#include"Camera.h"

#include<stdio.h>
#define STB_IMAGE_IMPLEMENTATION
#include<stb_image.h>
//Global Variables

float vertices[] = {
	// positions          // normals           // texture coords
	-0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f, 0.0f,
	0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f, 0.0f,
	0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f, 1.0f,
	0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f, 1.0f,
	-0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f, 1.0f,
	-0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f, 0.0f,

	-0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   0.0f, 0.0f,
	0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   1.0f, 0.0f,
	0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   1.0f, 1.0f,
	0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   1.0f, 1.0f,
	-0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   0.0f, 1.0f,
	-0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   0.0f, 0.0f,

	-0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f, 0.0f,
	-0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  1.0f, 1.0f,
	-0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
	-0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
	-0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  0.0f, 0.0f,
	-0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f, 0.0f,

	0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f,
	0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f,
	0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
	0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
	0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  0.0f, 0.0f,
	0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f,

	-0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f, 1.0f,
	0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  1.0f, 1.0f,
	0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f, 0.0f,
	0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f, 0.0f,
	-0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  0.0f, 0.0f,
	-0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f, 1.0f,

	-0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f, 1.0f,
	0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  1.0f, 1.0f,
	0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f, 0.0f,
	0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f, 0.0f,
	-0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  0.0f, 0.0f,
	-0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f, 1.0f
};

glm::vec3 cubePositions[] = {
	glm::vec3(0.0f,  0.0f,  0.0f),
	glm::vec3(2.0f,  5.0f, -15.0f),
	glm::vec3(-1.5f, -2.2f, -2.5f),
	glm::vec3(-3.8f, -2.0f, -12.3f),
	glm::vec3(2.4f, -0.4f, -3.5f),
	glm::vec3(-1.7f,  3.0f, -7.5f),
	glm::vec3(1.3f, -2.0f, -2.5f),
	glm::vec3(1.5f,  2.0f, -2.5f),
	glm::vec3(1.5f,  0.2f, -1.5f),
	glm::vec3(-1.3f,  1.0f, -1.5f)
};

glm::vec3 pointLightPositions[] = {
	glm::vec3(0.7f,  0.2f,  2.0f),
	glm::vec3(2.3f, -3.3f, -4.0f),
	glm::vec3(-4.0f,  2.0f, -12.0f),
	glm::vec3(0.0f,  0.0f, -3.0f)
};

// camera
Camera camera(glm::vec3(0.0f,0.0f, 3.0f), glm::vec3(0.0f, 1.0f, 0.0f), YAW, PITCH);
glm::vec3 lightPos(1.2f, 1.0f, 2.0f);
unsigned int indices[] = {
	0 , 1 , 3, // Frist Triangle
	1 , 2 , 3  // Second Triangle
};
unsigned int EBO;
unsigned int VAO;
unsigned int texture;
unsigned int textureSpecular;
unsigned int textureSmiley;
float deltaTime = 0.0f;
float lastFrame = 0.0f;
//Mouse Values
float lastX = 400 , lastY = 300;

float yaw = -90.0f, pitch = 0.0f;

bool firstMouse = true;

float fov = 45.0f;




extern FILE * fplogfile;

Shader* lightingShader;
Shader* lampShader;
Base* baseFramework;

//Function Prototype Declaration
void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void mouse_callback(GLFWwindow* window, double xPos, double yPos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
unsigned int loadTexture(const char* imagePath);


void Display(void);

//
void Processinput(GLFWwindow* window);
//Entrypoint Function
int main()
{
	/////////////////////////////////////// Initialization Starts
	baseFramework = new Base(); // initializes GLFW/GLUT/OpenGL

	GLFWwindow* window = glfwCreateWindow(800, 600, "Hello Window", NULL, NULL);
	if (window == NULL)
	{
		fprintf(fplogfile, "Window is not created successfully so exiting program \n");
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	glfwSetFramebufferSizeCallback(window , framebuffer_size_callback);
	
	glfwSetCursorPosCallback(window , mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);


	//initializing glad
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		fprintf(fplogfile, "Failed to initialize GLAD \n");
		return -1;
	}

	lightingShader = new Shader("vertexShader.vs", "fragmentShader.fs");
	lampShader = new Shader("vertexShader_Pos.vs", "fragmentShader_Pos.fs");

	glViewport(0, 0, 800, 600);

	
	/////Create VAO
	glGenVertexArrays(1, &VAO);

	glBindVertexArray(VAO);

	//create VBO (Vertex Buffer Object)
	unsigned int VBO;
	glGenBuffers(1, &VBO);
	//Activate VBO now
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	//Pass Data 
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);


	//Vertex data
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	//normal data
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);

	//Texture data
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
	glEnableVertexAttribArray(2);


	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Create EBO

	glGenBuffers(1, &EBO);
	//Activate VBO now
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	//Pass Data 
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	glEnable(GL_DEPTH_TEST);

	texture = loadTexture("container2.png");
	textureSpecular = loadTexture("container2_specular.png");
	////////////////////////////////Initialization Ends

	//////////////////////////////Game Loop Starts
	while (!glfwWindowShouldClose(window))
	{
		Processinput(window);
		Display();
		glfwSwapBuffers(window);
		glfwPollEvents();
	}
	///////////////////////////Game Loop Ends

	delete(lightingShader);
	delete(lampShader);
	delete(baseFramework); // it calls destructor of the class

	/////////////////////////Uninitialize End
	return 0;
}

void mouse_callback(GLFWwindow* window, double xPos, double yPos)
{
	if (firstMouse)
	{
		lastX = xPos;
		lastY = yPos;
		firstMouse = false;
	}
	float xoffset = xPos - lastX;
	float yoffset = lastY - yPos;
	lastX = xPos;
	lastY = yPos;

	camera.ProcessMouseMovement(xoffset, yoffset, true);
}


void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	camera.ProcessMouseScroll(yoffset);
}





void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
}

void Processinput(GLFWwindow* window)
{

	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(window, true);
	}

	if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
	{
		camera.ProcessKeyboard(FORWARD, deltaTime);
	}
	if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
	{
		camera.ProcessKeyboard(BACKWARD, deltaTime);
	}
	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
	{
		camera.ProcessKeyboard(LEFT, deltaTime);
	}
	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
	{
		camera.ProcessKeyboard(RIGHT, deltaTime);
	}

 

}

void Display(void)
{
	glClearColor(0.6f, 0.3f, 0.1f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	float currentFrame = glfwGetTime();
	deltaTime = currentFrame - lastFrame;
	lastFrame = currentFrame;

	// transformation matrices
	glm::mat4 modelMatrix = glm::mat4(1.0f);
	glm::mat4 viewMatrix = glm::mat4(1.0f);
	glm::mat4 projectionMatrix = glm::mat4(1.0f);

	// draw lamp
	lampShader->use();
	
	//View Transformation

	viewMatrix = camera.GetViewMatrix();


	//Projection Transform
	projectionMatrix = glm::perspective(glm::radians(fov), 800.0f / 600.0f, 0.1f, 100.0f);
	
	lampShader->setMat4("uViewMatrix", viewMatrix);
	lampShader->setMat4("uProjectionMatrix", projectionMatrix);
	glm::vec3 lightColor = glm::vec3(1.0f);
	glm::vec3 diffuseColor = glm::vec3(1.0f);
	glBindVertexArray(VAO);
	for (int i = 0; i < 4; i++)
	{
		modelMatrix = glm::mat4(1.0);
		modelMatrix = glm::translate(modelMatrix, pointLightPositions[i]);
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.2f));
		lampShader->setMat4("uModelMatrix", modelMatrix);

		if (i == 0)
		{
			diffuseColor = glm::vec3(0.2f, 0.2f, 0.9f);
			lampShader->setVec3("uLightColor", lightColor);
			lampShader->setVec3("uObjectColor", diffuseColor);
		}else if (i == 1)
		{
			diffuseColor = glm::vec3(1.0f, 1.0f, 0.0f);
			lampShader->setVec3("uLightColor", lightColor);
			lampShader->setVec3("uObjectColor", diffuseColor);
		}else if (i == 2)
		{
			diffuseColor = glm::vec3(1.0f, 0.0f, 1.0f);
			lampShader->setVec3("uLightColor", lightColor);
			lampShader->setVec3("uObjectColor", diffuseColor);
		}else if (i == 3)
		{
			diffuseColor = glm::vec3(1.0f, 0.0f, 0.0f);
			lampShader->setVec3("uLightColor", lightColor);
			lampShader->setVec3("uObjectColor", diffuseColor);
		}
		glDrawArrays(GL_TRIANGLES, 0, 36);
	}
	
	glBindVertexArray(0);
	lampShader->unUse();

	/////use program object
	// Object preparation
	lightingShader->use();

	//Create transformation matrix
	//View Transformation
	viewMatrix = camera.GetViewMatrix();
	//Projection Transform
	projectionMatrix = glm::perspective(glm::radians(fov), 800.0f / 600.0f, 0.1f, 100.0f);
	
	//setting uniforms
	lightingShader->setMat4("uViewMatrix", viewMatrix);
	lightingShader->setMat4("uProjectionMatrix", projectionMatrix);
	lightingShader->setVec3("uViewPos", camera.Position);

	
	//lightColor.x = sin(glfwGetTime() * 2.0f);
	//lightColor.y = sin(glfwGetTime() * 0.7f);
	//lightColor.z = sin(glfwGetTime() * 1.3f);
	
	glm::vec3 ambientColor = glm::vec3(0.05f, 0.05f, 0.05f);
	diffuseColor = glm::vec3(0.4f, 0.4f, 0.4f);
	glm::vec3 specularColor = glm::vec3(0.5f, 0.5f, 0.5f);
	glm::vec3 directionVector = glm::vec3(-0.2f, -1.0f, -0.3f);
	// light properties
	// phase 1: directional lighting
	lightingShader->setVec3("dirLight.direction", directionVector);
	lightingShader->setVec3("dirLight.ambient", ambientColor);
	lightingShader->setVec3("dirLight.diffuse", diffuseColor);
	lightingShader->setVec3("dirLight.specular",specularColor);

	// phase 2: point light
	ambientColor = glm::vec3(0.05f, 0.05f, 0.05f);
	diffuseColor = glm::vec3(0.2f, 0.2f, 0.9);
	specularColor = glm::vec3(1.0f, 1.0f, 1.0f);
	lightingShader->setVec3("pointLights[0].ambient", ambientColor);
	lightingShader->setVec3("pointLights[0].diffuse", diffuseColor);
	lightingShader->setVec3("pointLights[0].specular", specularColor);
	lightingShader->setVec3("pointLights[0].position", pointLightPositions[0]);
	lightingShader->setFloat("pointLights[0].constant", 1.0f);
	lightingShader->setFloat("pointLights[0].linear", 0.09f);
	lightingShader->setFloat("pointLights[0].quadratic", 0.032f);

	diffuseColor = glm::vec3(1.0f, 1.0f, 0.0f);
	lightingShader->setVec3("pointLights[1].ambient", ambientColor);
	lightingShader->setVec3("pointLights[1].diffuse", diffuseColor);
	lightingShader->setVec3("pointLights[1].specular", specularColor);
	lightingShader->setVec3("pointLights[1].position", pointLightPositions[1]);
	lightingShader->setFloat("pointLights[1].constant", 1.0f);
	lightingShader->setFloat("pointLights[1].linear", 0.09f);
	lightingShader->setFloat("pointLights[1].quadratic", 0.032f);

	diffuseColor = glm::vec3(1.0f, 0.0f, 1.0f);
	lightingShader->setVec3("pointLights[2].ambient", ambientColor);
	lightingShader->setVec3("pointLights[2].diffuse", diffuseColor);
	lightingShader->setVec3("pointLights[2].specular", specularColor);
	lightingShader->setVec3("pointLights[2].position", pointLightPositions[2]);
	lightingShader->setFloat("pointLights[2].constant", 1.0f);
	lightingShader->setFloat("pointLights[2].linear", 0.09f);
	lightingShader->setFloat("pointLights[2].quadratic", 0.032f);

	diffuseColor = glm::vec3(1.0f, 0.0f, 0.0f);
	lightingShader->setVec3("pointLights[3].ambient", ambientColor);
	lightingShader->setVec3("pointLights[3].diffuse", diffuseColor);
	lightingShader->setVec3("pointLights[3].specular", specularColor);
	lightingShader->setVec3("pointLights[3].position", pointLightPositions[3]);
	lightingShader->setFloat("pointLights[3].constant", 1.0f);
	lightingShader->setFloat("pointLights[3].linear", 0.09f);
	lightingShader->setFloat("pointLights[3].quadratic", 0.032f);

	// flash/spot light
	ambientColor = glm::vec3(0.0f, 0.0f, 0.0f);
	diffuseColor = glm::vec3(0.0f, 1.0f, 0.0f);
	specularColor = glm::vec3(1.0f, 1.0f, 1.0f);
	lightingShader->setVec3("spotLight.direction", camera.Front);
	lightingShader->setVec3("spotLight.position", camera.Position);
	lightingShader->setFloat("spotLight.cutoff", glm::cos(glm::radians(12.5f)));
	lightingShader->setFloat("spotLight.outerCutoff", glm::cos(glm::radians(15.0f)));
	lightingShader->setVec3("spotLight.ambient", ambientColor);
	lightingShader->setVec3("spotLight.diffuse", diffuseColor);
	lightingShader->setVec3("spotLight.specular", specularColor);

	// material properties
	lightingShader->setVec3("uMaterial.ambient", glm::vec3(1.0f, 0.5f, 0.31f));
	lightingShader->setInt("uMaterial.diffuse", 0);
	lightingShader->setInt("uMaterial.specular", 1);
	lightingShader->setFloat("uMaterial.shininess", 32.0f);
	glBindVertexArray(VAO);
	// DRAW CUBES
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, textureSpecular);
		

		for (int i = 0;i < 10;i++)
		{
			modelMatrix = glm::mat4(1.0f);
			modelMatrix = glm::translate(modelMatrix, cubePositions[i]);
			float angle = 20.0f * i;
			modelMatrix = glm::rotate(modelMatrix, glm::radians(angle), glm::vec3(1.0f, 0.3f, 0.5f));
			//modelMatrix = glm::scale(modelMatrix, glm::vec3(1.0f, 1.0f, 1.0f));
			lightingShader->setMat4("uModelMatrix", modelMatrix);
			glDrawArrays(GL_TRIANGLES, 0, 36);
		}
		
	//glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
	lightingShader->unUse();

}

/*
loadTexture:
Loads the texture and return texture id
*/
unsigned int loadTexture(const char* imagePath)
{
	/////////Texture
	unsigned int texture;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	//load img
	int width, height, nrChannels, imageFormat;
	unsigned char* data = stbi_load(imagePath, &width, &height, &nrChannels, 0);
	if (data)
	{
		if (nrChannels == 4) // png
		{
			imageFormat = GL_RGBA;
		}else if (nrChannels == 3) // jpg
		{
			imageFormat = GL_RGB;
		}
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, imageFormat, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else
	{
		fprintf(fplogfile, "Could Not Load Image");

	}

	stbi_image_free(data);
	return texture;
}