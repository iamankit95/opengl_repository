//Header Files
#include<stdio.h>
#include<glad\glad.h>
#include<GLFW\glfw3.h>

//Global Variables
FILE * fplogfile = NULL;
float vertices[] = {
	//First Traingle
	 0.5f ,  0.5f , 0.0f, 
	 0.5f , -0.5f , 0.0f,
    -0.5f , -0.5f , 0.0f,
    -0.5f ,  0.5f , 0.0f
};

unsigned int indices[] = {
	0 , 1 , 3, // Frist Triangle
	1 , 2 , 3  // Second Triangle
};

unsigned int shaderProgram;
unsigned int vertexShader;
unsigned int fragmentShader;
unsigned int VAO;
unsigned int EBO;


//Function Prototype Declaration
void framebuffer_size_callback(GLFWwindow* window, int width, int height);

void Display(void);

//
void Processinput(GLFWwindow* window);
//Entrypoint Function
int main()
{
	/////////////////////////////////////// Initialization Starts
	
	
	
	
	// opening file for loging
	int fopenresult  = fopen_s(&fplogfile, "log.txt", "w");
	if (fopenresult != 0)
	{
		return -1;
	}
	else
	{
		fprintf(fplogfile, "Log File Is Created Successfully \n");
	}

	glfwInit();
	fprintf(fplogfile, "GLFW initialized successfully \n");
	//OPENGL Version declaration
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR , 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	
	//WE ARE USING OPENGL CORE PROFILE VERSION
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	fprintf(fplogfile, "Using Opengl version core profile version 3.3 \n");

	
	GLFWwindow* window = glfwCreateWindow(800, 600, "Hello Window", NULL, NULL);
	if (window == NULL)
	{
		fprintf(fplogfile, "Window is not created successfully so exiting program \n");
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	glfwSetFramebufferSizeCallback(window , framebuffer_size_callback);
	//initializing glad
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		fprintf(fplogfile, "Failed to initialize GLAD \n");
		return -1;
	}

	glPolygonMode(GL_FRONT_AND_BACK , GL_FILL); //GL_LINE
	glViewport(0, 0, 800, 600);

	//Vertex Shader
	const GLchar *vertexShaderSourceCode =
		"#version 440 core"\
		"\n"\
		"layout (location =0) in vec3 aPos;"\
		"void main()"\
		"{"\
		"gl_Position = vec4(aPos.x , aPos.y , aPos.z , 1.0f);"\
		"}";

	
	vertexShader = glCreateShader(GL_VERTEX_SHADER);

	glShaderSource(vertexShader, 1, &vertexShaderSourceCode, NULL);
	glCompileShader(vertexShader);
	//Compilation on Graphics Card
	int success;
	char infoLog[512];
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
		fprintf(fplogfile, "VertexShader Compilation Error: %s \n" , infoLog);
		return -1;
	}
	else {
		fprintf(fplogfile, "Successfully Compiled Vertex Shader");
	}

	////Fragment Shader
	const GLchar *fragmentShaderSourceCode =
		"#version 440 core"\
		"\n"\
		"out vec4 FragColor;"\
		"void main()"\
		"{"\
		"FragColor = vec4(1.0f , 1.0f , 0.0f , 1.0f);"\
		"}";

	
	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

	glShaderSource(fragmentShader, 1, &fragmentShaderSourceCode, NULL);
	glCompileShader(fragmentShader);
	//Compilation on Graphics Card
	//int success;
	*infoLog = NULL;
	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
		fprintf(fplogfile, "fragmentShader Compilation Error: %s \n", infoLog);
		return -1;
	}
	else {
		fprintf(fplogfile, "Successfully Compiled Fragment Shader \n");
	}

	
	shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glLinkProgram(shaderProgram);

	*infoLog = NULL;
	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
	if (!success)
	{
		glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
		fprintf(fplogfile, "Program Object Linking Error: %s \n", infoLog);
		return -1;
	}
	else {
		fprintf(fplogfile, "Successfully Compiled Shader Program");
	}
	/////Create VAO
	glGenVertexArrays(1, &VAO);

	glBindVertexArray(VAO);

	//create VBO (Vertex Buffer Object)
	unsigned int VBO;
	glGenBuffers(1, &VBO);
	//Activate VBO now
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	//Pass Data 
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);



	

	//Create EBO

	glGenBuffers(1, &EBO);
	//Activate VBO now
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	//Pass Data 
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);


	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(0);


	////////////////////////////////Initialization Ends

	//////////////////////////////Game Loop Starts
	while (!glfwWindowShouldClose(window))
	{
		Processinput(window);
		Display();
		glfwSwapBuffers(window);
		glfwPollEvents();
	}
	///////////////////////////Game Loop Ends
	//closing file
	//////////////////////////Uninitialize Start
	fprintf(fplogfile, "Closing Log File \n");
	fclose(fplogfile);
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
	glDeleteProgram(shaderProgram);

	glfwTerminate();
	/////////////////////////Uninitialize End
	return 0;
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
}

void Processinput(GLFWwindow* window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(window, true);
	}
}

void Display(void)
{
	glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	/////use program object
	glUseProgram(shaderProgram);

	glBindVertexArray(VAO);
	//glDrawArrays(GL_TRIANGLES, 0, 6);
	// Draw (EBO)
	glDrawElements(GL_TRIANGLES, 6 , GL_UNSIGNED_INT , 0 );
	glBindVertexArray(0);
	glUseProgram(0);

}