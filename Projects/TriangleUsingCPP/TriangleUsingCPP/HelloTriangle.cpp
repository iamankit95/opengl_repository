//Header Files
#include"Shader.h"
#include<stdio.h>

//Global Variables
float vertices[] = {
	-0.5f , -0.5f , 0.0f,      1.0f , 0.0f , 0.0f ,
	 0.5f , -0.5f , 0.0f,      0.0f , 1.0f , 0.0f ,
	 0.0f ,  0.5f , 0.0f,      0.0f , 0.0f , 1.0f   
};

unsigned int VAO;

extern FILE * fplogfile;

Shader* ourShader;
Base* baseFramework;

//Function Prototype Declaration
void framebuffer_size_callback(GLFWwindow* window, int width, int height);

void Display(void);

//
void Processinput(GLFWwindow* window);
//Entrypoint Function
int main()
{
	/////////////////////////////////////// Initialization Starts
	baseFramework = new Base(); // initializes GLFW/GLUT/OpenGL

	GLFWwindow* window = glfwCreateWindow(800, 600, "Hello Window", NULL, NULL);
	if (window == NULL)
	{
		fprintf(fplogfile, "Window is not created successfully so exiting program \n");
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	glfwSetFramebufferSizeCallback(window , framebuffer_size_callback);
	//initializing glad
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		fprintf(fplogfile, "Failed to initialize GLAD \n");
		return -1;
	}

	ourShader = new Shader("vertexShader.vs", "fragmentShader.fs");

	glViewport(0, 0, 800, 600);

	
	/////Create VAO
	glGenVertexArrays(1, &VAO);

	glBindVertexArray(VAO);

	//create VBO (Vertex Buffer Object)
	unsigned int VBO;
	glGenBuffers(1, &VBO);
	//Activate VBO now
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	//Pass Data 
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
	////////////////////////////////Initialization Ends

	//////////////////////////////Game Loop Starts
	while (!glfwWindowShouldClose(window))
	{
		Processinput(window);
		Display();
		glfwSwapBuffers(window);
		glfwPollEvents();
	}
	///////////////////////////Game Loop Ends

	delete(ourShader);
	delete(baseFramework); // it calls destructor of the class

	/////////////////////////Uninitialize End
	return 0;
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
}

void Processinput(GLFWwindow* window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(window, true);
	}
}

void Display(void)
{
	glClearColor(0.1f, 0.3f, 0.4f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	/////use program object
	ourShader->use();

	glBindVertexArray(VAO);
	glDrawArrays(GL_TRIANGLES, 0, 3);
	
	glBindVertexArray(0);
	ourShader->unUse();

}