#include"Shader.h"

Shader::Shader(const char* vertexPath, const char* fragmentPath)
{
	std::ifstream vShaderFile;
	std::ifstream fShaderFile;
	std::string vertexCode;
	std::string fragmentCode;

	try
	{
		vShaderFile.open(vertexPath);
		fShaderFile.open(fragmentPath);
		std::stringstream vShaderStream;
		std::stringstream fShaderStream;
		vShaderStream << vShaderFile.rdbuf();
		fShaderStream << fShaderFile.rdbuf();
		vShaderFile.close();
		fShaderFile.close();
		vertexCode = vShaderStream.str();
		fragmentCode = fShaderStream.str();

	}
	catch(std::ifstream::failure e)
	{
		fprintf(fplogfile, "ERROR::SHADER::FILE_NOT_SUCCESSFULLY_READ \n");
	}

	const char * vShaderCode = vertexCode.c_str();
	const char * fShaderCode = fragmentCode.c_str();


	//////////////
	unsigned int vertexShader, fragmentShader;
	
	vertexShader = glCreateShader(GL_VERTEX_SHADER);

	glShaderSource(vertexShader, 1, &vShaderCode, NULL);
	glCompileShader(vertexShader);
	//Compilation on Graphics Card
	int success;
	char infoLog[512];
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
		fprintf(fplogfile, "VertexShader Compilation Error: %s \n", infoLog);
		
	}
	else {
		fprintf(fplogfile, "Successfully Compiled Vertex Shader\n");
	}





	//////////////////
	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

	glShaderSource(fragmentShader, 1, &fShaderCode, NULL);
	glCompileShader(fragmentShader);
	//Compilation on Graphics Card
	//int success;
	*infoLog = NULL;
	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
		fprintf(fplogfile, "fragmentShader Compilation Error: %s \n", infoLog);
	}
	else {
		fprintf(fplogfile, "Successfully Compiled Fragment Shader \n");
	}

	//////////////////////////////

	ID = glCreateProgram();
	glAttachShader(ID, vertexShader);
	glAttachShader(ID, fragmentShader);
	glLinkProgram(ID);

	*infoLog = NULL;
	glGetProgramiv(ID, GL_LINK_STATUS, &success);
	if (!success)
	{
		glGetProgramInfoLog(ID, 512, NULL, infoLog);
		fprintf(fplogfile, "Program Object Linking Error: %s \n", infoLog);
		
	}
	else {
		fprintf(fplogfile, "Successfully Compiled Shader Program\n");
	}


	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

}

void Shader::use()
{
	glUseProgram(ID);
}

void Shader::unUse()
{
	glUseProgram(0);
}

void Shader::setBool(const std::string  &name, bool value) const 
{
	unsigned int loc = glGetUniformLocation(ID, name.c_str());
	glUniform1i(loc, value);
}

void Shader::setInt(const std::string  &name, int value) const
{
	unsigned int loc = glGetUniformLocation(ID, name.c_str());
	glUniform1i(loc, value);
}
void Shader::setFloat(const std::string  &name, float value) const
{
	unsigned int loc = glGetUniformLocation(ID, name.c_str());
	glUniform1f(loc, value);
}
void Shader::setMat4(const std::string  &name, glm::mat4 value) const
{
	unsigned int loc = glGetUniformLocation(ID, name.c_str());
	glUniformMatrix4fv(loc, 1 , GL_FALSE ,  glm::value_ptr(value));
}

void Shader::setVec3(const std::string  &name, glm::vec3 value) const
{
	unsigned int loc = glGetUniformLocation(ID, name.c_str());
	glUniform3f(loc, value.x, value.y, value.z);
}



Shader::~Shader()
{
	glDeleteProgram(ID);
}