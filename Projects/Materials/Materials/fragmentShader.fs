#version 440 core


in vec3 outNormal;
in vec3 outFragPos;

out vec4 FragColor;

uniform vec3 uViewPos;

struct Material {
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    float shininess;
}; 

struct Light {
    vec3 position;
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

uniform Material uMaterial;
uniform Light uLight1;


void main()
{
// ambient lighting calculations
vec3  ambient=  uLight1.ambient * uMaterial.ambient;

// diffuse lighting calculations
vec3 norm = normalize(outNormal);
vec3 lightDir = normalize(uLight1.position - outFragPos);
float diff = max(dot(norm, lightDir), 0.0f);
vec3 diffuse = uLight1.diffuse * (diff * uMaterial.diffuse);

// specular lighting calculations
vec3 viewDir = normalize(uViewPos - outFragPos);
vec3 reflectDir = reflect(-lightDir, norm);
float spec = pow(max(dot(viewDir, reflectDir), 0.0f), uMaterial.shininess);
vec3 specular = uLight1.specular * (spec * uMaterial.specular);

vec3 result = ambient + diffuse + specular;
FragColor =vec4(result , 1.0f);
};
