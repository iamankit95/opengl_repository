#ifndef BASE_H
#define BASE_H

#include<glad\glad.h>
#include<GLFW\glfw3.h>
#include<iostream>
#include<stdio.h>
#include<fstream>
#include<string>
#include<sstream>

#include<glm\glm.hpp>
#include<glm\gtc\matrix_transform.hpp>
#include<glm\gtc\type_ptr.hpp>

#include<vector>

#define SCR_WIDTH 1280 //1280
#define SCR_HEIGHT 720 //720

#define SHADOW_WIDTH 1024
#define SHADOW_HEIGHT 1024

extern FILE * fplogfile;
extern bool explodeNonosuitModel;
extern bool isBlinnKeyPressed;
class Base
{
public:
	Base(); // constructor
	~Base(); // destructor
};
#endif
