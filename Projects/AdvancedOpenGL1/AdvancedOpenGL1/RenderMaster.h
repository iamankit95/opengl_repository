#ifndef RENDERMASTER_H
#define RENDERMASTER_H


#include"Base.h"
#include"Shader.h"
#include"Model.h"
#include"Camera.h"
#include"WorldRenderer.h"

class RenderMaster
{
public:
	FBO *fbo;
	FBO *fboInversion;
	FBO *fboGreyscale;
	FBO *fboSharpenEdge;
	FBO *fboEdgeDetection;
	FBO *fboBlur;
	FBO *fboGammaCorrect;
	FBO *fboPipelineStopper;
	FBO *fboDepthMap;
	WorldRenderer *worldRenderer;
	FBO *fboHdr;
	FBO *fboMultipleColorTargets;

	// for guassian blur
	FBO *fboPingForGaussianBlur;
	FBO *fboPongForGaussianBlur;

	// constructor's
	RenderMaster();

	// methods
	void initialize();
	void uninitialize();
	void renderScene(Camera* camera);
private:
	float fov = 45.0f;
	glm::mat4 projectionMatrix;
	Shader* inversionFramebufferShader;
	Shader* greyscaleFramebufferShader;
	Shader* twoDFramebufferShader;
	Shader* shaderDebugQuadDepth;
	Shader* shaderEdgeDetection;
	Shader* shaderBlur;
	Shader* shaderSharpenEdge;
	Shader* shaderGammaCorrect;
};


#endif // !RENDERMASTER_H
