#include"Base.h"

FILE * fplogfile;

Base::Base()
{
	// opening file for loging
	int fopenresult = fopen_s(&fplogfile, "log.txt", "w");
	if (fopenresult != 0)
	{
		// do nothing
		std::cout << "Cannot open the file for reading" << std::endl;
	}
	else
	{
		fprintf(fplogfile, "Log File Is Created Successfully \n");
	}

	// initializes GLFW
	glfwInit();
	fprintf(fplogfile, "GLFW initialized successfully \n");
	//OPENGL Version declaration
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

	//WE ARE USING OPENGL CORE PROFILE VERSION
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	fprintf(fplogfile, "Using Opengl version core profile version 3.3 \n");

}

Base::~Base()
{
	//closing file
	//////////////////////////Uninitialize Start
	// uninitializes GLFW
	glfwTerminate();
	fprintf(fplogfile, "GLFW Uninitialized \n");
	fprintf(fplogfile, "Closing Log File \n");
	fclose(fplogfile);
}