#include"CubemapRenderer.h"

CubemapRenderer::CubemapRenderer(glm::mat4 projMat)
{
	this->projectionMatrix = projMat;

	skyboxVertices =
	{
		// positions          
		-1.0f,  1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,

		-1.0f, -1.0f,  1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f,  1.0f,
		-1.0f, -1.0f,  1.0f,

		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,

		-1.0f, -1.0f,  1.0f,
		-1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f, -1.0f,  1.0f,
		-1.0f, -1.0f,  1.0f,

		-1.0f,  1.0f, -1.0f,
		1.0f,  1.0f, -1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f, -1.0f,

		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f,  1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f,  1.0f,
		1.0f, -1.0f,  1.0f
	};

	faces =
	{
		"resources/cubemaps/skybox/right.jpg",
		"resources/cubemaps/skybox/left.jpg",
		"resources/cubemaps/skybox/top.jpg",
		"resources/cubemaps/skybox/bottom.jpg",
		"resources/cubemaps/skybox/front.jpg",
		"resources/cubemaps/skybox/back.jpg"
	};
}

void CubemapRenderer::initialize()
{
	shaderCubeMapSkybox = new Shader("shaderCubeMapSkybox", "resources/shaders/vsCubemap.glsl", "resources/shaders/fsCubemap.glsl");

	

	// cubemap textures
	cubemapTexture = loadCubemap(faces);
	modelCubeMapSkybox = new Model("modelCubeMapSkybox", skyboxVertices, 100000, cubemapTexture);
}

void CubemapRenderer::uninitialize()
{

}

void CubemapRenderer::render(Camera* camera)
{
	
	// draw skybox
	glDepthMask(GL_FALSE);
	shaderCubeMapSkybox->use();
	shaderCubeMapSkybox->setMat4("uViewMatrix", glm::mat4(glm::mat3(camera->GetViewMatrix())));
	shaderCubeMapSkybox->setMat4("uProjectionMatrix", projectionMatrix);
	//glActiveTexture(GL_TEXTURE0);
	//glBindTexture(GL_TEXTURE_CUBE_MAP, cubemapTexture);

	modelCubeMapSkybox->DrawCubemap(shaderCubeMapSkybox);

	shaderCubeMapSkybox->unUse();
	glDepthMask(GL_TRUE);
	// skybox end
}
