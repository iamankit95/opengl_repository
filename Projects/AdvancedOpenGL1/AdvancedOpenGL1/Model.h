#ifndef MODEL_H
#define MODEL_H
#include"Base.h"
#include"Shader.h"
#include"Mesh.h"
#include<assimp\Importer.hpp>
#include<assimp\scene.h>
#include<assimp\postprocess.h>




class Model
{
public:
	const char* name;
	bool isOutlineNeeded;
	std::vector<Texture> textures_loaded;	// stores all the textures loaded so far, optimization to make sure textures aren't loaded more than once.
	Model(const char* name, const char* path);
	Model(const char* name, const char* path, int instances, glm::mat4* instacedModelMatrices);
	Model(const char* name, std::vector<float> vertices, int value, std::vector<Texture> textures); // PCNTTB - Position,Color,Normal,Texcoords,Tangents,Bitangents
	Model(const char* name, std::vector<float> vertices, int value, std::vector<Texture> textures,bool isInstanced, std::vector<glm::vec2> translations);
	Model(const char* name, std::vector<float> vertices, int value, unsigned int textures);
	void Draw(Shader* shader);
	
	void DrawCubemap(Shader* shader);
	void DrawPoints(Shader* shader);
	void Draw(Shader* shader, bool isInterleaved);
	void Draw(Shader* shader, int instanceCount, glm::mat4* instacedModelMatrices, bool isInterleaved);
	void Draw(Shader* shader, bool isInterleaved, unsigned int tbo);
	
private:
	std::vector<Mesh> meshes;
	std::string directory;
	void loadModel(std::string path);
	void processNode(aiNode *node, const aiScene *scene);
	Mesh processMesh(aiMesh *mesh, const aiScene *scene);
	std::vector<Texture> loadMaterialTextures(aiMaterial *mat, aiTextureType type,
		std::string typeName);
	void populateIntereavedData(std::vector<float> vertices, int dataHint,
		std::vector<Texture> textures);
	void populateIntereavedData(std::vector<float> vertices, int dataHint,
		std::vector<Texture> textures, bool isInstanced, std::vector<glm::vec2> translations);
};


#endif // !MODEL_H
