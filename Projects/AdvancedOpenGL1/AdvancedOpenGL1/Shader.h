#ifndef SHADER_H
#define SHADER_H


#include"Base.h"

enum ShaderType
{
	VERTEX_SHADER_TYPE,
	FRAGMENT_SHADER_TYPE,
	GEOMETRY_SHADER_TYPE
};

class Shader
{
public:
	//prpgram ID
	unsigned int ID;
	const char* name;

	// constructor declarations
	Shader(const char* vertexPath, const char* fragmentPath);
	Shader(const char *name, const char* vertexPath, const char* geometryPath, const char* fragmentPath);
	Shader(const char *name, const char* vertexPath, const char* fragmentPath);

	// desctructor declarations
	~Shader();

	//
	void use();
	void unUse();

	void setBool(const std::string  &name, bool value) const;
	void setInt(const std::string  &name, int value) const;
	void setFloat(const std::string  &name, float value) const;
	void setMat4(const std::string  &name, glm::mat4 value) const;
	void setVec3(const std::string  &name, glm::vec3 value) const;
	void setVec2(const std::string  &name, glm::vec2 value) const;

private:
	void setUpShaders(const char* vShader, const char* fShader);
	void setUpShaders(const char* vShader, const char* gShader, const char* fShader);
	unsigned int setupShader(const char* shaderPath, ShaderType type);
};





#endif
