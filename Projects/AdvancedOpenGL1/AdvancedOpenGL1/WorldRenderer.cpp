#include"WorldRenderer.h"

WorldRenderer::WorldRenderer(glm::mat4 projMat)
{
	// set projection matrix
	projectionMatrix = projMat;

	// Geometry Renderer
	geometryModelRenderer = new GeometryRenderer(projMat);

	//cubeMapRenderer = new CubemapRenderer(projMat);
	cartsRenderer = new CartsRenderer(projMat, geometryModelRenderer);

	//modelRenderer = new ModelRenderer(projMat);

	

	// for stopping pipeline
	fboDummy = new FBO();
	}

void WorldRenderer::initialize()
{
	//cubeMapRenderer->initialize();
	cartsRenderer->initialize();

	// model class needs to be initialized only once
	// as we are rendering multiple models using this class so 
	// initialization code for all models is written in this initialize() method
	//modelRenderer->initialize();
	geometryModelRenderer->initialize(); // first init geometry to provide geometry data
	
	shaderDepthMap = new Shader("shaderDepthMap", "resources/shaders/vsDepthMap.glsl", "resources/shaders/fsDepthMap.glsl");
	shaderShadowMap = new Shader("shaderShadowMap", "resources/shaders/vsLightsShadowMapping.glsl", "resources/shaders/fsLightsDirectionalShadowMapping.glsl");
	shaderDirectionalLight = new Shader("shaderDirectionalLight", "resources/shaders/vsLights.glsl", "resources/shaders/fsLightsDirectional.glsl");
	shaderDirectionalLightWithNormalMap = new Shader("shaderDirectionalLight", "resources/shaders/vsLightsDirectionalWithNormalMap.glsl", "resources/shaders/fsLightsDirectionalWithNormalMap.glsl");
	shaderDirectionalLightWithNormalParallaxMap = new Shader("shaderDirectionalLight", "resources/shaders/vsLightsDirectionalWithNormalParallaxMap.glsl", "resources/shaders/fsLightsDirectionalWithNormalParallaxMap.glsl");
	shaderHdr = new Shader("shaderHdr", "resources/shaders/vertexShaderFramebuffer.vs", "resources/shaders/fsHdr.glsl");
	shaderPointLighting = new Shader("shaderPointLighting", "resources/shaders/vsLights.glsl", "resources/shaders/fsLightsPoint.glsl");
	shaderTwoPassGuassianBlur = new Shader("shaderTwoPassGuassianBlur", "resources/shaders/vertexShaderFramebuffer.vs", "resources/shaders/fsTwopassGuassianBlur.glsl");
	shaderBloomFinal = new Shader("shaderTwoPassGuassianBlur", "resources/shaders/vertexShaderFramebuffer.vs", "resources/shaders/fsBloomFinal.glsl");

	// currently active shader
	shaderHandle = shaderPointLighting;
}

void WorldRenderer::renderWorld(Camera* camera, FBO* fbo, FBO* fboDepthMap)
{
	
	// for shadows:
	// phase 1: first render to depth map
	glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
	glBindFramebuffer(GL_FRAMEBUFFER, fboDepthMap->fboId);
	glClearColor(0.2f,0.3f,0.5f,1.0f);
	glClear(GL_DEPTH_BUFFER_BIT);
	// configure shaders and matrices
	// a projection matrix indirectly determines the range of what is visible 
	// (e.g. what is not clipped) you want to make sure the size of the projection frustum correctly contains 
	// the objects you want to be in the depth map. When objects or fragments are not in the depth map 
	// they will not produce shadows.
	float near_plane = 1.0f, far_plane = 7.5f;
	glm::mat4 lightProjection = glm::ortho(-10.0f, 10.0f, -10.0f, 10.0f, near_plane, far_plane);
	glm::vec3 lightPos = glm::vec3(-2.0f, 3.0f, -1.0f);
	glm::mat4 lightView = glm::lookAt(lightPos,
		glm::vec3(0.0f, 0.0f, 0.0f),
		glm::vec3(0.0f, 1.0f, 0.0f));

	glm::mat4 lightSpaceMatrix = lightProjection * lightView;
	
	shaderDepthMap->use();
	shaderDepthMap->setMat4("uLightSpaceMatrix", lightSpaceMatrix);
	// render scene
	//renderWorld(camera, fboDummy);
	glDisable(GL_CULL_FACE);
	glm::mat4 model = glm::mat4(1.0f);
	//model = glm::translate(model, glm::vec3(0.0f, 0.0f, -3.0f));
	shaderDepthMap->setMat4("uModelMatrix", model);
	geometryModelRenderer->renderPlane(shaderDepthMap);
	//glEnable(GL_CULL_FACE);
	//glCullFace(GL_FRONT);
	renderShadowedScene(shaderDepthMap);
	shaderDepthMap->unUse();
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	
	//glCullFace(GL_BACK); // don't forget to reset original culling face

	// 2. then render scene as normal with shadow mapping (using depth map)
	glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT);
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	shaderShadowMap->use();
	// configure shaders and matrices
	shaderShadowMap->setMat4("uViewMatrix", camera->GetViewMatrix());
	shaderShadowMap->setMat4("uProjectionMatrix", this->projectionMatrix);
	// set light uniforms
	shaderShadowMap->setVec3("uViewPos", camera->Position);
	shaderShadowMap->setVec3("uLightPos", lightPos);
	shaderShadowMap->setMat4("uLightSpaceMatrix", lightSpaceMatrix);

	// phase 1: directional lighting
	glm::vec3 ambientColor = glm::vec3(0.3f);
	glm::vec3 diffuseColor = glm::vec3(1.0f);
	glm::vec3 specularColor = glm::vec3(0.5f);
	glm::vec3 directionVector = glm::vec3(-0.2f, -1.0f, -0.3f);
	shaderShadowMap->setVec3("dirLight.direction", directionVector);
	shaderShadowMap->setVec3("dirLight.ambient", ambientColor);
	shaderShadowMap->setVec3("dirLight.diffuse", diffuseColor);
	shaderShadowMap->setVec3("dirLight.specular", specularColor);
	shaderShadowMap->setVec3("uMaterial.specular", specularColor);
	shaderShadowMap->setInt("uMaterial.diffuse", 0);
	shaderShadowMap->setInt("shadowMap", 1);
	// texture 0 is passed while creating model
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, fboDepthMap->tboDepthAttachment);

	//glDisable(GL_CULL_FACE);
	model = glm::mat4(1.0f);
	//model = glm::translate(model, glm::vec3(0.0f, 0.0f, -3.0f));
	shaderShadowMap->setMat4("uModelMatrix", model);
	geometryModelRenderer->renderPlane(shaderShadowMap);
	//glEnable(GL_CULL_FACE);
	renderShadowedScene(shaderShadowMap);
	//renderWorld(camera, fboDummy);
	shaderShadowMap->unUse();

	//glEnable(GL_CULL_FACE);
	
}

void WorldRenderer::renderShadowedScene(Shader* shader)
{
	// floor
	//shader->use();
	// transformation matrices
	glm::mat4 model = glm::mat4(1.0f);

	
	//shader->unUse();
	
	// cubes
	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(0.0f, 1.5f, 0.0));
	model = glm::scale(model, glm::vec3(0.5f));
	shader->setMat4("uModelMatrix", model);
	geometryModelRenderer->renderCube(shader);
	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(2.0f, 0.0f, 1.0));
	model = glm::scale(model, glm::vec3(0.5f));
	shader->setMat4("uModelMatrix", model);
	geometryModelRenderer->renderCube(shader);
	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 2.0));
	model = glm::rotate(model, glm::radians(60.0f), glm::normalize(glm::vec3(1.0, 0.0, 1.0)));
	model = glm::scale(model, glm::vec3(0.25));
	shader->setMat4("uModelMatrix", model);
	geometryModelRenderer->renderCube(shader);
}

void WorldRenderer::renderWorld(Camera *camera, FBO* fbo)
{
	// bind framebuffer
	if(fbo->fboId)
	{
		glBindFramebuffer(GL_FRAMEBUFFER, fbo->fboId);
		//fprintf(fplogfile, "WorldRenderer::WorldRenderer::FBO %u TBO %u\n", fbo->fboId, fbo->tboColorAttachment);
	}
	
	glEnable(GL_DEPTH_TEST);
	glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
	glDisable(GL_CULL_FACE);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	//glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// cubemap
	//cubeMapRenderer->render(camera);

	// render models
	//modelRenderer->render(camera);
	// render points
	//geometryModelRenderer->renderPoints(camera);
	//geometryModelRenderer->renderPlaneWithDirectionalLight(camera, shaderDirectionalLight);

	/*
	glm::mat4 projectionMatrix = this->projectionMatrix;
	glm::mat4 viewMatrix = camera->GetViewMatrix();
	shaderHandle->use();
	shaderHandle->setMat4("uViewMatrix", viewMatrix);
	shaderHandle->setMat4("uProjectionMatrix", projectionMatrix);
	shaderHandle->setVec3("uViewPos", camera->Position);
	//renderWallWithNormalParallaxMap(shaderHandle);
	renderTunnel(shaderHandle);
	shaderHandle->unUse();*/

	cartsRenderer->renderSceneWithWoodenFloor(camera);

	// render carts scene
	//cartsRenderer->renderScene(camera);
	
	// render quad
	//geometryModelRenderer->renderQuad(camera);

	// render instanced
	//geometryModelRenderer->renderQuadInstanced(camera);
	if (fbo->fboId)
	{
		// second pass: Render to a buffer
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
}

void WorldRenderer::performTwoPassGuassianBlur(Shader* shader, FBO* fbo, FBO* fboPing, FBO* fboPong, unsigned int colorBuffer, bool isHdr)
{
	if (fbo->fboId)
	{
		glBindFramebuffer(GL_FRAMEBUFFER, fbo->fboId);
		//fprintf(fplogfile, "WorldRenderer::WorldRenderer::FBO %u TBO %u\n", fbo->fboId, fbo->tboColorAttachment);

	}
	// blur bright fragments with two-pass Gaussian Blur 
	// --------------------------------------------------
	bool horizontal = true, first_iteration = true;
	unsigned int amount = 10;
	FBO* currentFBO;
	unsigned int currentTexture;
	float exposure = 0.5f;
	//glEnable(GL_DEPTH_TEST);
	//glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


	if (isHdr)
	{
		shader = shaderTwoPassGuassianBlur;
		// use shaders
		shader->use();
		shader->setInt("uHdr", isHdr);
		shader->setFloat("uExposure", exposure);
	}
	else
	{
		// use shaders
		shader->use();
	}

	for (unsigned int i = 0; i < amount; i++)
	{
		if (horizontal)
		{
			currentFBO = fboPing;
			currentTexture = fboPong->tboColorAttachment;
		}
		else
		{
			currentFBO = fboPong;
			currentTexture = fboPing->tboColorAttachment;
		}
		glBindFramebuffer(GL_FRAMEBUFFER, currentFBO->fboId);
		shader->setInt("horizontal", horizontal);
		if (first_iteration)
		{
			currentTexture = colorBuffer;
		}
		this->geometryModelRenderer->renderToScreen(shader, currentTexture);
		horizontal = !horizontal;
		if (first_iteration)
			first_iteration = false;
	}

	shader->unUse();
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	// now render floating point color buffer to 2D quad and tonemap HDR colors to default framebuffer's (clamped) color range
	// --------------------------------------------------------------------------------------------------------------------------
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	shaderBloomFinal->use();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, colorBuffer);
	shaderBloomFinal->setInt("texture_diffuse1", 0);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, fboPing->tboColorAttachment);
	shaderBloomFinal->setInt("texture_diffuse2", 1);
	shaderBloomFinal->setFloat("uExposure", exposure);
	shaderBloomFinal->setInt("uBloom", isHdr);
	
	this->geometryModelRenderer->renderToScreen(shaderBloomFinal, colorBuffer);
	shaderBloomFinal->unUse();
	//glDisable(GL_DEPTH_TEST);
	if (fbo->fboId)
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
}

// Render to FBO
void WorldRenderer::renderWorld(Shader* shader, FBO* fbo, unsigned int colorBuffer, bool isHdr)
{
	if (fbo->fboId)
	{
		glBindFramebuffer(GL_FRAMEBUFFER, fbo->fboId);
		//fprintf(fplogfile, "WorldRenderer::WorldRenderer::FBO %u TBO %u\n", fbo->fboId, fbo->tboColorAttachment);

	}
	//glEnable(GL_DEPTH_TEST);
	glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	

	if (isHdr)
	{
		shader = shaderHdr;
		// use shaders
		shader->use();
		float exposure = 0.5f;
		shader->setInt("uHdr", isHdr);
		shader->setFloat("uExposure", exposure);
	}
	else
	{
		// use shaders
		shader->use();
	}
	geometryModelRenderer->renderToScreen(shader, colorBuffer);
	shader->unUse();
	//glDisable(GL_DEPTH_TEST);
	if (fbo->fboId)
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
}

void WorldRenderer::uninitialize()
{
	//cartsRenderer->uninitialize();
	//cubeMapRenderer->uninitialize();
	//modelRenderer->uninitialize();
	geometryModelRenderer->uninitialize();
}

void WorldRenderer::renderWallWithNormalMap(Shader* shaderHandle)
{
	glm::mat4 modelMatrix = glm::mat4(1.0f);
	glm::vec3 lightPos(0.5f, 1.0f, 0.3f);
	glm::vec3 ambientColor = glm::vec3(0.3f);
	glm::vec3 diffuseColor = glm::vec3(1.0f);
	glm::vec3 specularColor = glm::vec3(0.5f, 0.5f, 0.5f);
	glm::vec3 directionVector = glm::vec3(-0.2f, -1.0f, -0.3f);
	modelMatrix = glm::rotate(modelMatrix, glm::radians((float)glfwGetTime() * -10.0f), glm::normalize(glm::vec3(1.0, 0.0, 1.0))); // rotate the quad to show normal mapping from multiple directions

	shaderHandle->setMat4("uModelMatrix", modelMatrix);
	shaderHandle->setVec3("uLightPos", lightPos);

	// light properties
	// phase 1: directional lighting
	shaderHandle->setVec3("dirLight.direction", directionVector);
	shaderHandle->setVec3("dirLight.ambient", ambientColor);
	shaderHandle->setVec3("dirLight.diffuse", diffuseColor);
	shaderHandle->setVec3("dirLight.specular", specularColor);

	// material properties
	shaderHandle->setVec3("uMaterial.ambient", glm::vec3(1.0f, 0.5f, 0.31f));
	shaderHandle->setInt("uMaterial.texture_diffuse1", 0);
	shaderHandle->setVec3("uMaterial.specular", glm::vec3(0.5f));
	shaderHandle->setFloat("uMaterial.shininess", 32.0f);
	shaderHandle->setInt("uMaterial.texture_normal1", 1);
	// draw here
	geometryModelRenderer->renderWall(shaderHandle);
}


void WorldRenderer::renderWallWithNormalParallaxMap(Shader* shaderHandle)
{
	glm::mat4 modelMatrix = glm::mat4(1.0f);
	glm::vec3 lightPos(0.5f, 1.0f, 0.3f);
	glm::vec3 ambientColor = glm::vec3(0.3f);
	glm::vec3 diffuseColor = glm::vec3(1.0f);
	glm::vec3 specularColor = glm::vec3(0.5f, 0.5f, 0.5f);
	glm::vec3 directionVector = glm::vec3(-0.2f, -1.0f, -0.3f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-1.0f, 0.0f, -1.0f));
	modelMatrix = glm::rotate(modelMatrix, glm::radians((float)glfwGetTime() * -10.0f), glm::normalize(glm::vec3(1.0, 0.0, 1.0))); // rotate the quad to show normal mapping from multiple directions

	shaderHandle->setMat4("uModelMatrix", modelMatrix);
	shaderHandle->setVec3("uLightPos", lightPos);

	// light properties
	// phase 1: directional lighting
	shaderHandle->setVec3("dirLight.direction", directionVector);
	shaderHandle->setVec3("dirLight.ambient", ambientColor);
	shaderHandle->setVec3("dirLight.diffuse", diffuseColor);
	shaderHandle->setVec3("dirLight.specular", specularColor);

	// material properties
	shaderHandle->setVec3("uMaterial.ambient", glm::vec3(1.0f, 0.5f, 0.31f));
	shaderHandle->setInt("uMaterial.texture_diffuse1", 0);
	shaderHandle->setVec3("uMaterial.specular", glm::vec3(0.5f));
	shaderHandle->setFloat("uMaterial.shininess", 32.0f);
	shaderHandle->setInt("uMaterial.texture_normal1", 1);
	shaderHandle->setInt("uMaterial.texture_depth1", 2);
	shaderHandle->setFloat("height_scale", 0.1f); // you can vary this for different results
	// draw here
	geometryModelRenderer->renderBricksWallWithNormalAndParallaxMap(shaderHandle);

	// wooden toy
	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(1.0f, 0.0f, -1.0f));
	modelMatrix = glm::rotate(modelMatrix, glm::radians((float)glfwGetTime() * -10.0f), glm::normalize(glm::vec3(1.0, 0.0, 1.0))); // rotate the quad to show normal mapping from multiple directions
	shaderHandle->setMat4("uModelMatrix", modelMatrix);
	geometryModelRenderer->renderWoodenToyBoxWithNormalAndParallaxMap(shaderHandle);
}

void WorldRenderer::renderTunnel(Shader* shaderHandle)
{
	// local variables
	glm::mat4 modelMatrix = glm::mat4(1.0f);
	glm::vec3 ambientColor = glm::vec3(0.1f);
	glm::vec3 diffuseColor = glm::vec3(1.0f);
	glm::vec3 specularColor = glm::vec3(0.5f);
	// positions
	std::vector<glm::vec3> lightPositions;
	lightPositions.push_back(glm::vec3(0.0f, 0.0f, 49.5f)); // back light
	lightPositions.push_back(glm::vec3(-1.4f, -1.9f, 9.0f));
	lightPositions.push_back(glm::vec3(0.0f, -1.8f, 4.0f));
	lightPositions.push_back(glm::vec3(0.8f, -1.7f, 6.0f));
	// colors
	std::vector<glm::vec3> lightColors;
	lightColors.push_back(glm::vec3(200.0f, 200.0f, 200.0f));
	//lightColors.push_back(glm::vec3(1.0f));
	lightColors.push_back(glm::vec3(1.0f, 0.0f, 0.0f));
	lightColors.push_back(glm::vec3(0.0f, 1.0f, 0.0f));
	lightColors.push_back(glm::vec3(0.0f, 0.0f, 1.0f));


	// code
	// translate objects
	modelMatrix = glm::translate(modelMatrix, glm::vec3(0.0f, 0.0f, 25.0));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(2.5f, 2.5f, 27.5f));
	// set Uniform values
	shaderHandle->setMat4("uModelMatrix", modelMatrix);
	shaderHandle->setInt("uInverseNormals", true);
	shaderHandle->setInt("uBlinn", false);
	// light properties
	// phase 1: point lighting
	for (unsigned int i = 0; i < lightPositions.size(); i++)
	{
		// lighting
		shaderHandle->setVec3("pointLights["+std::to_string(i)+"].ambient", ambientColor);
		shaderHandle->setVec3("pointLights[" + std::to_string(i) + "].diffuse", lightColors[i]);
		shaderHandle->setVec3("pointLights[" + std::to_string(i) + "].specular", specularColor);
		// light position
		shaderHandle->setVec3("pointLights[" + std::to_string(i) + "].position", lightPositions[i]);
		shaderHandle->setFloat("pointLights[" + std::to_string(i) + "].constant", 1.0f);
		shaderHandle->setFloat("pointLights[" + std::to_string(i) + "].linear", 0.09f);
		shaderHandle->setFloat("pointLights[" + std::to_string(i) + "].quadratic", 0.032f);
	}
	

	// material properties
	shaderHandle->setVec3("uMaterial.ambient", glm::vec3(0.1f));
	shaderHandle->setInt("uMaterial.texture_diffuse1", 0);
	shaderHandle->setVec3("uMaterial.uSpecular", glm::vec3(0.5f));
	shaderHandle->setFloat("uMaterial.shininess", 32.0f);
	// draw here
	geometryModelRenderer->renderCube(shaderHandle);

}
