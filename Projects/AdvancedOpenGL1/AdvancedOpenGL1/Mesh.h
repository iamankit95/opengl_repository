#ifndef MESH_H
#define MESH_H

#include"Base.h"
#include"Shader.h"
#include"Utils.h"

enum VERTEX_ATTRIBUTES
{
	RSM_VERTEX_POSITION_ATTRIBUTE = 0,
	RSM_VERTEX_COLOR_ATTRIBUTE,
	RSM_VERTEX_NORMAL_ATTRIBUTE,
	RSM_VERTEX_TEXCOORDS_ATTRIBUTE,
	RSM_VERTEX_TANGENT_ATTRIBUTE,
	RSM_VERTEX_BITANGENT_ATTRIBUTE,
	RSM_VERTEX_INSTANCE_ATTRIBUTE,
	// for mat4 i.e. 4 vec4's
	RSM_VERTEX_MAT4_VEC4_1_ATTRIBUTE,
	RSM_VERTEX_MAT4_VEC4_2_ATTRIBUTE,
	RSM_VERTEX_MAT4_VEC4_3_ATTRIBUTE,
	RSM_VERTEX_MAT4_VEC4_4_ATTRIBUTE
};
struct Vertex
{
	glm::vec3 position;
	glm::vec3 color;
	glm::vec3 normal;
	glm::vec2 texCoords;
	glm::vec3 tangent;
	glm::vec3 bitangent;
	glm::vec2 offset;
};

struct Texture
{
	unsigned int id;
	std::string name;
	std::string path;
	std::string type;
};

class Mesh
{
public:
	unsigned int VAO;
	// mesh data
	std::vector<Vertex> vertices;
	std::vector<float> verticesInterleaved;
	std::vector<unsigned int> indices;
	std::vector<Texture> textures;
	/*  Functions  */
	Mesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices, std::vector<Texture> textures);
	Mesh(std::vector<float> vertices, int dataHint, std::vector<Texture> textures);
	Mesh(std::vector<float> vertices, int dataHint, std::vector<Texture> textures, bool isInstanced, std::vector<glm::vec2> translations);
	void Draw(Shader* shader);
	void Draw(Shader* shader, int instances, std::string type);
	void Draw(Shader* shader, bool isInterleaved);
	void Draw(Shader* shader, int instanceCount);
	void DrawCubemap(Shader* shader);
	void DrawPoints(Shader* shader);
	void Draw(Shader* shader, bool isInterleaved, unsigned int tbo);

private:
	// render data
	unsigned int VBO, EBO, stride;
	void setUpMesh();
	void setUpInterleavedData(int dataHint);
	bool isInstanced;
};
#endif // !MESH_H
