
#include"Model.h"

unsigned int TextureFromFile(const char *path, const std::string &directory, bool gamma);
unsigned int loadTexture(const char* imagePath);
Model::Model(const char* name, const char* path)
{
	this->name = name;
	this->isOutlineNeeded = false;
	fprintf(fplogfile, "DEBUG::Model::Model::Preparing model %s \n", name);
	loadModel(path);
}

Model::Model(const char* name, const char* path, int instances, glm::mat4* instacedModelMatrices)
{
	this->name = name;
	this->isOutlineNeeded = false;
	fprintf(fplogfile, "DEBUG::Model::Model::Preparing model %s \n", name);
	loadModel(path);

	// populate instanced data
	// vertex buffer object
	fprintf(fplogfile, "DEBUG::Model::Model::Preparing model %f %f \n", &(instacedModelMatrices), instances);
		/*unsigned int buffer;
		glGenBuffers(1, &buffer);
		glBindBuffer(GL_ARRAY_BUFFER, buffer);
		glBufferData(GL_ARRAY_BUFFER, instances * sizeof(glm::mat4), &instacedModelMatrices[0], GL_STATIC_DRAW);

		for (unsigned int i = 0; i < this->meshes.size(); i++)
		{
			unsigned int VAO = this->meshes[i].VAO;
			glBindVertexArray(VAO);
			glBindBuffer(GL_ARRAY_BUFFER, buffer);
			// vertex attributes
			std::size_t vec4Size = sizeof(glm::vec4);
			glEnableVertexAttribArray(RSM_VERTEX_MAT4_VEC4_1_ATTRIBUTE);
			glVertexAttribPointer(RSM_VERTEX_MAT4_VEC4_1_ATTRIBUTE, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)0);
			glEnableVertexAttribArray(RSM_VERTEX_MAT4_VEC4_2_ATTRIBUTE);
			glVertexAttribPointer(RSM_VERTEX_MAT4_VEC4_2_ATTRIBUTE, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)(1 * vec4Size));
			glEnableVertexAttribArray(RSM_VERTEX_MAT4_VEC4_3_ATTRIBUTE);
			glVertexAttribPointer(RSM_VERTEX_MAT4_VEC4_3_ATTRIBUTE, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)(2 * vec4Size));
			glEnableVertexAttribArray(RSM_VERTEX_MAT4_VEC4_4_ATTRIBUTE);
			glVertexAttribPointer(RSM_VERTEX_MAT4_VEC4_4_ATTRIBUTE, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)(3 * vec4Size));

			glVertexAttribDivisor(RSM_VERTEX_MAT4_VEC4_1_ATTRIBUTE, 1);
			glVertexAttribDivisor(RSM_VERTEX_MAT4_VEC4_2_ATTRIBUTE, 1);
			glVertexAttribDivisor(RSM_VERTEX_MAT4_VEC4_3_ATTRIBUTE, 1);
			glVertexAttribDivisor(RSM_VERTEX_MAT4_VEC4_4_ATTRIBUTE, 1);

			glBindVertexArray(0);
		}*/
	
}

Model::Model(const char* name, std::vector<float> vertices, int dataHint,
	std::vector<Texture> textures)
{
	this->name = name;
	this->isOutlineNeeded = false;
	fprintf(fplogfile, "DEBUG::Model::Model::Preparing model %s \n", name);

	populateIntereavedData(vertices, dataHint, textures);
}

Model::Model(const char* name, std::vector<float> vertices, int value, std::vector<Texture> textures,
	bool isInstanced, std::vector<glm::vec2> translations)
{
	this->name = name;
	this->isOutlineNeeded = false;
	fprintf(fplogfile, "DEBUG::Model::Model::Preparing model %s \n", name);

	populateIntereavedData(vertices, value, textures, isInstanced, translations);
}
Model::Model(const char* name, std::vector<float> vertices, int dataHint,
	unsigned int cubemapTexture)
{
	this->name = name;
	this->isOutlineNeeded = false;
	fprintf(fplogfile, "DEBUG::Model::Model::Preparing model %s \n", name);
	
	std::vector<Texture> textures;
	Texture texture;
	texture.id = cubemapTexture;
	textures.push_back(texture);
	Mesh mesh(vertices, dataHint, textures);
	meshes.push_back(mesh);

	fprintf(fplogfile, "DEBUG::Model::Model::textures data size:%d and meshes:%d\n",
		textures.size(), meshes.size());
}

void Model::Draw(Shader* shader)
{
	for (unsigned int i = 0; i < meshes.size();i++)
	{
		meshes[i].Draw(shader);
	}
}

void Model::Draw(Shader* shader, int instanceCount, glm::mat4* instacedModelMatrices, bool isInterleaved)
{
	if(isInterleaved)
	{
		for (unsigned int i = 0; i < meshes.size();i++)
		{
			meshes[i].Draw(shader, instanceCount);
		}
	}
	else
	{
		unsigned int buffer;
		glGenBuffers(1, &buffer);
		glBindBuffer(GL_ARRAY_BUFFER, buffer);
		glBufferData(GL_ARRAY_BUFFER, instanceCount * sizeof(glm::mat4), &instacedModelMatrices[0], GL_STATIC_DRAW);

		for (unsigned int i = 0; i < this->meshes.size(); i++)
		{
			unsigned int VAO = this->meshes[i].VAO;
			glBindVertexArray(VAO);
			glBindBuffer(GL_ARRAY_BUFFER, buffer);
			// vertex attributes
			std::size_t vec4Size = sizeof(glm::vec4);
			glEnableVertexAttribArray(RSM_VERTEX_MAT4_VEC4_1_ATTRIBUTE);
			glVertexAttribPointer(RSM_VERTEX_MAT4_VEC4_1_ATTRIBUTE, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)0);
			glEnableVertexAttribArray(RSM_VERTEX_MAT4_VEC4_2_ATTRIBUTE);
			glVertexAttribPointer(RSM_VERTEX_MAT4_VEC4_2_ATTRIBUTE, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)(1 * vec4Size));
			glEnableVertexAttribArray(RSM_VERTEX_MAT4_VEC4_3_ATTRIBUTE);
			glVertexAttribPointer(RSM_VERTEX_MAT4_VEC4_3_ATTRIBUTE, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)(2 * vec4Size));
			glEnableVertexAttribArray(RSM_VERTEX_MAT4_VEC4_4_ATTRIBUTE);
			glVertexAttribPointer(RSM_VERTEX_MAT4_VEC4_4_ATTRIBUTE, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)(3 * vec4Size));

			glVertexAttribDivisor(RSM_VERTEX_MAT4_VEC4_1_ATTRIBUTE, 1);
			glVertexAttribDivisor(RSM_VERTEX_MAT4_VEC4_2_ATTRIBUTE, 1);
			glVertexAttribDivisor(RSM_VERTEX_MAT4_VEC4_3_ATTRIBUTE, 1);
			glVertexAttribDivisor(RSM_VERTEX_MAT4_VEC4_4_ATTRIBUTE, 1);

			glBindVertexArray(0);

			meshes[i].Draw(shader, instanceCount, "instaced");
		}

		/*for (unsigned int i = 0; i < meshes.size();i++)
		{
			meshes[i].Draw(shader,instanceCount, "instaced");
		}*/
	}
}

void Model::Draw(Shader* shader, bool isInterleaved)
{
	//fprintf(fplogfile, "Model::Draw::meshes count is %d \n",meshes.size());
	for (unsigned int i = 0; i < meshes.size();i++)
	{
		meshes[i].Draw(shader, isInterleaved);
	}

}



void Model::DrawCubemap(Shader* shader)
{
	for (unsigned int i = 0; i < meshes.size();i++)
	{
		meshes[i].DrawCubemap(shader);
	}
}

void Model::DrawPoints(Shader* shader)
{
	for (unsigned int i = 0; i < meshes.size();i++)
	{
		meshes[i].DrawPoints(shader);
	}
}

void Model::Draw(Shader* shader, bool isInterleaved, unsigned int tbo)
{
	//fprintf(fplogfile, "Model::Draw::meshes count is %d \n",meshes.size());
	for (unsigned int i = 0; i < meshes.size();i++)
	{
		meshes[i].Draw(shader, isInterleaved, tbo);
	}
}

void Model::loadModel(std::string path)
{
	Assimp::Importer importer;
	const aiScene *scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_CalcTangentSpace);
	if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
	{
		fprintf(fplogfile, "ERROR::Model::loadModel::ASSIMP:: %s \n", importer.GetErrorString());
		return;
	}
	else
	{
		directory = path.substr(0, path.find_last_of('/'));
		fprintf(fplogfile, "DEBUG::Model::loadModel::Model %s loaded successfully::directory path::%s \n", path.c_str(), directory.c_str());
	}
	
	processNode(scene->mRootNode, scene);
}

void Model::processNode(aiNode *node, const aiScene *scene)
{
	// process all the node's meshes (if any)
	for (unsigned int i = 0; i < node->mNumMeshes; i++)
	{
		aiMesh *mesh = scene->mMeshes[node->mMeshes[i]];
		meshes.push_back(processMesh(mesh, scene));
	}
	// then do the same for each of its children
	for (unsigned int i = 0; i < node->mNumChildren; i++)
	{
		processNode(node->mChildren[i], scene);
	}
}


Mesh Model::processMesh(aiMesh *mesh, const aiScene *scene)
{
	std::vector<Vertex> vertices;
	std::vector<unsigned int> indices;
	std::vector<Texture> textures;

	for (unsigned int i = 0; i < mesh->mNumVertices; i++)
	{
		Vertex vertex;
		// process vertex positions, normals and texture coordinates
		glm::vec3 vector;
		vector.x = mesh->mVertices[i].x;
		vector.y = mesh->mVertices[i].y;
		vector.z = mesh->mVertices[i].z;
		vertex.position = vector;

		//vertex.color = glm::vec3(1.0f);

		vector.x = mesh->mNormals[i].x;
		vector.y = mesh->mNormals[i].y;
		vector.z = mesh->mNormals[i].z;
		vertex.normal = vector;

		if (mesh->mTextureCoords[0]) // does the mesh contain texture coordinates?
		{
			glm::vec2 vec;
			vec.x = mesh->mTextureCoords[0][i].x;
			vec.y = mesh->mTextureCoords[0][i].y;
			vertex.texCoords = vec;
		}
		else
			vertex.texCoords = glm::vec2(0.0f, 0.0f);

		// tangent
		vector.x = mesh->mTangents[i].x;
		vector.y = mesh->mTangents[i].y;
		vector.z = mesh->mTangents[i].z;
		vertex.tangent = vector;
		// bitangent
		vector.x = mesh->mBitangents[i].x;
		vector.y = mesh->mBitangents[i].y;
		vector.z = mesh->mBitangents[i].z;
		vertex.bitangent = vector;

		vertices.push_back(vertex);
	}
	// process indices
	for (unsigned int i = 0; i < mesh->mNumFaces; i++)
	{
		aiFace face = mesh->mFaces[i];
		for (unsigned int j = 0; j < face.mNumIndices; j++)
			indices.push_back(face.mIndices[j]);
	}

		// process material
	if (mesh->mMaterialIndex >= 0)
	{
		aiMaterial *material = scene->mMaterials[mesh->mMaterialIndex];
		std::vector<Texture> diffuseMaps = loadMaterialTextures(material,
			aiTextureType_DIFFUSE, "texture_diffuse");
		textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());
		std::vector<Texture> specularMaps = loadMaterialTextures(material,
			aiTextureType_SPECULAR, "texture_specular");
		textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
		
		// 3. normal maps
		std::vector<Texture> normalMaps = loadMaterialTextures(material, 
			aiTextureType_HEIGHT, "texture_normal");
		textures.insert(textures.end(), normalMaps.begin(), normalMaps.end());
		// 4. height maps
		std::vector<Texture> heightMaps = loadMaterialTextures(material, 
			aiTextureType_AMBIENT, "texture_height");
		textures.insert(textures.end(), heightMaps.begin(), heightMaps.end());
	}

	fprintf(fplogfile, "DEBUG::Model::processMesh::Mesh:%s - vertices:%d indices:%d textures:%d \n", 
		mesh->mName.C_Str(), vertices.size(), indices.size(), textures.size());
	
	return Mesh(vertices, indices, textures);
}

std::vector<Texture> Model::loadMaterialTextures(aiMaterial *mat, aiTextureType type, std::string typeName)
{
	std::vector<Texture> textures;
	for (unsigned int i = 0; i < mat->GetTextureCount(type); i++)
	{
		aiString str;
		mat->GetTexture(type, i, &str);
		bool skip = false;
		for (unsigned int j = 0; j < textures_loaded.size(); j++)
		{
			fprintf(fplogfile, "DEBUG::Model::loadMaterialTextures::textures_loaded:%s and type:%s\n", textures_loaded[j].path.data(), textures_loaded[j].type.c_str());
			if (std::strcmp(textures_loaded[j].path.data(), str.C_Str()) == 0)
			{
				textures.push_back(textures_loaded[j]);
				skip = true;
				break;
			}
		}
		if (!skip)
		{   // if texture hasn't been loaded already, load it
			Texture texture;
			texture.id = TextureFromFile(str.C_Str(), directory, false);
			texture.type = typeName;
			texture.path = str.C_Str();
			textures.push_back(texture);
			textures_loaded.push_back(texture); // add to loaded textures
		}
	}
	return textures;
}

void Model::populateIntereavedData(std::vector<float> vertices, int dataHint, 
	std::vector<Texture> textures)
{
	for (int i = 0; i < textures.size(); i++)
	{
		textures[i].id = loadTexture(textures[i].path.c_str());
	}
	Mesh mesh(vertices, dataHint, textures);
	meshes.push_back(mesh);
	fprintf(fplogfile, "DEBUG::Model::populateIntereavedData::textures data size:%d and meshes:%d\n", 
		textures.size(), meshes.size());

}


void Model::populateIntereavedData(std::vector<float> vertices, int dataHint,
	std::vector<Texture> textures, bool isInstanced, std::vector<glm::vec2> translations)
{
	for (int i = 0; i < textures.size(); i++)
	{
		textures[i].id = loadTexture(textures[i].path.c_str());
	}
	Mesh mesh(vertices, dataHint, textures, isInstanced, translations);
	meshes.push_back(mesh);
	fprintf(fplogfile, "DEBUG::Model::populateIntereavedData::textures data size:%d and meshes:%d\n",
		textures.size(), meshes.size());
}
