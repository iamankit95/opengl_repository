#include"CartsRenderer.h"
#include"ModelData.h"

CartsRenderer::CartsRenderer(glm::mat4 projMat)
{
	// set projection matrix
	projectionMatrix = projMat;
}

CartsRenderer::CartsRenderer(glm::mat4 projMat, GeometryRenderer* geometryRenderer)
{
	this->projectionMatrix = projMat;
	this->geometryRenderer = geometryRenderer;
}

void CartsRenderer::initialize()
{

	// renderbuffer ends
	lightingShader = new Shader("lightingShader", "resources/shaders/vsLights.glsl", "resources/shaders/fsLights.glsl");
	lampShader = new Shader("lampShader", "resources/shaders/vsLightSource.glsl", "resources/shaders/fsLightSource.glsl");
	//lightingShader = new Shader("vsLights.glsl", "fsLights.glsl");
	//lampShader = new Shader("vertexShader_Pos.vs", "fragmentShader_Pos.fs");
	shaderLampMRT = new Shader("lampShader", "resources/shaders/vsLightSource.glsl", "resources/shaders/fsLightSourceMRT.glsl");

	modelShader = new Shader("modelShader", "resources/shaders/vsModelLoading.glsl", "resources/shaders/fsModelLoading.glsl");
	singleColorShader = new Shader("singleColorShader", "resources/shaders/vsModelLoading.glsl", "resources/shaders/fsSingleColor.glsl");
	shaderPointLighting = new Shader("shaderPointLighting", "resources/shaders/vsLights.glsl", "resources/shaders/fsLightsPoint.glsl");
	shaderPointLightingMRT = new Shader("shaderPointLightingMRT", "resources/shaders/vsLights.glsl", "resources/shaders/fsLightsPointMRT.glsl");

	std::string modelPath = "resources/model/nanosuit/nanosuit.obj";
	//ourModel = new Model("nanosuit", "resources/model/nanosuit/nanosuit.obj");

	// cube model
	std::vector<Texture> textures;
	Texture textureContainer2;
	textureContainer2.name = "container2.png";
	textureContainer2.path = "resources/textures/container2.png";
	textureContainer2.type = "texture_diffuse";
	Texture textureContainer2_Specular;
	textureContainer2_Specular.name = "container2_specular.png";
	textureContainer2_Specular.path = "resources/textures/container2_specular.png";
	textureContainer2_Specular.type = "texture_specular";
	textures.push_back(textureContainer2);
	textures.push_back(textureContainer2_Specular);
	cubeModels = new Model("cube", cubeVerticesPNT, 101100, textures);
	textures.clear();

	// ground cube
	Texture textureGround;
	textureGround.name = "marble.jpg";
	textureGround.path = "resources/textures/marble.jpg";
	textureGround.type = "texture_diffuse";
	textures.push_back(textureGround);
	groundCubeModels = new Model("groundCubeModels", cubeVerticesPT_CCW, 100100, textures);
	textures.clear();

	// floor quad 
	Texture textureFloor;
	textureFloor.name = "metal.png";
	textureFloor.path = "resources/textures/metal.png";
	textureFloor.type = "texture_diffuse";
	textures.push_back(textureFloor);
	floorQuadModels = new Model("floorQuadModels", floorQuadVerticesPT, 100100, textures);
	textures.clear();

	// grass quad model
	Texture textureGrass;
	textureGrass.name = "marble.jpg";
	textureGrass.path = "resources/textures/grass.png";
	textureGrass.type = "texture_diffuse";
	textures.push_back(textureGrass);
	grassQuadModels = new Model("grassQuadModels", grassQuadVerticesPT, 100100, textures);
	textures.clear();


	Texture textureTransWindow;
	textureTransWindow.name = "blending_transparent_window.png";
	textureTransWindow.path = "resources/textures/blending_transparent_window.png";
	textureTransWindow.type = "texture_diffuse";
	textures.push_back(textureTransWindow);
	transperentWindowQuadModels = new Model("transperentWindowQuadModels", grassQuadVerticesPT, 100100, textures);
	textures.clear();
}

void CartsRenderer::renderScene(Camera *camera)
{
	
	// transformation matrices
	glm::mat4 modelMatrix = glm::mat4(1.0f);
	glm::mat4 viewMatrix = glm::mat4(1.0f);
	glm::vec3 lightColor = glm::vec3(1.0f);
	glm::vec3 diffuseColor = glm::vec3(1.0f);
	//View Transformation
	viewMatrix = camera->GetViewMatrix();


	glStencilMask(0x00); // make sure we don't update the stencil buffer while drawing the floor
						 /////use program object


						 // Object preparation
	lightingShader->use();
	//setting uniforms
	lightingShader->setMat4("uViewMatrix", viewMatrix);
	lightingShader->setMat4("uProjectionMatrix", projectionMatrix);
	lightingShader->setVec3("uViewPos", camera->Position);


	//lightColor.x = sin(glfwGetTime() * 2.0f);
	//lightColor.y = sin(glfwGetTime() * 0.7f);
	//lightColor.z = sin(glfwGetTime() * 1.3f);

	glm::vec3 ambientColor = glm::vec3(0.05f, 0.05f, 0.05f);
	diffuseColor = glm::vec3(0.4f, 0.4f, 0.4f);
	glm::vec3 specularColor = glm::vec3(0.5f, 0.5f, 0.5f);
	glm::vec3 directionVector = glm::vec3(-0.2f, -1.0f, -0.3f);
	// light properties
	// phase 1: directional lighting
	lightingShader->setVec3("dirLight.direction", directionVector);
	lightingShader->setVec3("dirLight.ambient", ambientColor);
	lightingShader->setVec3("dirLight.diffuse", diffuseColor);
	lightingShader->setVec3("dirLight.specular", specularColor);

	// phase 2: point light
	ambientColor = glm::vec3(0.05f, 0.05f, 0.05f);
	diffuseColor = glm::vec3(0.2f, 0.2f, 0.9);
	specularColor = glm::vec3(1.0f, 1.0f, 1.0f);
	lightingShader->setVec3("pointLights[0].ambient", ambientColor);
	lightingShader->setVec3("pointLights[0].diffuse", diffuseColor);
	lightingShader->setVec3("pointLights[0].specular", specularColor);
	lightingShader->setVec3("pointLights[0].position", pointLightPositions[0]);
	lightingShader->setFloat("pointLights[0].constant", 1.0f);
	lightingShader->setFloat("pointLights[0].linear", 0.09f);
	lightingShader->setFloat("pointLights[0].quadratic", 0.032f);

	diffuseColor = glm::vec3(1.0f, 1.0f, 0.0f);
	lightingShader->setVec3("pointLights[1].ambient", ambientColor);
	lightingShader->setVec3("pointLights[1].diffuse", diffuseColor);
	lightingShader->setVec3("pointLights[1].specular", specularColor);
	lightingShader->setVec3("pointLights[1].position", pointLightPositions[1]);
	lightingShader->setFloat("pointLights[1].constant", 1.0f);
	lightingShader->setFloat("pointLights[1].linear", 0.09f);
	lightingShader->setFloat("pointLights[1].quadratic", 0.032f);

	diffuseColor = glm::vec3(1.0f, 0.0f, 1.0f);
	lightingShader->setVec3("pointLights[2].ambient", ambientColor);
	lightingShader->setVec3("pointLights[2].diffuse", diffuseColor);
	lightingShader->setVec3("pointLights[2].specular", specularColor);
	lightingShader->setVec3("pointLights[2].position", pointLightPositions[2]);
	lightingShader->setFloat("pointLights[2].constant", 1.0f);
	lightingShader->setFloat("pointLights[2].linear", 0.09f);
	lightingShader->setFloat("pointLights[2].quadratic", 0.032f);

	diffuseColor = glm::vec3(1.0f, 0.0f, 0.0f);
	lightingShader->setVec3("pointLights[3].ambient", ambientColor);
	lightingShader->setVec3("pointLights[3].diffuse", diffuseColor);
	lightingShader->setVec3("pointLights[3].specular", specularColor);
	lightingShader->setVec3("pointLights[3].position", pointLightPositions[3]);
	lightingShader->setFloat("pointLights[3].constant", 1.0f);
	lightingShader->setFloat("pointLights[3].linear", 0.09f);
	lightingShader->setFloat("pointLights[3].quadratic", 0.032f);

	// flash/spot light
	ambientColor = glm::vec3(0.0f, 0.0f, 0.0f);
	diffuseColor = glm::vec3(0.0f, 1.0f, 0.0f);
	specularColor = glm::vec3(1.0f, 1.0f, 1.0f);
	lightingShader->setVec3("spotLight.direction", camera->Front);
	lightingShader->setVec3("spotLight.position", camera->Position);
	lightingShader->setFloat("spotLight.cutoff", glm::cos(glm::radians(12.5f)));
	lightingShader->setFloat("spotLight.outerCutoff", glm::cos(glm::radians(15.0f)));
	lightingShader->setVec3("spotLight.ambient", ambientColor);
	lightingShader->setVec3("spotLight.diffuse", diffuseColor);
	lightingShader->setVec3("spotLight.specular", specularColor);

	// material properties
	lightingShader->setVec3("uMaterial.ambient", glm::vec3(1.0f, 0.5f, 0.31f));
	lightingShader->setInt("uMaterial.diffuse", 0);
	lightingShader->setInt("uMaterial.specular", 1);
	lightingShader->setFloat("uMaterial.shininess", 32.0f);

	for (int i = 0;i < 10;i++)
	{
		modelMatrix = glm::mat4(1.0f);
		modelMatrix = glm::translate(modelMatrix, cubePositions[i]);
		float angle = 20.0f * i;
		modelMatrix = glm::rotate(modelMatrix, glm::radians(angle), glm::vec3(1.0f, 0.3f, 0.5f));
		//modelMatrix = glm::scale(modelMatrix, glm::vec3(1.0f, 1.0f, 1.0f));
		lightingShader->setMat4("uModelMatrix", modelMatrix);
		//glDrawArrays(GL_TRIANGLES, 0, 36);
		cubeModels->Draw(lightingShader, true);
	}
	lightingShader->unUse();

	// draw lamp
	lampShader->use();
	lampShader->setMat4("uViewMatrix", viewMatrix);
	lampShader->setMat4("uProjectionMatrix", projectionMatrix);

	//glBindVertexArray(VAO);
	for (int i = 0; i < 4; i++)
	{
		modelMatrix = glm::mat4(1.0);
		modelMatrix = glm::translate(modelMatrix, pointLightPositions[i]);
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.2f));
		lampShader->setMat4("uModelMatrix", modelMatrix);

		if (i == 0)
		{
			diffuseColor = glm::vec3(0.2f, 0.2f, 0.9f);
			lampShader->setVec3("uLightColor", lightColor);
			lampShader->setVec3("uObjectColor", diffuseColor);
		}
		else if (i == 1)
		{
			diffuseColor = glm::vec3(1.0f, 1.0f, 0.0f);
			lampShader->setVec3("uLightColor", lightColor);
			lampShader->setVec3("uObjectColor", diffuseColor);
		}
		else if (i == 2)
		{
			diffuseColor = glm::vec3(1.0f, 0.0f, 1.0f);
			lampShader->setVec3("uLightColor", lightColor);
			lampShader->setVec3("uObjectColor", diffuseColor);
		}
		else if (i == 3)
		{
			diffuseColor = glm::vec3(1.0f, 0.0f, 0.0f);
			lampShader->setVec3("uLightColor", lightColor);
			lampShader->setVec3("uObjectColor", diffuseColor);
		}
		cubeModels->Draw(lampShader, true);
	}

	lampShader->unUse();


	/*
	modelShader->use();
	modelMatrix = glm::mat4(1.0f);
	//viewMatrix = glm::mat4(1.0f);
	//projectionMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(0.0f, -2.0f, -2.0f));
	//modelMatrix = glm::rotate(modelMatrix, glm::radians(90.0f), glm::vec3(1.0f, 0.3f, 0.5f));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(0.2f));
	modelShader->setMat4("uModelMatrix", modelMatrix);
	modelShader->setMat4("uViewMatrix", viewMatrix);
	modelShader->setMat4("uProjectionMatrix", projectionMatrix);
	ourModel->Draw(modelShader);
	//modelShader->unUse();
	*/

	// render ground cubes
	modelShader->use();

	modelShader->setMat4("uViewMatrix", viewMatrix);
	modelShader->setMat4("uProjectionMatrix", projectionMatrix);



	// floor
	modelMatrix = glm::mat4(1.0f);
	modelShader->setMat4("uModelMatrix", modelMatrix);
	floorQuadModels->Draw(modelShader, true);

	glEnable(GL_CULL_FACE);
	glStencilFunc(GL_ALWAYS, 1, 0xFF); // all fragments should pass the stencil test
	glStencilMask(0xFF); // enable writing to the stencil buffer
						 // cube 1
	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-3.0f, 0.0f, -1.0f));
	modelShader->setMat4("uModelMatrix", modelMatrix);
	groundCubeModels->Draw(modelShader, true);
	// cube 2
	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(2.0f, 0.0f, 0.0f));
	modelShader->setMat4("uModelMatrix", modelMatrix);
	groundCubeModels->Draw(modelShader, true);

	for (unsigned int i = 0; i < vegetation.size(); i++)
	{
		modelMatrix = glm::mat4(1.0f);
		modelMatrix = glm::translate(modelMatrix, vegetation[i]);
		modelShader->setMat4("uModelMatrix", modelMatrix);
		grassQuadModels->Draw(modelShader, true);
	}

	modelShader->unUse();
	glDisable(GL_CULL_FACE);
	// border outline starts
	glStencilFunc(GL_NOTEQUAL, 1, 0xFF);
	glStencilMask(0x00); // disable writing to the stencil buffer
						 //glDisable(GL_DEPTH_TEST);
	singleColorShader->use();
	singleColorShader->setMat4("uViewMatrix", viewMatrix);
	singleColorShader->setMat4("uProjectionMatrix", projectionMatrix);



	// cube 1
	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-3.0f, 0.0f, -1.0f));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(1.1f));
	singleColorShader->setMat4("uModelMatrix", modelMatrix);
	groundCubeModels->Draw(singleColorShader, true);
	// cube 2
	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(2.0f, 0.0f, 0.0f));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(1.1f));
	singleColorShader->setMat4("uModelMatrix", modelMatrix);
	groundCubeModels->Draw(singleColorShader, true);
	singleColorShader->unUse();

	glStencilMask(0xFF);
	glStencilFunc(GL_ALWAYS, 1, 0xFF);
	//glEnable(GL_DEPTH_TEST);
	// border outline ends

	// grass models
	modelShader->use();
	modelShader->setMat4("uViewMatrix", viewMatrix);
	modelShader->setMat4("uProjectionMatrix", projectionMatrix);

	// draw transparent windows
	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-1.0f, 0.0f, -2.0f));
	modelShader->setMat4("uModelMatrix", modelMatrix);
	transperentWindowQuadModels->Draw(modelShader, true);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(1.5f, 0.0f, 1.0f));
	modelShader->setMat4("uModelMatrix", modelMatrix);
	transperentWindowQuadModels->Draw(modelShader, true);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-1.5f, 0.0f, 2.0f));
	modelShader->setMat4("uModelMatrix", modelMatrix);
	transperentWindowQuadModels->Draw(modelShader, true);

	for (unsigned int i = 0; i < vegetation.size(); i++)
	{
		modelMatrix = glm::mat4(1.0f);
		modelMatrix = glm::translate(modelMatrix, vegetation[i]);
		modelShader->setMat4("uModelMatrix", modelMatrix);
		grassQuadModels->Draw(modelShader, true);
	}

	modelShader->unUse();
}

void CartsRenderer::renderSceneWithWoodenFloor(Camera* camera)
{
	// local variables
	glm::vec3 ambientColor = glm::vec3(0.1f);
	glm::vec3 specularColor = glm::vec3(0.5f);
	// lighting info
	// -------------
	// positions
	std::vector<glm::vec3> lightPositions;
	lightPositions.push_back(glm::vec3(0.0f, 0.5f, 1.5f));
	lightPositions.push_back(glm::vec3(-4.0f, 0.5f, -3.0f));
	lightPositions.push_back(glm::vec3(3.0f, 0.5f, 1.0f));
	lightPositions.push_back(glm::vec3(-.8f, 2.4f, -1.0f));
	// colors
	std::vector<glm::vec3> lightColors;
	lightColors.push_back(glm::vec3(2.0f));
	lightColors.push_back(glm::vec3(10.0f, 0.0f, 0.0f));
	lightColors.push_back(glm::vec3(0.0f, 0.0f, 2.0f));
	lightColors.push_back(glm::vec3(0.0f, 10.0f, 10.0f));
	// transformation matrices
	glm::mat4 modelMatrix = glm::mat4(1.0f);
	glm::mat4 projectionMatrix = this->projectionMatrix;
	glm::mat4 viewMatrix = camera->GetViewMatrix();

	// code
	shaderPointLightingMRT->use();
	shaderPointLightingMRT->setMat4("uViewMatrix", viewMatrix);
	shaderPointLightingMRT->setMat4("uProjectionMatrix", projectionMatrix);
	shaderPointLightingMRT->setVec3("uViewPos", camera->Position);
	// light properties
	// phase 1: point lighting
	for (unsigned int i = 0; i < lightPositions.size(); i++)
	{
		// lighting
		shaderPointLightingMRT->setVec3("pointLights[" + std::to_string(i) + "].ambient", ambientColor);
		shaderPointLightingMRT->setVec3("pointLights[" + std::to_string(i) + "].diffuse", lightColors[i]);
		shaderPointLightingMRT->setVec3("pointLights[" + std::to_string(i) + "].specular", specularColor);
		// light position
		shaderPointLightingMRT->setVec3("pointLights[" + std::to_string(i) + "].position", lightPositions[i]);
		shaderPointLightingMRT->setFloat("pointLights[" + std::to_string(i) + "].constant", 1.0f);
		shaderPointLightingMRT->setFloat("pointLights[" + std::to_string(i) + "].linear", 0.09f);
		shaderPointLightingMRT->setFloat("pointLights[" + std::to_string(i) + "].quadratic", 0.032f);
	}


	// material properties
	shaderPointLightingMRT->setVec3("uMaterial.ambient", glm::vec3(0.1f));
	shaderPointLightingMRT->setInt("uMaterial.texture_diffuse1", 0);
	shaderPointLightingMRT->setInt("uMaterial.specular", 1);
	shaderPointLightingMRT->setFloat("uMaterial.shininess", 32.0f);

	modelMatrix = glm::translate(modelMatrix, glm::vec3(0.0f, -1.0f, 0.0));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(12.5f, 0.5f, 12.5f));
	shaderPointLightingMRT->setMat4("uModelMatrix", modelMatrix);
	// create one large cube that acts as the floor
	this->geometryRenderer->renderPlane(shaderPointLightingMRT);
	// then create multiple cubes as the scenery
	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(0.0f, 1.5f, 0.0));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(0.5f));
	shaderPointLightingMRT->setMat4("uModelMatrix", modelMatrix);
	this->geometryRenderer->renderCube(shaderPointLightingMRT);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(2.0f, 0.0f, 1.0));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(0.5f));
	shaderPointLightingMRT->setMat4("uModelMatrix", modelMatrix);
	this->geometryRenderer->renderCube(shaderPointLightingMRT);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-1.0f, -1.0f, 2.0));
	modelMatrix = glm::rotate(modelMatrix, glm::radians(60.0f), glm::normalize(glm::vec3(1.0, 0.0, 1.0)));
	shaderPointLightingMRT->setMat4("uModelMatrix", modelMatrix);
	this->geometryRenderer->renderCube(shaderPointLightingMRT);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(0.0f, 2.7f, 4.0));
	modelMatrix = glm::rotate(modelMatrix, glm::radians(23.0f), glm::normalize(glm::vec3(1.0, 0.0, 1.0)));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(1.25));
	shaderPointLightingMRT->setMat4("uModelMatrix", modelMatrix);
	this->geometryRenderer->renderCube(shaderPointLightingMRT);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-2.0f, 1.0f, -3.0));
	modelMatrix = glm::rotate(modelMatrix, glm::radians(124.0f), glm::normalize(glm::vec3(1.0, 0.0, 1.0)));
	shaderPointLightingMRT->setMat4("uModelMatrix", modelMatrix);
	this->geometryRenderer->renderCube(shaderPointLightingMRT);

	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-3.0f, 0.0f, 0.0));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(0.5f));
	shaderPointLightingMRT->setMat4("uModelMatrix", modelMatrix);
	this->geometryRenderer->renderCube(shaderPointLightingMRT);
	shaderPointLightingMRT->unUse();

	// finally show all the light sources as bright cubes
	shaderLampMRT->use();
	shaderLampMRT->setMat4("uViewMatrix", viewMatrix);
	shaderLampMRT->setMat4("uProjectionMatrix", projectionMatrix);
	glm::vec3 diffuseColor = glm::vec3(1.0f);
	for (unsigned int i = 0; i < lightPositions.size(); i++)
	{
		modelMatrix = glm::mat4(1.0f);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(lightPositions[i]));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.25f));
		shaderLampMRT->setMat4("uModelMatrix", modelMatrix);
		shaderLampMRT->setVec3("uLightColor", lightColors[i]);
		shaderLampMRT->setVec3("uObjectColor", diffuseColor);
		this->geometryRenderer->renderCube(shaderLampMRT);
	}

	shaderLampMRT->unUse();
}

void CartsRenderer::uninitialize()
{
	if (lightingShader)
		delete(lightingShader);

	if (lampShader)
		delete(lampShader);

	if (modelShader)
		delete(modelShader);

	if (ourModel)
		delete(ourModel);
}
