#include"GeometryRenderer.h"

GeometryRenderer::GeometryRenderer(glm::mat4 projMat)
{
	this->projectionMatrix = projMat;

	points = {
		-0.5f,  0.5f, 0.0f,   1.0f, 0.0f, 0.0f,// top-left
		0.5f,  0.5f, 0.0f,    0.0f, 1.0f, 0.0f,// top-right
		0.5f, -0.5f, 0.0f,    0.0f, 0.0f, 1.0f,// bottom-right
		-0.5f, -0.5f , 0.0f,  1.0f, 1.0f, 0.0f  // bottom-left
	};
	
	
	quadVerticesPT =
	{
		// positions		// color					// texCoords
		-1.0f,  1.0f, 0.0f, 1.0f, 0.0f, 0.0f,	 0.0f, 1.0f,
		-1.0f, -1.0f, 0.0f,  0.0f, 1.0f, 0.0f,	0.0f, 0.0f,
		1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f,	1.0f, 0.0f,

		-1.0f,  1.0f, 0.0f,  1.0f, 0.0f, 0.0f,	0.0f, 1.0f,
		1.0f, -1.0f,  0.0f, 0.0f, 1.0f, 0.0f,	1.0f, 0.0f,
		1.0f,  1.0f,  0.0f, 0.0f, 1.0f, 1.0f,	1.0f, 1.0f
	};

	quadVerticesInstancesPC =
	{
		// positions     // colors
		-0.05f,  0.05f, 0.0f, 1.0f, 0.0f, 0.0f,
		0.05f, -0.05f, 0.0f, 0.0f, 1.0f, 0.0f,
		-0.05f, -0.05f, 0.0f, 0.0f, 0.0f, 1.0f,

		-0.05f,  0.05f, 0.0f, 1.0f, 0.0f, 0.0f,
		0.05f, -0.05f, 0.0f, 0.0f, 1.0f, 0.0f,
		0.05f,  0.05f, 0.0f, 0.0f, 1.0f, 1.0f
	};

	planeVerticesInstancesPNT = 
	{
		// positions            // normals         // texcoords
		25.0f, -0.5f,  25.0f,  0.0f, 1.0f, 0.0f,  25.0f,  0.0f,
		-25.0f, -0.5f,  25.0f,  0.0f, 1.0f, 0.0f,   0.0f,  0.0f,
		-25.0f, -0.5f, -25.0f,  0.0f, 1.0f, 0.0f,   0.0f, 25.0f,

		25.0f, -0.5f,  25.0f,  0.0f, 1.0f, 0.0f,  25.0f,  0.0f,
		-25.0f, -0.5f, -25.0f,  0.0f, 1.0f, 0.0f,   0.0f, 25.0f,
		25.0f, -0.5f, -25.0f,  0.0f, 1.0f, 0.0f,  25.0f, 25.0f
	};

	cubeVerticesPNT = {
		// back face
		-1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 0.0f, // bottom-left
		1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 1.0f, // top-right
		1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 0.0f, // bottom-right         
		1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 1.0f, // top-right
		-1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 0.0f, // bottom-left
		-1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 1.0f, // top-left
		// front face
		-1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, // bottom-left
		1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 0.0f, // bottom-right
		1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 1.0f, // top-right
		1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 1.0f, // top-right
		-1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 1.0f, // top-left
		-1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, // bottom-left
		// left face
		-1.0f,  1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-right
		-1.0f,  1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 1.0f, // top-left
		-1.0f, -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-left
		-1.0f, -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-left
		-1.0f, -1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 0.0f, // bottom-right
		-1.0f,  1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-right
		// right face
		1.0f,  1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-left
		1.0f, -1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-right
		1.0f,  1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 1.0f, // top-right         
		1.0f, -1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-right
		1.0f,  1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-left
		1.0f, -1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 0.0f, // bottom-left     
		// bottom face
		-1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 1.0f, // top-right
		1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 1.0f, // top-left
		1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f, // bottom-left
		1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f, // bottom-left
		-1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 0.0f, // bottom-right
		-1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 1.0f, // top-right
		// top face
		-1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f, // top-left
		1.0f,  1.0f , 1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f, // bottom-right
		1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 1.0f, // top-right     
		1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f, // bottom-right
		-1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f, // top-left
		-1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 0.0f  // bottom-left 
	};

	// for instancing
	//int index = 0;
	float offset = 0.1f;
	for (int y = -10; y < 10; y += 2)
	{
		for (int x = -10; x < 10; x += 2)
		{
			glm::vec2 translation;
			translation.x = (float)x/10.0f + offset;
			translation.y = (float)y / 10.0f + offset;
			//translations[index++] = translation;
			translations.push_back(translation);
		}
	}

	// for quad with tangent and bitangent calculations start
	// positions
	glm::vec3 pos1(-1.0f, 1.0f, 0.0f);
	glm::vec3 pos2(-1.0f, -1.0f, 0.0f);
	glm::vec3 pos3(1.0f, -1.0f, 0.0f);
	glm::vec3 pos4(1.0f, 1.0f, 0.0f);
	// texture coordinates
	glm::vec2 uv1(0.0f, 1.0f);
	glm::vec2 uv2(0.0f, 0.0f);
	glm::vec2 uv3(1.0f, 0.0f);
	glm::vec2 uv4(1.0f, 1.0f);
	// normal vector
	glm::vec3 nm(0.0f, 0.0f, 1.0f);

	// calculate tangent/bitangent vectors of both triangles
	glm::vec3 tangent1, bitangent1;
	glm::vec3 tangent2, bitangent2;
	// triangle 1
	// ----------
	glm::vec3 edge1 = pos2 - pos1;
	glm::vec3 edge2 = pos3 - pos1;
	glm::vec2 deltaUV1 = uv2 - uv1;
	glm::vec2 deltaUV2 = uv3 - uv1;

	GLfloat f = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y);

	tangent1.x = f * (deltaUV2.y * edge1.x - deltaUV1.y * edge2.x);
	tangent1.y = f * (deltaUV2.y * edge1.y - deltaUV1.y * edge2.y);
	tangent1.z = f * (deltaUV2.y * edge1.z - deltaUV1.y * edge2.z);
	tangent1 = glm::normalize(tangent1);

	bitangent1.x = f * (-deltaUV2.x * edge1.x + deltaUV1.x * edge2.x);
	bitangent1.y = f * (-deltaUV2.x * edge1.y + deltaUV1.x * edge2.y);
	bitangent1.z = f * (-deltaUV2.x * edge1.z + deltaUV1.x * edge2.z);
	bitangent1 = glm::normalize(bitangent1);

	// triangle 2
	// ----------
	edge1 = pos3 - pos1;
	edge2 = pos4 - pos1;
	deltaUV1 = uv3 - uv1;
	deltaUV2 = uv4 - uv1;

	f = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y);

	tangent2.x = f * (deltaUV2.y * edge1.x - deltaUV1.y * edge2.x);
	tangent2.y = f * (deltaUV2.y * edge1.y - deltaUV1.y * edge2.y);
	tangent2.z = f * (deltaUV2.y * edge1.z - deltaUV1.y * edge2.z);
	tangent2 = glm::normalize(tangent2);


	bitangent2.x = f * (-deltaUV2.x * edge1.x + deltaUV1.x * edge2.x);
	bitangent2.y = f * (-deltaUV2.x * edge1.y + deltaUV1.x * edge2.y);
	bitangent2.z = f * (-deltaUV2.x * edge1.z + deltaUV1.x * edge2.z);
	bitangent2 = glm::normalize(bitangent2);


	quadVerticesPNTTB = {
		// positions            // normal         // texcoords  // tangent                          // bitangent
		pos1.x, pos1.y, pos1.z, nm.x, nm.y, nm.z, uv1.x, uv1.y, tangent1.x, tangent1.y, tangent1.z, bitangent1.x, bitangent1.y, bitangent1.z,
		pos2.x, pos2.y, pos2.z, nm.x, nm.y, nm.z, uv2.x, uv2.y, tangent1.x, tangent1.y, tangent1.z, bitangent1.x, bitangent1.y, bitangent1.z,
		pos3.x, pos3.y, pos3.z, nm.x, nm.y, nm.z, uv3.x, uv3.y, tangent1.x, tangent1.y, tangent1.z, bitangent1.x, bitangent1.y, bitangent1.z,

		pos1.x, pos1.y, pos1.z, nm.x, nm.y, nm.z, uv1.x, uv1.y, tangent2.x, tangent2.y, tangent2.z, bitangent2.x, bitangent2.y, bitangent2.z,
		pos3.x, pos3.y, pos3.z, nm.x, nm.y, nm.z, uv3.x, uv3.y, tangent2.x, tangent2.y, tangent2.z, bitangent2.x, bitangent2.y, bitangent2.z,
		pos4.x, pos4.y, pos4.z, nm.x, nm.y, nm.z, uv4.x, uv4.y, tangent2.x, tangent2.y, tangent2.z, bitangent2.x, bitangent2.y, bitangent2.z
	};
	// for quad with tangent and bitangent calculations end
}

void GeometryRenderer::initialize()
{
	// points
	std::vector<Texture> textures;

	// shaders
	shaderTwoDimentionalPosition = new Shader("shaderTwoDimentionalPosition", "resources/shaders/vsColorPass.glsl", "resources/shaders/gsPointToLine.glsl","resources/shaders/fsMultiColor.glsl");
	shaderQuadInstanced = new Shader("shaderQuadInstanced", "resources/shaders/vsTwoDimPos.glsl", "resources/shaders/fsSampleColor.glsl");
	shaderQuad = new Shader("shaderQuad", "resources/shaders/vsColorPass.glsl", "resources/shaders/fsSampleColor.glsl");
	shaderPointLight = new Shader("shaderPointLight", "resources/shaders/vsLights.glsl", "resources/shaders/fsLightsPoint.glsl");
	shaderLamp = new Shader("shaderLamp", "resources/shaders/vsLightSource.glsl", "resources/shaders/fsLightSource.glsl");
	
	// models
	modelPoints = new Model("modelPoints", points, 110000, textures);
	modelQuadRenderToScreen = new Model("modelQuadRenderToScreen", quadVerticesPT, 110100, textures);
	// isInstaced and translations
	screenQuadModels = new Model("screenQuadModels", quadVerticesInstancesPC, 110000, textures, true, translations);
		
	// plane
	Texture texturePlane;
	texturePlane.name = "wood.png";
	texturePlane.path = "resources/textures/wood.png";
	texturePlane.type = "texture_diffuse";
	textures.push_back(texturePlane);
	modelPlane = new Model("modelPlane", planeVerticesInstancesPNT, 101100, textures);
	textures.clear();

	// lamp model
	Texture textureContainer2;
	textureContainer2.name = "container2.png";
	textureContainer2.path = "resources/textures/container2.png";
	textureContainer2.type = "texture_diffuse";
	Texture textureContainer2_Specular;
	textureContainer2_Specular.name = "container2_specular.png";
	textureContainer2_Specular.path = "resources/textures/container2_specular.png";
	textureContainer2_Specular.type = "texture_specular";
	textures.push_back(textureContainer2);
	textures.push_back(textureContainer2_Specular);
	modelCube = new Model("cube", cubeVerticesPNT, 101100, textures);
	textures.clear();


	// wall model - quad - normal mapping
	Texture textureBrickWall;
	textureBrickWall.name = "brickwall.jpg";
	textureBrickWall.path = "resources/textures/brickwall.jpg";
	textureBrickWall.type = "texture_diffuse";
	textures.push_back(textureBrickWall);
	Texture textureBrickwallNormalMap;
	textureBrickwallNormalMap.name = "brickwall_normal.jpg";
	textureBrickwallNormalMap.path = "resources/textures/brickwall_normal.jpg";
	textureBrickwallNormalMap.type = "texture_normal";
	textures.push_back(textureBrickwallNormalMap);
	modelWallQuad = new Model("modelWallQuad", quadVerticesPNTTB, 101111, textures);
	textures.clear();

	// bricks model - normal and parallax mapping
	Texture textureBrick2Wall;
	textureBrick2Wall.name = "bricks2.jpg";
	textureBrick2Wall.path = "resources/textures/bricks2.jpg";
	textureBrick2Wall.type = "texture_diffuse";
	textures.push_back(textureBrick2Wall);
	Texture textureBrick2wallNormalMap;
	textureBrick2wallNormalMap.name = "bricks2_normal.jpg";
	textureBrick2wallNormalMap.path = "resources/textures/bricks2_normal.jpg";
	textureBrick2wallNormalMap.type = "texture_normal";
	textures.push_back(textureBrick2wallNormalMap);
	Texture textureBrick2WallDepthMap;
	textureBrick2WallDepthMap.name = "bricks2_disp.jpg";
	textureBrick2WallDepthMap.path = "resources/textures/bricks2_disp.jpg";
	textureBrick2WallDepthMap.type = "texture_height"; // or it's inverse - depth
	textures.push_back(textureBrick2WallDepthMap);
	modelBricksWall = new Model("modelBricksWall", quadVerticesPNTTB, 101111, textures);
	textures.clear();

	// wooden toy box model - normal and parallax mapping
	Texture textureWoodenToyBox;
	textureWoodenToyBox.name = "wood.png";
	textureWoodenToyBox.path = "resources/textures/wood.png";
	textureWoodenToyBox.type = "texture_diffuse";
	textures.push_back(textureWoodenToyBox);
	Texture textureWoodenToyBoxNormalMap;
	textureWoodenToyBoxNormalMap.name = "toy_box_normal.png";
	textureWoodenToyBoxNormalMap.path = "resources/textures/toy_box_normal.png";
	textureWoodenToyBoxNormalMap.type = "texture_normal";
	textures.push_back(textureWoodenToyBoxNormalMap);
	Texture textureWoodenToyBoxDepthMap;
	textureWoodenToyBoxDepthMap.name = "toy_box_disp.png";
	textureWoodenToyBoxDepthMap.path = "resources/textures/toy_box_disp.png";
	textureWoodenToyBoxDepthMap.type = "texture_height"; // or it's inverse - depth
	textures.push_back(textureWoodenToyBoxDepthMap);
	modelWoodenToyBox = new Model("modelWoodenToyBox", quadVerticesPNTTB, 101111, textures);
	textures.clear();
}

void GeometryRenderer::renderPoints(Camera* camera)
{
	shaderTwoDimentionalPosition->use();
	glm::mat4 modelMatrix = glm::mat4(1.0f);
	glm::mat4 viewMatrix = camera->GetViewMatrix();

	modelMatrix = glm::translate(modelMatrix, glm::vec3(0.0f, 0.0f, -3.0f));
	shaderTwoDimentionalPosition->setMat4("uModelMatrix", modelMatrix);
	shaderTwoDimentionalPosition->setMat4("uViewMatrix", viewMatrix);
	shaderTwoDimentionalPosition->setMat4("uProjectionMatrix", projectionMatrix);
	modelPoints->DrawPoints(shaderTwoDimentionalPosition);
	shaderTwoDimentionalPosition->unUse();
}

void GeometryRenderer::renderQuad(Camera* camera)
{
	shaderQuad->use();
	glm::mat4 modelMatrix = glm::mat4(1.0f);
	glm::mat4 viewMatrix = camera->GetViewMatrix();

	modelMatrix = glm::translate(modelMatrix, glm::vec3(0.0f, 0.0f, -3.0f));
	shaderQuad->setMat4("uModelMatrix", modelMatrix);
	shaderQuad->setMat4("uViewMatrix", viewMatrix);
	shaderQuad->setMat4("uProjectionMatrix", projectionMatrix);
	screenQuadModels->Draw(shaderQuad, true);
	shaderQuad->unUse();
}

void GeometryRenderer::renderPlaneWithPointLight(Camera* camera)
{
	shaderPointLight->use();
	// transformation matrices
	glm::mat4 modelMatrix = glm::mat4(1.0f);
	glm::mat4 viewMatrix = glm::mat4(1.0f);
	
	//View Transformation
	viewMatrix = camera->GetViewMatrix();

	modelMatrix = glm::translate(modelMatrix, glm::vec3(0.0f, 0.0f, -3.0f));
	shaderPointLight->setMat4("uModelMatrix", modelMatrix);
	shaderPointLight->setMat4("uViewMatrix", viewMatrix);
	shaderPointLight->setMat4("uProjectionMatrix", projectionMatrix);
	shaderPointLight->setVec3("uViewPos", camera->Position);

	// point light
	glm::vec3 ambientColor = glm::vec3(0.05f, 0.05f, 0.05f);
	glm::vec3 diffuseColor = glm::vec3(1.0f);
	glm::vec3 specularColor = glm::vec3(0.5f, 0.5f, 0.5f);
	glm::vec3 lightPos(0.0f, 0.0f, 0.0f);


	shaderPointLight->setVec3("pointLights[0].ambient", ambientColor);
	shaderPointLight->setVec3("pointLights[0].diffuse", diffuseColor);
	shaderPointLight->setVec3("pointLights[0].specular", specularColor);
	shaderPointLight->setVec3("pointLights[0].position", lightPos);
	shaderPointLight->setFloat("pointLights[0].constant", 1.0f);
	shaderPointLight->setFloat("pointLights[0].linear", 0.09f);
	shaderPointLight->setFloat("pointLights[0].quadratic", 0.032f);

	// material properties
	shaderPointLight->setVec3("uMaterial.ambient", glm::vec3(1.0f, 0.5f, 0.31f));
	shaderPointLight->setInt("uMaterial.diffuse", 0);
	shaderPointLight->setInt("uMaterial.specular", 1);
	shaderPointLight->setFloat("uMaterial.shininess", 32.0f);
	shaderPointLight->setBool("uBlinn", isBlinnKeyPressed);
	shaderPointLight->setVec3("uMaterial.uSpecular", glm::vec3(0.3f));
	
	modelPlane->Draw(shaderPointLight, true);
	shaderPointLight->unUse();

	// draw lamp
	shaderLamp->use();
	modelMatrix = glm::mat4(1.0);
	modelMatrix = glm::translate(modelMatrix, lightPos);
	modelMatrix = glm::scale(modelMatrix, glm::vec3(0.1f));
	shaderLamp->setMat4("uModelMatrix", modelMatrix);
	shaderLamp->setMat4("uViewMatrix", viewMatrix);
	shaderLamp->setMat4("uProjectionMatrix", projectionMatrix);
	shaderLamp->setVec3("uLightColor", diffuseColor);
	shaderLamp->setVec3("uObjectColor", diffuseColor);

	modelCube->Draw(shaderLamp, true);
	shaderLamp->unUse();
}

void GeometryRenderer::renderCube(Shader* shader)
{
	//shader->use();
	modelCube->Draw(shader, true);
	//shader->unUse();
}
void GeometryRenderer::renderPlaneWithDirectionalLight(Camera* camera, Shader* shaderDirectionalLight)
{
	shaderDirectionalLight->use();
	// transformation matrices
	glm::mat4 modelMatrix = glm::mat4(1.0f);
	glm::mat4 viewMatrix = glm::mat4(1.0f);

	//View Transformation
	viewMatrix = camera->GetViewMatrix();

	//modelMatrix = glm::translate(modelMatrix, glm::vec3(0.0f, 0.0f, -3.0f));
	shaderDirectionalLight->setMat4("uModelMatrix", modelMatrix);
	shaderDirectionalLight->setMat4("uViewMatrix", viewMatrix);
	shaderDirectionalLight->setMat4("uProjectionMatrix", projectionMatrix);
	shaderDirectionalLight->setVec3("uViewPos", camera->Position);

	// point light
	glm::vec3 ambientColor = glm::vec3(0.3f);
	glm::vec3 diffuseColor = glm::vec3(1.0f);
	glm::vec3 specularColor = glm::vec3(0.5f, 0.5f, 0.5f);
	glm::vec3 lightPos(0.0f, 0.0f, 0.0f);
	glm::vec3 directionVector = glm::vec3(-0.2f, -1.0f, -0.3f);


	// light properties
	// phase 1: directional lighting
	shaderDirectionalLight->setVec3("dirLight.direction", directionVector);
	shaderDirectionalLight->setVec3("dirLight.ambient", ambientColor);
	shaderDirectionalLight->setVec3("dirLight.diffuse", diffuseColor);
	shaderDirectionalLight->setVec3("dirLight.specular", specularColor);

	// material properties
	shaderDirectionalLight->setVec3("uMaterial.ambient", glm::vec3(1.0f, 0.5f, 0.31f));
	shaderDirectionalLight->setInt("uMaterial.diffuse", 0);
	shaderDirectionalLight->setVec3("uMaterial.specular", glm::vec3(0.5f));
	shaderDirectionalLight->setFloat("uMaterial.shininess", 32.0f);
	//shaderDirectionalLight->setVec3("uMaterial.uSpecular", glm::vec3(0.3f));

	modelPlane->Draw(shaderDirectionalLight, true);

	// cubes
	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(0.0f, 1.5f, 0.0));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(0.5f));
	shaderDirectionalLight->setMat4("uModelMatrix", modelMatrix);
	renderCube(shaderDirectionalLight);
	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(2.0f, 0.0f, 1.0));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(0.5f));
	shaderDirectionalLight->setMat4("uModelMatrix", modelMatrix);
	renderCube(shaderDirectionalLight);
	modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-1.0f, 0.0f, 2.0));
	modelMatrix = glm::rotate(modelMatrix, glm::radians(60.0f), glm::normalize(glm::vec3(1.0, 0.0, 1.0)));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(0.25));
	shaderDirectionalLight->setMat4("uModelMatrix", modelMatrix);
	renderCube(shaderDirectionalLight);

	shaderDirectionalLight->unUse();
}

void GeometryRenderer::renderPlane(Shader* shader)
{
	modelPlane->Draw(shader, true);
}

void GeometryRenderer::renderQuadInstanced(Camera* camera)
{
	shaderQuadInstanced->use();
	glm::mat4 modelMatrix = glm::mat4(1.0f);
	glm::mat4 viewMatrix = camera->GetViewMatrix();

	modelMatrix = glm::translate(modelMatrix, glm::vec3(0.0f, 0.0f, -2.0f));
	shaderQuadInstanced->setMat4("uModelMatrix", modelMatrix);
	shaderQuadInstanced->setMat4("uViewMatrix", viewMatrix);
	shaderQuadInstanced->setMat4("uProjectionMatrix", projectionMatrix);

	// set instances
	for (unsigned int i = 0; i < 100; i++)
	{
		shaderQuadInstanced->setVec2(("offsets[" + std::to_string(i) + "]"), translations[i]);
		//fprintf(fplogfile, "DEBUG::GeometryRenderer::renderQuadInstanced::translations:%f,%f\n", this->translations[i].x, this->translations[i].y);

	}
	screenQuadModels->Draw(shaderQuadInstanced, 100, &glm::mat4(1.0), true); // 100 instances for instanced drawing
	shaderQuadInstanced->unUse();
}


void GeometryRenderer::renderToScreen(Shader* shader, unsigned int tbo)
{
	modelQuadRenderToScreen->Draw(shader, true, tbo);
}

void GeometryRenderer::uninitialize()
{
	
}

void GeometryRenderer::renderWall(Shader* shader)
{
	modelWallQuad->Draw(shader, true);
}

void GeometryRenderer::renderBricksWallWithNormalAndParallaxMap(Shader* shader)
{
	modelBricksWall->Draw(shader, true);
}

void GeometryRenderer::renderWoodenToyBoxWithNormalAndParallaxMap(Shader* shader)
{
	modelWoodenToyBox->Draw(shader, true);
}
