#include"ModelRenderer.h"


ModelRenderer::ModelRenderer(glm::mat4 projMat)
{
	this->projectionMatrix = projMat;
}

void ModelRenderer::initialize()
{
	shaderModelLoaded = new Shader("shaderModelLoaded", "resources/shaders/vsModelLoading.glsl", "resources/shaders/fsModelLoading.glsl");
	shaderModelLoadedInstancedModelMatrix = new Shader("shaderModelLoadedInstancedModelMatrix", "resources/shaders/vsModelLoadingInstancedModelMatrix.glsl", "resources/shaders/fsModelLoading.glsl");
	shaderModelExplodeInDirectionOfNormal = new Shader("shaderModelExplodeInDirectionOfNormal", "resources/shaders/vsModelLoadingExplodeInDirectionOfNormal.glsl", "resources/shaders/gsExplodeVerticesInDirectionOfNormal.glsl","resources/shaders/fsModelLoading.glsl");
	// nanosuit model
	std::string modelPath = "resources/model/nanosuit/nanosuit.obj";
	modelNanosuit = new Model("nanosuit", modelPath.c_str());
	// asteroid field
	modelPath = "resources/model/planet/planet.obj";
	modelPlanet = new Model("planet", modelPath.c_str());

	this->numberOfAsteroids = 10000;
	upliftOffset = 15;
	
	fillAsteroidPositions();
	modelPath = "resources/model/rock/rock.obj";
	//fprintf(fplogfile, "DEBUG:: ModelRenderer::initialize:modelMatrices size-%d\n", sizeof(modelMatricesForAsteroids));
	modelAsteroid = new Model("asteroid", modelPath.c_str(), numberOfAsteroids, modelMatricesForAsteroids);
}

void ModelRenderer::fillAsteroidPositions()
{
	// fill matrix with the asteroid position data for instanced rendering
	modelMatricesForAsteroids = new glm::mat4[numberOfAsteroids];
	srand(glfwGetTime()); // initialize random seed	
	float radius = 30;
	float offset = 2.5f;
	for (unsigned int i = 0; i < numberOfAsteroids; i++)
	{
		glm::mat4 model = glm::mat4(1.0f);
		// 1. translation: displace along circle with 'radius' in range [-offset, offset]
		float angle = (float)i / (float)numberOfAsteroids * 360.0f;
		float displacement = (rand() % (int)(2 * offset * 100)) / 100.0f - offset;
		float x = sin(angle) * radius + displacement;
		displacement = (rand() % (int)(2 * offset * 100)) / 100.0f - offset;
		float y = displacement * 0.4f; // keep height of field smaller compared to width of x and z
		displacement = (rand() % (int)(2 * offset * 100)) / 100.0f - offset;
		float z = cos(angle) * radius + displacement;
		model = glm::translate(model, glm::vec3(x, y + upliftOffset, z - upliftOffset));

		// 2. scale: scale between 0.05 and 0.25f
		float scale = (rand() % 20) / 100.0f + 0.05;
		model = glm::scale(model, glm::vec3(scale));

		// 3. rotation: add random rotation around a (semi)randomly picked rotation axis vector
		float rotAngle = (rand() % 360);
		model = glm::rotate(model, rotAngle, glm::vec3(0.4f, 0.6f, 0.8f));

		// 4. now add to list of matrices
		modelMatricesForAsteroids[i] = model;
	}
}

void ModelRenderer::uninitialize()
{
	
}

void ModelRenderer::render(Camera* camera)
{
	glDisable(GL_BLEND);
	// draw model
	
	this->renderNanoSuit(camera);
	//this->renderAsteroidField(camera);
	this->renderAsteroidFieldInstanced(camera);
	//shaderModelLoaded->unUse();
	glEnable(GL_BLEND);
}


void ModelRenderer::renderNanoSuit(Camera* camera)
{
	if(explodeNonosuitModel==true)
	{
	shaderModelExplodeInDirectionOfNormal->use();
	glm::mat4 modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-2.0f, -0.5f, 0.0f));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(0.2f));
	shaderModelExplodeInDirectionOfNormal->setMat4("uModelMatrix", modelMatrix);
	shaderModelExplodeInDirectionOfNormal->setMat4("uViewMatrix", glm::mat4(camera->GetViewMatrix()));
	shaderModelExplodeInDirectionOfNormal->setMat4("uProjectionMatrix", projectionMatrix);
	shaderModelExplodeInDirectionOfNormal->setFloat("uTime", glfwGetTime());
	modelNanosuit->Draw(shaderModelExplodeInDirectionOfNormal);
	shaderModelExplodeInDirectionOfNormal->unUse();
	}
	else
	{
	shaderModelLoaded->use();
	glm::mat4 modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-2.0f, -0.5f, 0.0f));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(0.2f));
	shaderModelLoaded->setMat4("uModelMatrix", modelMatrix);
	shaderModelLoaded->setMat4("uViewMatrix", glm::mat4(camera->GetViewMatrix()));
	shaderModelLoaded->setMat4("uProjectionMatrix", projectionMatrix);
	shaderModelLoaded->setFloat("uTime", glfwGetTime());
	modelNanosuit->Draw(shaderModelLoaded);
	shaderModelLoaded->unUse();
	}
}

void ModelRenderer::renderAsteroidField(Camera* camera)
{
	fillAsteroidPositions();

	glm::mat4 modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(0.0f, -3.0f, -3.0f));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(4.0f, 4.0f, 4.0f));
	shaderModelLoaded->setMat4("uModelMatrix", modelMatrix);
	shaderModelLoaded->setMat4("uViewMatrix", glm::mat4(camera->GetViewMatrix()));
	shaderModelLoaded->setMat4("uProjectionMatrix", projectionMatrix);
	modelPlanet->Draw(shaderModelLoaded);

	// draw meteorites
	for (unsigned int i = 0; i < numberOfAsteroids; i++)
	{
		shaderModelLoaded->setMat4("uModelMatrix", modelMatricesForAsteroids[i]);
		modelAsteroid->Draw(shaderModelLoaded);
	}
}

void ModelRenderer::renderAsteroidFieldInstanced(Camera* camera)
{
	shaderModelLoaded->use();
	glm::mat4 modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(0.0f, -3.0f + upliftOffset, -3.0f- upliftOffset));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(4.0f, 4.0f, 4.0f));
	shaderModelLoaded->setMat4("uModelMatrix", modelMatrix);
	shaderModelLoaded->setMat4("uViewMatrix", glm::mat4(camera->GetViewMatrix()));
	shaderModelLoaded->setMat4("uProjectionMatrix", projectionMatrix);
	modelPlanet->Draw(shaderModelLoaded);
	shaderModelLoaded->unUse();
	// render rocks
	shaderModelLoadedInstancedModelMatrix->use();
	fillAsteroidPositions();
	shaderModelLoadedInstancedModelMatrix->setMat4("uViewMatrix", glm::mat4(camera->GetViewMatrix()));
	shaderModelLoadedInstancedModelMatrix->setMat4("uProjectionMatrix", projectionMatrix);
	modelAsteroid->Draw(shaderModelLoadedInstancedModelMatrix, numberOfAsteroids, this->modelMatricesForAsteroids, false);
	shaderModelLoadedInstancedModelMatrix->unUse();
}

