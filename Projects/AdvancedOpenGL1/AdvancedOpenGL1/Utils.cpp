#include"Utils.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

/*
loadTexture:
Loads the texture and return texture id
*/
unsigned int loadTexture(const char* imagePath)
{
	/////////Texture
	unsigned int texture;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	

	//load img
	int width, height, nrChannels, imageFormat;
	fprintf(fplogfile, "DEBUG::loadTexture: Loading texture %s\n", imagePath);
	unsigned char* data = stbi_load(imagePath, &width, &height, &nrChannels, 0);
	if (data)
	{
		if (nrChannels == 4) // png
		{
			imageFormat = GL_RGBA;
		}
		else if (nrChannels == 3) // jpg
		{
			imageFormat = GL_RGB;
		}
		glTexImage2D(GL_TEXTURE_2D, 0, imageFormat, width, height, 0, imageFormat, GL_UNSIGNED_BYTE, data);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, imageFormat == GL_RGBA ? GL_CLAMP_TO_EDGE : GL_REPEAT); //GL_CLAMP_TO_EDGE
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, imageFormat == GL_RGBA ? GL_CLAMP_TO_EDGE : GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else
	{
		fprintf(fplogfile, "ERROR::Could Not Load Image");

	}

	stbi_image_free(data);
	return texture;
}


unsigned int TextureFromFile(const char *path, const std::string &directory, bool gamma)
{
	std::string filename = std::string(path);
	filename = directory + '/' + filename;

	unsigned int textureID;
	glGenTextures(1, &textureID);

	int width, height, nrComponents;
	fprintf(fplogfile, "DEBUG::TextureFromFile: Loading texture %s from path %s\n", path, directory.c_str());
	unsigned char *data = stbi_load(filename.c_str(), &width, &height, &nrComponents, 0);
	if (data)
	{
		GLenum format;
		if (nrComponents == 1)
			format = GL_RED;
		else if (nrComponents == 3)
			format = GL_RGB;
		else if (nrComponents == 4)
			format = GL_RGBA;

		glBindTexture(GL_TEXTURE_2D, textureID);
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, format == GL_RGBA ? GL_CLAMP_TO_EDGE : GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, format == GL_RGBA ? GL_CLAMP_TO_EDGE : GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		stbi_image_free(data);
	}
	else
	{
		fprintf(fplogfile, "ERROR::Texture failed to load at path: %s \n", path);
		stbi_image_free(data);
	}

	return textureID;
}

void fillFramebufferColorDepth24Stencil8(FBO *fbo)
{
	// create renderbuffer start
	glGenFramebuffers(1, &fbo->fboId);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo->fboId);

	fbo->tboColorAttachment =  createTexture2DColorAttachmentBuffer(GL_RGBA);
	// attach it to the currently bound framebuffer object
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fbo->tboColorAttachment, 0);

	createDepth24Stencil8DepthStencilAttachmentBuffer(fbo);
	// attach it to the currently bound framebuffer object
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, fbo->rboDepthStencilAttachment);

	// check if the framebuffer is complete and if it's not, we print an error message
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		fprintf(fplogfile, "ERROR::FRAMEBUFFER:: Framebuffer is not complete!\n");
	}
	else
	{
		fprintf(fplogfile, "DEBUG::FRAMEBUFFER:: Framebuffer is completed!\n");
	}

	fprintf(fplogfile, "DEBUG::FRAMEBUFFER:: Framebuffer:%u textureColorBuffer:%u and depthAndStencilRebderBuffer:%u is creadted successfully!\n",
		fbo->fboId, fbo->tboColorAttachment, fbo->rboDepthStencilAttachment);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

}

void fillFramebuffer2ColorDepthTexture2DAttachment(FBO* fbo)
{
	// create renderbuffer start
	glGenFramebuffers(1, &fbo->fboId);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo->fboId);

	// first color attachment
	fbo->tboColorAttachment = createTexture2DColorAttachmentBuffer(GL_RGBA16F);
	// attach it to the currently bound framebuffer object
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fbo->tboColorAttachment, 0);

	// second color attachment
	fbo->tboColorAttachment1 = createTexture2DColorAttachmentBuffer(GL_RGBA16F);
	// attach it to the currently bound framebuffer object
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, fbo->tboColorAttachment1, 0);

	createTexture2DDepthAttachment(fbo, SCR_WIDTH, SCR_HEIGHT);
	// attach it to the currently bound framebuffer object
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, fbo->tboDepthAttachment, 0);

	// check if the framebuffer is complete and if it's not, we print an error message
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		fprintf(fplogfile, "ERROR::FRAMEBUFFER:: Framebuffer is not complete!\n");
	}
	else
	{
		fprintf(fplogfile, "DEBUG::FRAMEBUFFER:: Framebuffer is completed!\n");
	}

	fprintf(fplogfile, "DEBUG::FRAMEBUFFER:: Framebuffer:%u textureColorBuffer:%u and depthAndStencilRebderBuffer:%u is creadted successfully!\n",
		fbo->fboId, fbo->tboColorAttachment, fbo->rboDepthStencilAttachment);


	// We do have to explicitly tell OpenGL we're rendering to multiple colorbuffers via glDrawBuffers 
	// as otherwise OpenGL only renders to a framebuffer's first color attachment ignoring all others
	unsigned int attachments[2] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
	glDrawBuffers(2, attachments);
	fprintf(fplogfile, "DEBUG::FRAMEBUFFER::Rendering to %d colorbuffer attachments\n", 2);


	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void fillFramebufferColorOnlyTexture2DAttachment(FBO* fbo)
{
	// create renderbuffer start
	glGenFramebuffers(1, &fbo->fboId);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo->fboId);

	fbo->tboColorAttachment = createTexture2DColorAttachmentBuffer(GL_RGBA16F);
	// attach it to the currently bound framebuffer object
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fbo->tboColorAttachment, 0);

	// check if the framebuffer is complete and if it's not, we print an error message
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		fprintf(fplogfile, "ERROR::FRAMEBUFFER:: Framebuffer is not complete!\n");
	}
	else
	{
		fprintf(fplogfile, "DEBUG::FRAMEBUFFER:: Framebuffer is completed!\n");
	}

	fprintf(fplogfile, "DEBUG::FRAMEBUFFER:: Framebuffer:%u textureColorBuffer:%u  is creadted successfully!\n",
		fbo->fboId, fbo->tboColorAttachment);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void fillFramebufferColorDepthTexture2DAttachment(FBO *fbo)
{
	// create renderbuffer start
	glGenFramebuffers(1, &fbo->fboId);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo->fboId);

	fbo->tboColorAttachment = createTexture2DColorAttachmentBuffer(GL_RGBA);
	// attach it to the currently bound framebuffer object
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fbo->tboColorAttachment, 0);

	createTexture2DDepthAttachment(fbo, SHADOW_WIDTH, SHADOW_HEIGHT);
	// attach it to the currently bound framebuffer object
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, fbo->tboDepthAttachment, 0);

	// check if the framebuffer is complete and if it's not, we print an error message
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		fprintf(fplogfile, "ERROR::FRAMEBUFFER:: Framebuffer is not complete!\n");
	}
	else
	{
		fprintf(fplogfile, "DEBUG::FRAMEBUFFER:: Framebuffer is completed!\n");
	}

	fprintf(fplogfile, "DEBUG::FRAMEBUFFER:: Framebuffer:%u textureColorBuffer:%u and depthAndStencilRebderBuffer:%u is creadted successfully!\n",
		fbo->fboId, fbo->tboColorAttachment, fbo->rboDepthStencilAttachment);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void fillFramebufferFloatingPointColorDepthTexture2DAttachment(FBO *fbo)
{
	// create renderbuffer start
	glGenFramebuffers(1, &fbo->fboId);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo->fboId);

	// first color attachment
	fbo->tboColorAttachment = createTexture2DColorAttachmentBuffer(GL_RGBA16F);
	// attach it to the currently bound framebuffer object
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fbo->tboColorAttachment, 0);

	createTexture2DDepthAttachment(fbo, SCR_WIDTH, SCR_HEIGHT);
	// attach it to the currently bound framebuffer object
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, fbo->tboDepthAttachment, 0);

	// check if the framebuffer is complete and if it's not, we print an error message
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		fprintf(fplogfile, "ERROR::FRAMEBUFFER:: Framebuffer is not complete!\n");
	}
	else
	{
		fprintf(fplogfile, "DEBUG::FRAMEBUFFER:: Framebuffer is completed!\n");
	}

	fprintf(fplogfile, "DEBUG::FRAMEBUFFER:: Framebuffer:%u textureColorBuffer:%u and depthAndStencilRebderBuffer:%u is creadted successfully!\n",
		fbo->fboId, fbo->tboColorAttachment, fbo->rboDepthStencilAttachment);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void fillFramebufferDepthTexture2DAttachment(FBO *fbo)
{
	// create renderbuffer start
	glGenFramebuffers(1, &fbo->fboId);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo->fboId);

	createTexture2DDepthAttachment(fbo, SHADOW_WIDTH, SHADOW_HEIGHT);
	// attach it to the currently bound framebuffer object
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, fbo->tboDepthAttachment, 0);

	// A framebuffer object however is not complete without a color buffer so we need to explicitly tell OpenGL 
	// we're not going to render any color data
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);

	// check if the framebuffer is complete and if it's not, we print an error message
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		fprintf(fplogfile, "ERROR::FRAMEBUFFER:: Framebuffer is not complete!\n");
	}
	else
	{
		fprintf(fplogfile, "DEBUG::FRAMEBUFFER:: Framebuffer is completed!\n");
	}

	fprintf(fplogfile, "DEBUG::FRAMEBUFFER:: Framebuffer:%u depthBebderBuffer:%u is creadted successfully!\n",
		fbo->fboId,fbo->tboDepthAttachment);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void createTexture2DDepthAttachment(FBO* fbo, unsigned int width, unsigned int height)
{
	glGenTextures(1, &fbo->tboDepthAttachment);
	glBindTexture(GL_TEXTURE_2D, fbo->tboDepthAttachment);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT,
		width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	float borderColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
}

void createDepth24Stencil8DepthStencilAttachmentBuffer(FBO *fbo)
{
	// create renderbuffer: for depth/stencil assignment
	glGenRenderbuffers(1, &fbo->rboDepthStencilAttachment);
	glBindRenderbuffer(GL_RENDERBUFFER, fbo->rboDepthStencilAttachment);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, SCR_WIDTH, SCR_HEIGHT);
	//glBindRenderbuffer(GL_RENDERBUFFER, 0);
}

unsigned int createTexture2DColorAttachmentBuffer(GLenum internalFormat)
{
	unsigned int attachment;
	// generate texture: for texture attachment
	glGenTextures(1, &attachment);
	glBindTexture(GL_TEXTURE_2D, attachment);
	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, SCR_WIDTH, SCR_HEIGHT, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	//glBindTexture(GL_TEXTURE_2D, 0);
	return attachment;
}


/*Function to return the digit of n-th position of num. Position starts from 0*/
int getDigitAtPosition(int num, int n)
{
	int r, t1, t2;

	t1 = pow(10, n + 1);
	r = num % t1;

	if (n > 0)
	{
		t2 = pow(10, n);
		r = r / t2;
	}

	return r;
}

unsigned int loadCubemap(std::vector<std::string> faces)
{
	unsigned int textureId;
	glGenTextures(1, &textureId);
	glBindTexture(GL_TEXTURE_CUBE_MAP, textureId);

	// six different texture targets for targetting the faces
	// GL_TEXTURE_CUBE_MAP_POSITIVE_X - Right
	// GL_TEXTURE_CUBE_MAP_NEGATIVE_X - Left
	// GL_TEXTURE_CUBE_MAP_POSITIVE_Y - Top
	// GL_TEXTURE_CUBE_MAP_NEGATIVE_Y - Bottom
	// GL_TEXTURE_CUBE_MAP_POSITIVE_Z - Back
	// GL_TEXTURE_CUBE_MAP_NEGATIVE_Z - Front

	
	int width, height, nrChannels;
	unsigned char* data;
	for (GLuint i = 0; i < faces.size(); i++)
	{
		data = stbi_load(faces[i].c_str(), &width, &height, &nrChannels, 0);

		if(data)
		{
		glTexImage2D(
			GL_TEXTURE_CUBE_MAP_POSITIVE_X + i,
			0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data
		);
		stbi_image_free(data);
		}
		else
		{
			fprintf(fplogfile, "Cubemap tex failed to load at path:%s\n",faces[i]);
		}
	}
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
	return textureId;
}