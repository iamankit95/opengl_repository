#pragma once
#include"Base.h"
#include"Camera.h"
#include"Shader.h"
#include"Model.h"

class CubemapRenderer
{
public:
	CubemapRenderer(glm::mat4 projMat);
	void initialize();
	void uninitialize();
	void render(Camera* camera);
private:
	glm::mat4 projectionMatrix;
	std::vector<float> skyboxVertices;
	std::vector<std::string> faces;
	Model* modelCubeMapSkybox;
	Shader* shaderCubeMapSkybox;
	unsigned int cubemapTexture;
};

