#pragma once
#include"Base.h"
#include"Camera.h"
#include"Shader.h"
#include"Model.h"

class GeometryRenderer
{
public:
	GeometryRenderer(glm::mat4 projMat);
	void initialize();
	void uninitialize();
	void renderPoints(Camera* camera);
	void renderQuad(Camera* camera);
	void renderQuadInstanced(Camera* camera);
	void renderPlaneWithPointLight(Camera* camera);
	void renderPlaneWithDirectionalLight(Camera* camera, Shader* shaderDirectionalLight);
	void renderToScreen(Shader* camera, unsigned int tbo);
	void renderCube(Shader* shader);
	void renderPlane(Shader* shader);
	void renderWall(Shader* shader);
	void renderBricksWallWithNormalAndParallaxMap(Shader* shader);
	void renderWoodenToyBoxWithNormalAndParallaxMap(Shader* shader);
private:
	glm::mat4 projectionMatrix;
	Shader* shaderTwoDimentionalPosition;
	Shader* shaderQuad;
	Shader* shaderQuadInstanced;
	Model* modelPoints;
	Model* screenQuadModels;
	Shader* shaderPointLight;
	Model* modelPlane;
	Shader* shaderLamp;
	Model* modelCube;
	Model* modelQuadRenderToScreen;
	Model* modelWallQuad;
	Model* modelBricksWall;
	Model* modelWoodenToyBox;
	std::vector<float> points;
	std::vector<float> quadVerticesPT;
	std::vector<float> quadVerticesInstancesPC;
	std::vector<float> planeVerticesInstancesPNT;
	std::vector<float> quadVerticesPNTTB;
	std::vector<float> cubeVerticesPNT;
	// for instancing
	std::vector<glm::vec2> translations;
};

