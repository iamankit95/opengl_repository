#ifndef UTILS_H
#define UTILS_H
#include<stdio.h>
#include<math.h>
#include"Base.h"

struct FBO
{
	unsigned int fboId;
	unsigned int tboColorAttachment;
	unsigned int tboColorAttachment1;
	unsigned int tboDepthAttachment;
	unsigned int tboStencilAttachment;
	unsigned int rboDepthStencilAttachment;
};

unsigned int loadTexture(const char* imagePath);
unsigned int TextureFromFile(const char *path, const std::string &directory, bool gamma);
void fillFramebufferColorDepth24Stencil8(FBO *fbo);
void fillFramebuffer2ColorDepthTexture2DAttachment(FBO* fbo);
unsigned int createTexture2DColorAttachmentBuffer(GLenum internalFormat);
void createDepth24Stencil8DepthStencilAttachmentBuffer(FBO *fbo);
void createTexture2DDepthAttachment(FBO* fbo, unsigned int width, unsigned int height);
void fillFramebufferColorDepthTexture2DAttachment(FBO *fbo);
void fillFramebufferFloatingPointColorDepthTexture2DAttachment(FBO *fbo);
void fillFramebufferDepthTexture2DAttachment(FBO *fbo);
void fillFramebufferColorOnlyTexture2DAttachment(FBO* fbo);
unsigned int loadCubemap(std::vector<std::string> faces);
/*Function to return the digit of n-th position of num. Position starts from 0*/
int getDigitAtPosition(int num, int n);
#endif // !UTILS_H
