#pragma once

#include"Base.h"
#include"Camera.h"
#include"Shader.h"
#include"Model.h"
#include"CartsRenderer.h"
#include"ModelRenderer.h"
#include"GeometryRenderer.h"

class WorldRenderer
{
public:
	WorldRenderer(glm::mat4 projMat);
	void initialize();
	void uninitialize();
	void renderWorld(Camera* camera, FBO* fbo);
	void renderWorld(Camera* camera, FBO* fbo, FBO* fboDepthMap); // for rendering shadows
	void renderWorld(Shader* shader, FBO* fbo, unsigned int colorBuffer, bool isHdr);
	void performTwoPassGuassianBlur(Shader* shader, FBO* fbo, FBO* fboPing, FBO* fboPong, unsigned int colorBuffer, bool isHdr);
	void renderShadowedScene(Shader* shader);
	void renderWallWithNormalMap(Shader* shaderHandle);
	void renderWallWithNormalParallaxMap(Shader* shaderHandle);
	void renderTunnel(Shader* shaderHandle);
private:
	CartsRenderer* cartsRenderer;
	CubemapRenderer* cubeMapRenderer;
	ModelRenderer* modelRenderer;
	GeometryRenderer* geometryModelRenderer;
	FBO* fboDummy;

	// asteroid field
	ModelRenderer* modelPlanet;
	ModelRenderer* modelAsteroid;
	Shader* shaderDepthMap;
	Shader* shaderShadowMap;
	Shader* shaderHandle;

	Shader* shaderDirectionalLightWithNormalMap;
	Shader* shaderDirectionalLight;
	Shader* shaderDirectionalLightWithNormalParallaxMap;
	Shader* shaderHdr;
	Shader* shaderPointLighting;
	Shader* shaderTwoPassGuassianBlur;
	Shader* shaderBloomFinal;
	glm::mat4 projectionMatrix;

};
