#pragma once
#include"Base.h"
#include"Camera.h"
#include"Shader.h"
#include"Model.h"

class ModelRenderer
{
public:
	ModelRenderer(glm::mat4 projMat);
	void initialize();
	void uninitialize();
	void render(Camera* camera);
	
private:
	glm::mat4 projectionMatrix;
	Shader* shaderModelLoaded;
	Shader* shaderModelLoadedInstancedModelMatrix;
	Shader* shaderModelExplodeInDirectionOfNormal;
	Model* modelNanosuit;

	// asteroid field
	Model* modelPlanet;
	Model* modelAsteroid;
	unsigned int numberOfAsteroids;
	unsigned int upliftOffset;
	glm::mat4 *modelMatricesForAsteroids;

	void renderNanoSuit(Camera* camera);
	void renderAsteroidField(Camera* camera);
	void renderAsteroidFieldInstanced(Camera* camera);
	void fillAsteroidPositions();
};

