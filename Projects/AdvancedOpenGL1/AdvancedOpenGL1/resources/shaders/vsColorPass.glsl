#version 440 core

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;

// interface block
out VS_OUT {
    vec3 color;
} vs_out;

uniform mat4 uModelMatrix;
uniform mat4 uViewMatrix;
uniform mat4 uProjectionMatrix; 

void main()
{ 
    gl_Position = uProjectionMatrix * uViewMatrix * uModelMatrix * vec4(aPos, 1.0); 
	vs_out.color = aColor;
}
