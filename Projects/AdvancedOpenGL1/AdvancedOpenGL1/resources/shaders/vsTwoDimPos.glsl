#version 440 core

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;
layout (location = 6) in vec2 aOffset;

// interface block
out VS_OUT {
    vec3 color;
} vs_out;

uniform mat4 uModelMatrix;
uniform mat4 uViewMatrix;
uniform mat4 uProjectionMatrix; 

void main()
{ 
	vec2 pos = aPos.xy * (gl_InstanceID / 100.0);
    gl_Position = uProjectionMatrix * uViewMatrix * uModelMatrix * vec4(pos.xy+aOffset, 0.0, 1.0); 
	vs_out.color = aColor;
}
