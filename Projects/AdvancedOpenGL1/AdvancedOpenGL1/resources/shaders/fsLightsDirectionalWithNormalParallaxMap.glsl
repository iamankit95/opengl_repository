#version 440 core 

in VS_OUT {
    vec3 FragPos;
    vec3 Normal;
    vec2 TexCoords;
	vec3 TangentLightPos;
    vec3 TangentViewPos;
    vec3 TangentFragPos;
} fs_in;

out vec4 FragColor;

struct Material {
    vec3 ambient;
    //vec3 diffuse;
	sampler2D texture_diffuse1;
    vec3 specular;
	//sampler2D specular;
    float shininess;
	sampler2D texture_normal1; // normal mapping
	sampler2D texture_depth1; // depth mapping - inverse of height map
}; 

struct DirLight {
    vec3 direction;
  
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};  

uniform DirLight dirLight;
uniform Material uMaterial;
uniform vec3 uViewPos;
uniform float height_scale;

// function prototype declarations
vec3 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir);  
vec2 ParallaxMapping(vec2 texCoords, vec3 viewDir);
vec2 gTexCoords;
// entry point function
void main()
{

 
vec3 viewDir = normalize(fs_in.TangentViewPos - fs_in.TangentFragPos);
gTexCoords = ParallaxMapping(fs_in.TexCoords,  viewDir);
if(gTexCoords.x > 1.0 || gTexCoords.y > 1.0 || gTexCoords.x < 0.0 || gTexCoords.y < 0.0)
    discard;
// obtain normal from normal map in range [0,1]
vec3 norm = texture(uMaterial.texture_normal1, gTexCoords).rgb;
// transform normal vector to range [-1,1]
norm = normalize(norm * 2.0 - 1.0); 

// phase 1: directional lighting
vec3 result = CalcDirLight(dirLight, norm, viewDir);
FragColor =vec4(result , 1.0f);
};

vec3 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir)
{
vec3 lightDir = normalize(fs_in.TangentLightPos - fs_in.TangentFragPos);
//Diffuse Shading
float diff = max(dot(lightDir,normal), 0.0f);
//Specular Shadding
vec3 reflectDir = reflect(-lightDir, normal);
vec3 halfwayDir = normalize(lightDir + viewDir); 
float spec = pow(max(dot(normal, halfwayDir), 0.0f), uMaterial.shininess);

vec3  ambient=  light.ambient * vec3(texture(uMaterial.texture_diffuse1 , gTexCoords));
vec3 diffuse = light.diffuse * (diff * vec3(texture(uMaterial.texture_diffuse1 , gTexCoords)));
vec3 specular = light.specular * (spec * uMaterial.specular);

return(ambient + diffuse + specular);

}

vec2 ParallaxMapping(vec2 texCoords, vec3 viewDir)
{ 
	// number of depth layers
    const float minLayers = 8.0;
	const float maxLayers = 32.0;
	float numLayers = mix(maxLayers, minLayers, abs(dot(vec3(0.0, 0.0, 1.0), viewDir)));  
    // calculate the size of each layer
    float layerDepth = 1.0 / numLayers;
    // depth of current layer
    float currentLayerDepth = 0.0;
    // the amount to shift the texture coordinates per layer (from vector P)
    vec2 P = viewDir.xy * height_scale; 
    vec2 deltaTexCoords = P / numLayers;

	// get initial values
	vec2  currentTexCoords     = texCoords;
	float currentDepthMapValue = texture(uMaterial.texture_depth1, currentTexCoords).r;
  
	while(currentLayerDepth < currentDepthMapValue)
	{
		// shift texture coordinates along direction of P
		currentTexCoords -= deltaTexCoords;
		// get depthmap value at current texture coordinates
		currentDepthMapValue = texture(uMaterial.texture_depth1, currentTexCoords).r;  
		// get depth of next layer
		currentLayerDepth += layerDepth;  
	}

	// get texture coordinates before collision (reverse operations)
	vec2 prevTexCoords = currentTexCoords + deltaTexCoords;

	// get depth after and before collision for linear interpolation
	float afterDepth  = currentDepthMapValue - currentLayerDepth;
	float beforeDepth = texture(uMaterial.texture_depth1, prevTexCoords).r - currentLayerDepth + layerDepth;
 
	// interpolation of texture coordinates
	float weight = afterDepth / (afterDepth - beforeDepth);
	vec2 finalTexCoords = prevTexCoords * weight + currentTexCoords * (1.0 - weight);
	return finalTexCoords;  
} 
