#version 440 core


out vec4 FragColor;

in vec3 outTexCoords;

uniform samplerCube uCubemap;

void main()
{
vec4 texColor = texture(uCubemap, outTexCoords);

FragColor = texColor;

};
