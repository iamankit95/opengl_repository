#version 440 core


out vec4 FragColor;

in vec2 TexCoords;
uniform sampler2D texture_diffuse1;

void main()
{
FragColor = texture(texture_diffuse1, TexCoords);

// to view depth values
	//float depthValue = texture(texture_diffuse1, TexCoords).r;
    //FragColor = vec4(vec3(depthValue), 1.0);
};
