#version 440 core

layout (location =0) in vec3 aPos;
layout (location =2) in vec3 aNormal;
layout (location =3) in vec2 aTexcoords;

out VS_OUT {
    vec3 FragPos;
    vec3 Normal;
    vec2 TexCoords;
} vs_out;


uniform mat4 uModelMatrix;
uniform mat4 uViewMatrix;
uniform mat4 uProjectionMatrix; 

uniform bool uInverseNormals;

void main()
{
gl_Position = uProjectionMatrix * uViewMatrix * uModelMatrix * vec4(aPos, 1.0);
// convert to world space coords
vs_out.FragPos = vec3(uModelMatrix * vec4(aPos, 1.0));

vec3 n = uInverseNormals ? -aNormal : aNormal;

vs_out.Normal =  mat3(transpose(inverse(uModelMatrix))) * n; 
vs_out.TexCoords = aTexcoords;
};
