#version 440 core


out vec4 FragColor;

in vec2 TexCoords;

uniform sampler2D texture_diffuse1;

void main()
{
FragColor = texture(texture_diffuse1, TexCoords);
//(FragColor.r + FragColor.g + FragColor.b) / 3.0;
float average = 0.2126 * FragColor.r + 0.7152 * FragColor.g + 0.0722 * FragColor.b;
FragColor = vec4(average, average, average, 1.0);
};
