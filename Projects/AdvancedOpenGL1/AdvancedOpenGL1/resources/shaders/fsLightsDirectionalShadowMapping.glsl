#version 440 core 

in VS_OUT {
    vec3 FragPos;
    vec3 Normal;
    vec2 TexCoords;
    vec4 FragPosLightSpace;
} fs_in;

out vec4 FragColor;

struct Material {
    vec3 ambient;
    //vec3 diffuse;
	sampler2D diffuse;
    vec3 specular;
	//sampler2D specular;
    float shininess;
}; 

uniform sampler2D shadowMap;

struct DirLight {
    vec3 direction;
  
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};  

uniform DirLight dirLight;
uniform Material uMaterial;

uniform vec3 uViewPos;
uniform vec3 uLightPos;

float ShadowCalculation(vec4 fragPosLightSpace)
{
    // perform perspective divide
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    // transform to [0,1] range
    projCoords = projCoords * 0.5 + 0.5;

	
    // get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
    float closestDepth = texture(shadowMap, projCoords.xy).r; 
    // get depth of current fragment from light's perspective
    float currentDepth = projCoords.z;
	// calculate bias (based on depth map resolution and slope)
    vec3 normal = normalize(fs_in.Normal);
    vec3 lightDir = normalize(uLightPos - fs_in.FragPos);
	float bias = max(0.05 * (1.0 - dot(normal, lightDir)), 0.005);  //0.005;
    // check whether current frag pos is in shadow
    //float shadow = currentDepth- bias > closestDepth  ? 1.0 : 0.0;
	
	float shadow = 0.0;
	vec2 texelSize = 1.0 / textureSize(shadowMap, 0);
	for(int x = -1; x <= 1; ++x)
	{
		for(int y = -1; y <= 1; ++y)
		{
			float pcfDepth = texture(shadowMap, projCoords.xy + vec2(x, y) * texelSize).r; 
			shadow += currentDepth - bias > pcfDepth ? 1.0 : 0.0;        
		}    
	}
	shadow /= 9.0;

	if(projCoords.z > 1.0)
        shadow = 0.0;

    return shadow;
}

// function prototype declarations
vec3 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir);  

// entry point function
void main()
{
vec3 norm = normalize(fs_in.Normal);
vec3 viewDir = normalize(uViewPos - fs_in.FragPos);

// phase 1: directional lighting
vec3 result = CalcDirLight(dirLight, norm, viewDir);
FragColor =vec4(result , 1.0f);
};

vec3 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir)
{
vec3 lightDir = normalize(uLightPos - fs_in.FragPos); //normalize(-light.direction);//
//Diffuse Shading
float diff = max(dot(lightDir, normal), 0.0f);
//Specular Shadding
vec3 reflectDir = reflect(-lightDir, normal);

float spec = pow(max(dot(viewDir, reflectDir), 0.0f), uMaterial.shininess);
vec3  ambient=  light.ambient * vec3(texture(uMaterial.diffuse , fs_in.TexCoords));
vec3 diffuse = light.diffuse * (diff * vec3(texture(uMaterial.diffuse , fs_in.TexCoords)));
vec3 specular = light.specular * (spec * uMaterial.specular);

// calculate shadow
float shadow = ShadowCalculation(fs_in.FragPosLightSpace);  

vec3 lighting = (ambient + (1.0 - shadow) * (diffuse + specular)) * vec3(texture(uMaterial.diffuse , fs_in.TexCoords));
//vec3 lighting = (ambient + diffuse + specular);

return lighting;
}
