#version 440 core 

in VS_OUT {
    vec3 FragPos;
    vec3 Normal;
    vec2 TexCoords;
	vec3 TangentLightPos;
    vec3 TangentViewPos;
    vec3 TangentFragPos;
} fs_in;

out vec4 FragColor;

struct Material {
    vec3 ambient;
    //vec3 diffuse;
	sampler2D texture_diffuse1;
    vec3 specular;
	//sampler2D specular;
    float shininess;
	sampler2D texture_normal1;
}; 

struct DirLight {
    vec3 direction;
  
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};  

uniform DirLight dirLight;
uniform Material uMaterial;
uniform vec3 uViewPos;


// function prototype declarations
vec3 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir);  

// entry point function
void main()
{
 // obtain normal from normal map in range [0,1]
vec3 norm = texture(uMaterial.texture_normal1, fs_in.TexCoords).rgb;
// transform normal vector to range [-1,1]
norm = normalize(norm * 2.0 - 1.0); 

vec3 viewDir = normalize(fs_in.TangentViewPos - fs_in.TangentFragPos);

// phase 1: directional lighting
vec3 result = CalcDirLight(dirLight, norm, viewDir);
FragColor =vec4(result , 1.0f);
};

vec3 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir)
{
vec3 lightDir = normalize(fs_in.TangentLightPos - fs_in.TangentFragPos);
//Diffuse Shading
float diff = max(dot(lightDir,normal), 0.0f);
//Specular Shadding
vec3 reflectDir = reflect(-lightDir, normal);
vec3 halfwayDir = normalize(lightDir + viewDir); 
float spec = pow(max(dot(normal, halfwayDir), 0.0f), uMaterial.shininess);

vec3  ambient=  light.ambient * vec3(texture(uMaterial.texture_diffuse1 , fs_in.TexCoords));
vec3 diffuse = light.diffuse * (diff * vec3(texture(uMaterial.texture_diffuse1 , fs_in.TexCoords)));
vec3 specular = light.specular * (spec * uMaterial.specular);

return(ambient + diffuse + specular);

}
