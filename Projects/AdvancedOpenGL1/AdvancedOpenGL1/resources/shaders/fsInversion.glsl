#version 440 core


out vec4 FragColor;

in vec2 TexCoords;

uniform sampler2D texture_diffuse1;

void main()
{
FragColor = vec4(vec3(1.0 - texture(texture_diffuse1, TexCoords)), 1.0);
//FragColor = vec4(0.9,0.2,0.6, 1.0);
};
