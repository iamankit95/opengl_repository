#version 440 core


out vec4 FragColor;

in vec2 TexCoords;

uniform sampler2D texture_diffuse1;

void main()
{
FragColor = texture(texture_diffuse1, TexCoords);
// apply gamma correction
float gamma = 2.2;
FragColor.rgb = pow(FragColor.rgb, vec3(1.0/gamma));
};
