#version 440 core 

in VS_OUT {
    vec3 FragPos;
    vec3 Normal;
    vec2 TexCoords;
} fs_in;

out vec4 FragColor;

struct Material {
    vec3 ambient;
    //vec3 diffuse;
	sampler2D diffuse;
    vec3 specular;
	//sampler2D specular;
    float shininess;
}; 

struct DirLight {
    vec3 direction;
  
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};  

uniform DirLight dirLight;
uniform Material uMaterial;
uniform vec3 uViewPos;

// function prototype declarations
vec3 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir);  

// entry point function
void main()
{
vec3 norm = normalize(fs_in.Normal);
vec3 viewDir = normalize(uViewPos - fs_in.FragPos);

// phase 1: directional lighting
vec3 result = CalcDirLight(dirLight, norm, viewDir);
FragColor =vec4(result , 1.0f);
};

vec3 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir)
{
vec3 lightDir = normalize(-light.direction);
//Diffuse Shading
float diff = max(dot(normal, lightDir), 0.0f);
//Specular Shadding
vec3 reflectDir = reflect(-lightDir, normal);

float spec = pow(max(dot(viewDir, reflectDir), 0.0f), uMaterial.shininess);
vec3  ambient=  light.ambient * vec3(texture(uMaterial.diffuse , fs_in.TexCoords));
vec3 diffuse = light.diffuse * (diff * vec3(texture(uMaterial.diffuse , fs_in.TexCoords)));
vec3 specular = light.specular * (spec * uMaterial.specular);

return(ambient + diffuse + specular);

}
