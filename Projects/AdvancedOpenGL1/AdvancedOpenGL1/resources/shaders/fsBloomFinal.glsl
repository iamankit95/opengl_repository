#version 440 core


out vec4 FragColor;

in vec2 TexCoords;

uniform sampler2D texture_diffuse1; // scene
uniform sampler2D texture_diffuse2; // bloom blur
uniform bool uBloom;
uniform float uExposure;

void main()
{
	const float gamma = 2.2;
    vec3 hdrColor = texture(texture_diffuse1, TexCoords).rgb;      
    vec3 bloomColor = texture(texture_diffuse2, TexCoords).rgb;
    if(uBloom)
        hdrColor += bloomColor; // additive blending
    // tone mapping
    vec3 result = vec3(1.0) - exp(-hdrColor * uExposure);
    // also gamma correct while we're at it       
    result = pow(result, vec3(1.0 / gamma));
    FragColor = vec4(result, 1.0);
};
