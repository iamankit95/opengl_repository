#version 440 core

layout (location =0) in vec3 aPos;
layout (location =2) in vec3 aNormal;
layout (location =3) in vec2 aTexcoords;
layout (location = 4) in vec3 aTangent;
layout (location = 5) in vec3 aBitangent;  

out VS_OUT {
    vec3 FragPos;
    vec3 Normal;
    vec2 TexCoords;
	vec3 TangentLightPos;
    vec3 TangentViewPos;
    vec3 TangentFragPos;
} vs_out;


uniform mat4 uModelMatrix;
uniform mat4 uViewMatrix;
uniform mat4 uProjectionMatrix; 

uniform vec3 uLightPos;
uniform vec3 uViewPos;

void main()
{
gl_Position = uProjectionMatrix * uViewMatrix * uModelMatrix * vec4(aPos, 1.0);
// convert to world space coords
vs_out.FragPos = vec3(uModelMatrix * vec4(aPos, 1.0));
vs_out.Normal =  mat3(transpose(inverse(uModelMatrix))) * aNormal; 
vs_out.TexCoords = aTexcoords;

 vec3 T = normalize(vec3(uModelMatrix * vec4(aTangent,   0.0)));
   vec3 B = normalize(vec3(uModelMatrix * vec4(aBitangent, 0.0)));
   vec3 N = normalize(vec3(uModelMatrix * vec4(aNormal,    0.0)));
   mat3 TBN = mat3(T, B, N);

   vs_out.TangentLightPos = TBN * uLightPos;
    vs_out.TangentViewPos  = TBN * uViewPos;
    vs_out.TangentFragPos  = TBN * vs_out.FragPos;
};
