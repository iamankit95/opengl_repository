#version 440 core 

in VS_OUT {
    vec3 FragPos;
    vec3 Normal;
    vec2 TexCoords;
} fs_in;

layout (location = 0) out vec4 FragColor;
layout (location = 1) out vec4 BrightColor;  

uniform vec3 uViewPos;

struct Material {
    vec3 ambient;
    //vec3 diffuse;
	sampler2D texture_diffuse1;
    //vec3 uSpecular;
	sampler2D specular;
    float shininess;
}; 

struct PointLight {    
    vec3 position;
    
    float constant;
    float linear;
    float quadratic;  

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};  
#define NR_POINT_LIGHTS 4
uniform PointLight pointLights[NR_POINT_LIGHTS];


uniform Material uMaterial;
uniform bool uBlinn;

// function prototype declarations
vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir); 

// entry point function
void main()
{
	vec3 norm = normalize(fs_in.Normal);
	vec3 viewDir = normalize(uViewPos - fs_in.FragPos);

	vec3 result;
	// phase 1: point lighting
	for(int i=0; i<NR_POINT_LIGHTS; i++)
	{
		result += CalcPointLight(pointLights[i], norm, fs_in.FragPos, viewDir);
	}
	FragColor =vec4(result , 1.0f);

	// check whether fragment output is higher than threshold, if so output as brightness color
    float brightness = dot(FragColor.rgb, vec3(0.2126, 0.7152, 0.0722));
    if(brightness > 1.0)
        BrightColor = vec4(FragColor.rgb, 1.0);
    else
        BrightColor = vec4(0.0, 0.0, 0.0, 1.0);
};


vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
vec3 lightDir = normalize(light.position - fragPos);

float diff = max(dot(normal, lightDir), 0.0f);
vec3 reflectDir = reflect(-lightDir, normal);

float spec = 0.0f;
if(uBlinn)
{
	vec3 halfwayDir = normalize(lightDir + viewDir); 
	spec = pow(max(dot(normal, halfwayDir), 0.0), uMaterial.shininess);
}else
{
	spec = pow(max(dot(viewDir, reflectDir), 0.0), uMaterial.shininess/2);
}

float distance = length(light.position - fragPos);
float attenuation = 1.0f / (light.constant + light.linear * distance + light.quadratic * (distance * distance));

vec3  ambient=  light.ambient * vec3(texture(uMaterial.texture_diffuse1 , fs_in.TexCoords));
vec3 diffuse = light.diffuse * (diff * vec3(texture(uMaterial.texture_diffuse1 , fs_in.TexCoords)));
vec3 specular = light.specular * (spec  *  vec3(texture(uMaterial.specular , fs_in.TexCoords))); //uMaterial.uSpecular); //*
ambient  *= attenuation; 
diffuse  *= attenuation;
specular *= attenuation;

return (ambient + diffuse + specular);

}  
