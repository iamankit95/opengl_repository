#version 440 core


out vec4 FragColor;

in vec2 TexCoords;
uniform sampler2D texture_diffuse1;
uniform bool uHdr;
uniform float uExposure;

void main()
{
const float gamma = 2.2;
vec3 hdrColor = texture(texture_diffuse1, TexCoords).rgb;

if(uHdr)
{
	// reinhard
    // vec3 result = hdrColor / (hdrColor + vec3(1.0));
    // exposure
    vec3 result = vec3(1.0) - exp(-hdrColor * uExposure);
    // also gamma correct while we're at it       
    result = pow(result, vec3(1.0 / gamma));
    FragColor = vec4(result, 1.0);
}else
{
	vec3 result = pow(hdrColor, vec3(1.0 / gamma));
    FragColor = vec4(result, 1.0);
}

};
