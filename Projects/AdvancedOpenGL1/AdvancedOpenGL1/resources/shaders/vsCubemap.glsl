#version 440 core

layout (location = 0) in vec3 aPos;

out vec3 outTexCoords;

uniform mat4 uProjectionMatrix;
uniform mat4 uViewMatrix;

void main()
{
    outTexCoords = aPos;
    gl_Position = uProjectionMatrix * uViewMatrix * vec4(aPos, 1.0);
}  