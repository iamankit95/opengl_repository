#version 440 core

void main()
{ 
	// stores only depth values
    gl_FragDepth = gl_FragCoord.z;
}
