#include"Shader.h"

Shader::Shader(const char* vertexPath, const char* fragmentPath)
{
	setUpShaders(vertexPath, fragmentPath);
}

Shader::Shader(const char* name, const char* vertexPath, const char* fragmentPath)
{
	this->name = name;
	fprintf(fplogfile, "DEBUG::Shader::Shader::Setting up shader: %s \n", name);
	setUpShaders(vertexPath, fragmentPath);
}

Shader::Shader(const char *name, const char* vertexPath, const char* geometryPath, const char* fragmentPath)
{
	this->name = name;
	fprintf(fplogfile, "DEBUG::Shader::Shader::Setting up shader: %s \n", name);
	setUpShaders(vertexPath, geometryPath, fragmentPath);
}

void Shader::setUpShaders(const char* vertexPath, const char* geometryPath, const char* fragmentPath)
{
	//////////////
	unsigned int vertexShader, fragmentShader, geometryShader;

	vertexShader = setupShader(vertexPath, VERTEX_SHADER_TYPE);
	geometryShader = setupShader(geometryPath, GEOMETRY_SHADER_TYPE);
	fragmentShader = setupShader(fragmentPath, FRAGMENT_SHADER_TYPE);
	//////////////////////////////

	ID = glCreateProgram();
	glAttachShader(ID, vertexShader);
	glAttachShader(ID, geometryShader);
	glAttachShader(ID, fragmentShader);
	glLinkProgram(ID);

	int success;
	char infoLog[512];
	glGetProgramiv(ID, GL_LINK_STATUS, &success);
	if (!success)
	{
		glGetProgramInfoLog(ID, 512, NULL, infoLog);
		fprintf(fplogfile, "DEBUG::Shader::setUpShaders::Program Object Linking Error: %s \n", infoLog);

	}
	else {
		fprintf(fplogfile, "DEBUG::Shader::setUpShaders::Successfully Linked Shader Program:%u\n", ID);
	}


	glDeleteShader(vertexShader);
	glDeleteShader(geometryShader);
	glDeleteShader(fragmentShader);
}

void Shader::setUpShaders(const char* vertexPath, const char* fragmentPath)
{
	//////////////
	unsigned int vertexShader, fragmentShader;

	vertexShader = setupShader(vertexPath, VERTEX_SHADER_TYPE);
	fragmentShader = setupShader(fragmentPath, FRAGMENT_SHADER_TYPE);

	//////////////////////////////

	ID = glCreateProgram();
	glAttachShader(ID, vertexShader);
	glAttachShader(ID, fragmentShader);
	glLinkProgram(ID);

	int success;
	char infoLog[512];
	glGetProgramiv(ID, GL_LINK_STATUS, &success);
	if (!success)
	{
		glGetProgramInfoLog(ID, 512, NULL, infoLog);
		fprintf(fplogfile, "DEBUG::Shader::setUpShaders::Program Object Linking Error: %s \n", infoLog);

	}
	else {
		fprintf(fplogfile, "DEBUG::Shader::setUpShaders::Successfully Linked Shader Program:%u\n", ID);
	}


	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
}

unsigned int Shader::setupShader(const char* shaderPath, ShaderType type)
{
	std::ifstream shaderFile;
	std::string shaderCodeStr;

	try
	{
		shaderFile.open(shaderPath);
		std::stringstream shaderStream;
		shaderStream << shaderFile.rdbuf();
		shaderFile.close();
		shaderCodeStr = shaderStream.str();
	}
	catch (std::ifstream::failure e)
	{
		fprintf(fplogfile, "ERROR::SHADER::setUpShaders::FILE_NOT_SUCCESSFULLY_READ \n");
	}

	const char * shaderCode = shaderCodeStr.c_str();

	unsigned int SBO;
	const char* successMessage;
	const char* failureMessage;

	if (type == VERTEX_SHADER_TYPE)
	{
		SBO = glCreateShader(GL_VERTEX_SHADER);
		successMessage = "DEBUG::Shader::setUpShader::Successfully Compiled Vertex Shader: %s\n";
		failureMessage = "ERROR::Shader::setUpShader::VertexShader Compilation Error: %s \n";
	}
	else if (type == GEOMETRY_SHADER_TYPE)
	{
		SBO = glCreateShader(GL_GEOMETRY_SHADER);
		successMessage = "DEBUG::Shader::setUpShader::Successfully Compiled Geometry Shader: %s\n";
		failureMessage = "ERROR::Shader::setUpShader::GeometryShader Compilation Error: %s \n";
	}
	else if (type == FRAGMENT_SHADER_TYPE)
	{
		SBO = glCreateShader(GL_FRAGMENT_SHADER);
		successMessage = "DEBUG::Shader::setUpShader::Successfully Compiled Fragment Shader: %s\n";
		failureMessage = "ERROR::Shader::setUpShader::FragmentShader Compilation Error: %s \n";
	}
	

	glShaderSource(SBO, 1, &shaderCode, NULL);
	glCompileShader(SBO);

	//Compilation on Graphics Card
	int success;
	char infoLog[512];
	glGetShaderiv(SBO, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(SBO, 512, NULL, infoLog);
		fprintf(fplogfile, failureMessage, infoLog);

	}
	else {
		fprintf(fplogfile, successMessage, shaderPath);
	}

	return SBO;
}

void Shader::use()
{
	glUseProgram(ID);
}

void Shader::unUse()
{
	glUseProgram(0);
}

void Shader::setBool(const std::string  &name, bool value) const 
{
	unsigned int loc = glGetUniformLocation(ID, name.c_str());
	glUniform1i(loc, value);
}

void Shader::setInt(const std::string  &name, int value) const
{
	unsigned int loc = glGetUniformLocation(ID, name.c_str());
	glUniform1i(loc, value);
}
void Shader::setFloat(const std::string  &name, float value) const
{
	unsigned int loc = glGetUniformLocation(ID, name.c_str());
	glUniform1f(loc, value);
}
void Shader::setMat4(const std::string  &name, glm::mat4 value) const
{
	unsigned int loc = glGetUniformLocation(ID, name.c_str());
	glUniformMatrix4fv(loc, 1 , GL_FALSE ,  glm::value_ptr(value));
}

void Shader::setVec3(const std::string  &name, glm::vec3 value) const
{
	unsigned int loc = glGetUniformLocation(ID, name.c_str());
	glUniform3f(loc, value.x, value.y, value.z);
}

void Shader::setVec2(const std::string  &name, glm::vec2 value) const
{
	unsigned int loc = glGetUniformLocation(ID, name.c_str());
	glUniform2f(loc, value.x, value.y);
}

Shader::~Shader()
{
	// don't delete the program here
	// it is eventually deleting the program once renderloop is done it's iteration
	//glDeleteProgram(ID);
}