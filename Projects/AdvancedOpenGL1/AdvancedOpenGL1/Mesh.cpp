#include"Mesh.h"

Mesh::Mesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices, std::vector<Texture> textures)
{
	this->vertices = vertices;
	this->indices = indices;
	this->textures = textures;

	setUpMesh();
}

void Mesh::setUpMesh()
{
	/////Create VAO
	glGenVertexArrays(1, &VAO);

	glBindVertexArray(VAO);

	//create VBO (Vertex Buffer Object)
	unsigned int VBO;
	glGenBuffers(1, &VBO);
	//Activate VBO now
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	//Pass Data 
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);


	//Vertex data
	glVertexAttribPointer(RSM_VERTEX_POSITION_ATTRIBUTE, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
	glEnableVertexAttribArray(RSM_VERTEX_POSITION_ATTRIBUTE);

	//color data
	glVertexAttribPointer(RSM_VERTEX_COLOR_ATTRIBUTE, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, color));
	glEnableVertexAttribArray(RSM_VERTEX_COLOR_ATTRIBUTE);

	//normal data
	glVertexAttribPointer(RSM_VERTEX_NORMAL_ATTRIBUTE, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, normal));
	glEnableVertexAttribArray(RSM_VERTEX_NORMAL_ATTRIBUTE);

	//Texture data
	glVertexAttribPointer(RSM_VERTEX_TEXCOORDS_ATTRIBUTE, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, texCoords));
	glEnableVertexAttribArray(RSM_VERTEX_TEXCOORDS_ATTRIBUTE);

	// vertex tangent
	glEnableVertexAttribArray(RSM_VERTEX_TANGENT_ATTRIBUTE);
	glVertexAttribPointer(RSM_VERTEX_TANGENT_ATTRIBUTE, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, tangent));
	
	// vertex bitangent
	glEnableVertexAttribArray(RSM_VERTEX_BITANGENT_ATTRIBUTE);
	glVertexAttribPointer(RSM_VERTEX_BITANGENT_ATTRIBUTE, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, bitangent));

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Create EBO

	glGenBuffers(1, &EBO);
	//Activate VBO now
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	//Pass Data 
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);
	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

}


Mesh::Mesh(std::vector<float> vertices, int dataHint, std::vector<Texture> textures)
{
	this->verticesInterleaved = vertices;
	this->textures = textures;

	setUpInterleavedData(dataHint);

	fprintf(fplogfile, "DEBUG::Mesh::Mesh::total vertices: this->verticesInterleaved:%d\n", this->verticesInterleaved.size() / stride);
}

// this is overloaded for instanced drawing
Mesh::Mesh(std::vector<float> vertices, int dataHint, std::vector<Texture> textures, bool isInstanced, std::vector<glm::vec2> translations)
{
	this->verticesInterleaved = vertices;
	this->textures = textures;
	this->isInstanced = isInstanced;

	setUpInterleavedData(dataHint);

	if (isInstanced)
	{
		glBindVertexArray(VAO);
		unsigned int instanceVBO;
		glGenBuffers(1, &instanceVBO);
		glBindBuffer(GL_ARRAY_BUFFER, instanceVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2) * 100, &translations[0], GL_STATIC_DRAW);
		// vertex offset for instancing
		glEnableVertexAttribArray(RSM_VERTEX_INSTANCE_ATTRIBUTE);
		glVertexAttribPointer(RSM_VERTEX_INSTANCE_ATTRIBUTE, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), (void*)0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glVertexAttribDivisor(RSM_VERTEX_INSTANCE_ATTRIBUTE, 1);
		glBindVertexArray(0);
	}
	

	fprintf(fplogfile, "DEBUG::Mesh::Mesh::total vertices: this->verticesInterleaved:%d\n", this->verticesInterleaved.size() / stride);

}
void Mesh::Draw(Shader* shader)
{
	unsigned int diffuseNr = 1;
	unsigned int specularNr = 1;
	unsigned int normalNr = 1;
	unsigned int heightNr = 1;

	for (int i = 0; i < textures.size(); i++)
	{
		glActiveTexture(GL_TEXTURE0 + i);
		std::string number;
		std::string name = textures[i].type;

		if (name == "texture_diffuse")
		{
			number = std::to_string(diffuseNr++);
		}else if (name == "texture_specular")
		{
			number = std::to_string(specularNr++);
		}
		else if (name == "texture_normal")
			number = std::to_string(normalNr++); // transfer unsigned int to stream
		else if (name == "texture_height")
			number = std::to_string(heightNr++); // transfer unsigned int to stream

		//"material."+
		shader->setFloat(("material." + name+number).c_str(), i);
		glBindTexture(GL_TEXTURE_2D, textures[i].id);
	}

	

	// draw mesh
	glBindVertexArray(VAO);
	glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);

	glActiveTexture(GL_TEXTURE0);
}

void Mesh::Draw(Shader* shader, int instances, std::string type)
{
	unsigned int diffuseNr = 1;
	unsigned int specularNr = 1;
	unsigned int normalNr = 1;
	unsigned int heightNr = 1;

	for (int i = 0; i < textures.size(); i++)
	{
		glActiveTexture(GL_TEXTURE0 + i);
		std::string number;
		std::string name = textures[i].type;

		if (name == "texture_diffuse")
		{
			number = std::to_string(diffuseNr++);
		}
		else if (name == "texture_specular")
		{
			number = std::to_string(specularNr++);
		}
		else if (name == "texture_normal")
			number = std::to_string(normalNr++); // transfer unsigned int to stream
		else if (name == "texture_height")
			number = std::to_string(heightNr++); // transfer unsigned int to stream

												 //"material."+
		shader->setFloat(("material." + name + number).c_str(), i);
		glBindTexture(GL_TEXTURE_2D, textures[i].id);
	}



	// draw mesh
	glBindVertexArray(VAO);
	//glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
	glDrawElementsInstanced(
		GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0, instances
	);

	glBindVertexArray(0);

	glActiveTexture(GL_TEXTURE0);
}
void Mesh::Draw(Shader* shader, bool isInterleaved)
{
	unsigned int diffuseNr = 1;
	unsigned int specularNr = 1;
	unsigned int normalNr = 1;
	unsigned int heightNr = 1;

	for (int i = 0; i < textures.size(); i++)
	{
		glActiveTexture(GL_TEXTURE0 + i);
		std::string number;
		std::string name = textures[i].type;

		if (name == "texture_diffuse")
		{
			number = std::to_string(diffuseNr++);
		}
		else if (name == "texture_specular")
		{
			number = std::to_string(specularNr++);
		}
		else if (name == "texture_normal")
			number = std::to_string(normalNr++); // transfer unsigned int to stream
		else if (name == "texture_height") // or it's inverse - depth map
			number = std::to_string(heightNr++); // transfer unsigned int to stream

												 //"material."+
		shader->setFloat(("uMaterial." + name + number).c_str(), i);
		glBindTexture(GL_TEXTURE_2D, textures[i].id);

		//fprintf(fplogfile, "DEBUG::Mesh::Draw::using td-%s %u %d\n", ("uMaterial." + name + number).c_str(),textures[i].id, i);
	}

	//fprintf(fplogfile, "DEBUG::Mesh::Draw::using shader %u-%s and VAO-%u\n", shader.ID, shader.name, VAO);

	// draw mesh
	glBindVertexArray(VAO);
	glDrawArrays(GL_TRIANGLES, 0, verticesInterleaved.size() / stride);
	glBindVertexArray(0);

	glActiveTexture(GL_TEXTURE0);
}

void Mesh::Draw(Shader* shader, int instaceCount)
{
	// draw mesh
	glBindVertexArray(VAO);
	//fprintf(fplogfile, "DEBUG::Mesh::DrawInstanced:instaceCount=%d\n", verticesInterleaved.size() / stride);
	//glDrawArrays(GL_TRIANGLES, 0, verticesInterleaved.size() / stride);
	glDrawArraysInstanced(GL_TRIANGLES, 0, verticesInterleaved.size() / stride, instaceCount);
	glBindVertexArray(0);
}

void Mesh::DrawCubemap(Shader* shader)
{
	for (int i = 0; i < textures.size(); i++)
	{
		glActiveTexture(GL_TEXTURE0 + i);
		
		shader->setFloat("uCubemap", i);
		glBindTexture(GL_TEXTURE_CUBE_MAP, textures[i].id);
	}

	//fprintf(fplogfile, "DEBUG::Mesh::Draw::using shader %u-%s and VAO-%u\n", shader.ID, shader.name, VAO);

	// draw mesh
	glBindVertexArray(VAO);
	glDrawArrays(GL_TRIANGLES, 0, verticesInterleaved.size() / stride);
	glBindVertexArray(0);

	glActiveTexture(GL_TEXTURE0);
}

void Mesh::Draw(Shader* shader, bool isInterleaved, unsigned int tbo)
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, tbo);
	// draw mesh
	glBindVertexArray(VAO);
	glDrawArrays(GL_TRIANGLES, 0, verticesInterleaved.size() / stride);
	glBindVertexArray(0);
}

void Mesh::DrawPoints(Shader* shader)
{
	// draw mesh
	glBindVertexArray(VAO);
	glDrawArrays(GL_POINTS, 0, verticesInterleaved.size() / stride);
	glBindVertexArray(0);
}


void Mesh::setUpInterleavedData(int dataHint)
{
	/////Create VAO
	glGenVertexArrays(1, &VAO);

	glBindVertexArray(VAO);

	//create VBO (Vertex Buffer Object)
	glGenBuffers(1, &VBO);
	//Activate VBO now
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	//Pass Data 
	glBufferData(GL_ARRAY_BUFFER, verticesInterleaved.size() * sizeof(float), &verticesInterleaved[0], GL_STATIC_DRAW);

	// dataHint: PCNTTB - 
	// Position,Color,Normal,Texcoords,Tangents,Bitangents
	// 101100
	// ToDo - calculate value later
	int stride = 0;
	int isVertexPositionDataPresent = getDigitAtPosition(dataHint, 5);
	int isVertexColorDataPresent = getDigitAtPosition(dataHint, 4);
	int isVertexNormalDataPresent = getDigitAtPosition(dataHint, 3);
	int isVertexTexcoordsDataPresent = getDigitAtPosition(dataHint, 2);
	int isVertexTangentDataPresent = getDigitAtPosition(dataHint, 1);
	int isVertexBitangentDataPresent = getDigitAtPosition(dataHint, 0);
	// position
	if (isVertexPositionDataPresent == 1) // position data
	{
		stride += 3;
	}
	if (isVertexColorDataPresent == 1) // color data
	{
		stride += 3;
	}
	if (isVertexNormalDataPresent == 1) // normal data
	{
		stride += 3;
	}
	if (isVertexTexcoordsDataPresent == 1) // texcoords data
	{
		stride += 2;
	}
	 if (isVertexTangentDataPresent == 1) // tangent data 
	{
		stride += 3;
	}
	 if (isVertexBitangentDataPresent == 1) // bitangent data
	{
		stride += 3;
	}
	 
	this->stride = stride;

	unsigned int offset = 0;
	fprintf(fplogfile, "DEBUG::Mesh::setUpInterleavedData:initial offset - %u\n", offset);
	if (isVertexPositionDataPresent == 1)
	{
		offset = 0;
		fprintf(fplogfile, "DEBUG::Mesh::setUpInterleavedData:vertex position offset - %u\n", offset);

		//Vertex data
		glVertexAttribPointer(RSM_VERTEX_POSITION_ATTRIBUTE, 3, GL_FLOAT, GL_FALSE, stride * sizeof(float), (void*)(offset));
		glEnableVertexAttribArray(RSM_VERTEX_POSITION_ATTRIBUTE);
	}
	if(isVertexColorDataPresent==1)
	{
		offset += 3;
		fprintf(fplogfile, "DEBUG::Mesh::setUpInterleavedData:vertex color offset - %u\n", offset);

	//color data
	glVertexAttribPointer(RSM_VERTEX_COLOR_ATTRIBUTE, 3, GL_FLOAT, GL_FALSE, stride * sizeof(float), (void*)(offset * sizeof(float)));
	glEnableVertexAttribArray(RSM_VERTEX_COLOR_ATTRIBUTE);
	}
	if (isVertexNormalDataPresent == 1)
	{
		offset += 3;
		fprintf(fplogfile, "DEBUG::Mesh::setUpInterleavedData:vertex normals offset - %u\n", offset);

		//normal data
		glVertexAttribPointer(RSM_VERTEX_NORMAL_ATTRIBUTE, 3, GL_FLOAT, GL_FALSE, stride * sizeof(float), (void*)(offset * sizeof(float)));
		glEnableVertexAttribArray(RSM_VERTEX_NORMAL_ATTRIBUTE);
	}
	if(isVertexTexcoordsDataPresent==1)
	{
		offset += 3;
		fprintf(fplogfile, "DEBUG::Mesh::setUpInterleavedData:vertex texcoords offset - %u\n", offset);

	//Texture data
	glVertexAttribPointer(RSM_VERTEX_TEXCOORDS_ATTRIBUTE, 2, GL_FLOAT, GL_FALSE, stride * sizeof(float), (void*)(offset * sizeof(float)));
	glEnableVertexAttribArray(RSM_VERTEX_TEXCOORDS_ATTRIBUTE);
	}
	if (isVertexTangentDataPresent == 1)
	{
		offset += 2;
		fprintf(fplogfile, "DEBUG::Mesh::setUpInterleavedData:vertex tangent offset - %u\n", offset);
		//normal data
		glVertexAttribPointer(RSM_VERTEX_TANGENT_ATTRIBUTE, 3, GL_FLOAT, GL_FALSE, stride * sizeof(float), (void*)(offset * sizeof(float)));
		glEnableVertexAttribArray(RSM_VERTEX_TANGENT_ATTRIBUTE);
	}
	if (isVertexBitangentDataPresent == 1)
	{
		offset += 3;
		fprintf(fplogfile, "Mesh::setUpInterleavedData:vertex bitangent offset - %u\n", offset);

		//normal data
		glVertexAttribPointer(RSM_VERTEX_BITANGENT_ATTRIBUTE, 3, GL_FLOAT, GL_FALSE, stride * sizeof(float), (void*)(offset * sizeof(float)));
		glEnableVertexAttribArray(RSM_VERTEX_BITANGENT_ATTRIBUTE);
	}
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	fprintf(fplogfile, "DEBUG::Mesh::setUpInterleavedData:\n\tStride values - "\
		"\tdataHint - %u \n"\
		"\tisVertexBitangentDataPresent - %u \n"\
		"\tisVertexTangentDataPresent - %u \n"\
		"\tisVertexTexcoordsDataPresent - %u \n"\
		"\tisVertexNormalDataPresent - %u \n"\
		"\tisVertexColorDataPresent - %u \n"\
		"\tisVertexPositionDataPresent - %u \n"\
		"\tstride - %u \n"\
		" \n", dataHint, isVertexBitangentDataPresent, isVertexTangentDataPresent,
		isVertexTexcoordsDataPresent, isVertexNormalDataPresent,
		isVertexColorDataPresent, isVertexPositionDataPresent, stride);
}