//Header Files
#include"RenderMaster.h"
#include"Camera.h"

#include<stdio.h>
#include<iostream>

//Global Variables


// camera
Camera camera(glm::vec3(0.0f,0.0f, 3.0f), glm::vec3(0.0f, 1.0f, 0.0f), YAW, PITCH);
glm::vec3 lightPos(1.2f, 1.0f, 2.0f);


unsigned int texture;
unsigned int textureSpecular;
unsigned int textureSmiley;
float deltaTime = 0.0f;
float lastFrame = 0.0f;
//Mouse Values
float lastX = 400 , lastY = 300;

float yaw = -90.0f, pitch = 0.0f;

bool firstMouse = true;

extern FILE * fplogfile;
bool explodeNonosuitModel = false;
bool isBlinnKeyPressed = false;

Base* baseFramework;
RenderMaster* renderMaster;


//Function Prototype Declaration
void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void mouse_callback(GLFWwindow* window, double xPos, double yPos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);

void Display(void);

//
void Processinput(GLFWwindow* window);




//Entrypoint Function
int main()
{
	/////////////////////////////////////// Initialization Starts
	baseFramework = new Base(); // initializes GLFW/GLUT/OpenGL
	renderMaster = new RenderMaster();

	GLFWmonitor *monitor = glfwGetPrimaryMonitor();
	const GLFWvidmode* mode = glfwGetVideoMode(monitor);
	glfwWindowHint(GLFW_RED_BITS, mode->redBits);
	glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
	glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
	glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);
	GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "Hello OpenGL", NULL, NULL);
	//GLFWwindow* window = glfwCreateWindow(mode->width, mode->height, "Hello OpenGL", monitor, NULL);
	//GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "Hello OpenGL", monitor, NULL);
	if (window == NULL)
	{
		fprintf(fplogfile, "Window is not created successfully so exiting program \n");
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	glfwSetFramebufferSizeCallback(window , framebuffer_size_callback);
	
	glfwSetCursorPosCallback(window , mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);


	//initializing glad
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		fprintf(fplogfile, "Failed to initialize GLAD \n");
		return -1;
	}

	renderMaster->initialize();
	////////////////////////////////Initialization Ends

	//////////////////////////////Game Loop Starts
	while (!glfwWindowShouldClose(window))
	{
		Processinput(window);
		Display();
		glfwSwapBuffers(window);
		glfwPollEvents();
	}
	///////////////////////////Game Loop Ends
	renderMaster->uninitialize();
	delete(renderMaster);
	delete(baseFramework); // it calls destructor of the class
	/////////////////////////Uninitialize End
	return 0;
}

void mouse_callback(GLFWwindow* window, double xPos, double yPos)
{
	if (firstMouse)
	{
		lastX = xPos;
		lastY = yPos;
		firstMouse = false;
	}
	float xoffset = xPos - lastX;
	float yoffset = lastY - yPos;
	lastX = xPos;
	lastY = yPos;

	camera.ProcessMouseMovement(xoffset, yoffset, true);
}


void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	camera.ProcessMouseScroll(yoffset);
}





void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
}

void Processinput(GLFWwindow* window)
{

	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(window, true);
	}

	if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
	{
		camera.ProcessKeyboard(FORWARD, deltaTime);
	}
	if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
	{
		camera.ProcessKeyboard(BACKWARD, deltaTime);
	}
	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
	{
		camera.ProcessKeyboard(LEFT, deltaTime);
	}
	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
	{
		camera.ProcessKeyboard(RIGHT, deltaTime);
	}

	// explode object
	if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS)
	{
		if (explodeNonosuitModel == false)
		{
			explodeNonosuitModel = true;
		}
		else
		{
			explodeNonosuitModel = false;
		}
	}else if (glfwGetKey(window, GLFW_KEY_B) == GLFW_PRESS) // for blinn lighting model
	{
		if (isBlinnKeyPressed == false)
		{
			isBlinnKeyPressed = true;
		}
		else
		{
			isBlinnKeyPressed = false;
		}
	}

}

void Display(void)
{
	
	float currentFrame = glfwGetTime();
	deltaTime = currentFrame - lastFrame;
	lastFrame = currentFrame;

	renderMaster->renderScene(&camera);
	
}
