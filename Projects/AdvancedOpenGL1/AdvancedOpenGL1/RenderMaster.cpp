#include"RenderMaster.h"
#include"Utils.h"

RenderMaster::RenderMaster()
{
	//Projection Transform
	projectionMatrix = glm::perspective(glm::radians(fov), (float)SCR_WIDTH / SCR_HEIGHT, 0.1f, 100.0f);
	worldRenderer = new WorldRenderer(projectionMatrix);
}

void RenderMaster::initialize()
{
	glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT);

	glEnable(GL_DEPTH_TEST);

	// GL_FALSE makes depth buffer read-only; it won't allow to update the depth buffer
	// this has effect only if depth testing is enabled
	glDepthMask(GL_TRUE);

	// sets the comparision operator OpenGL should use
	// while performing depth testing
	// GL_LESS - passes depth test if fragment depth value is less that the stored one
	/*GL_ALWAYS:
	The depth test always passes so the fragments that are drawn last are rendered
	in front of the fragments that were drawn before, even though they should've been
	at the front. e.g. Suppose we've drawn the floor plane last,
	the plane's fragments overwrite each of the container's previously written fragments.
	*/
	glDepthFunc(GL_LESS);

	//glEnable(GL_STENCIL_TEST);

	glEnable(GL_BLEND); // enables blending
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glEnable(GL_CULL_FACE); // enable face culling
	glCullFace(GL_BACK);
	glFrontFace(GL_CCW);

	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	fprintf(fplogfile, "WorldRenderer::WorldRenderer::OpenGL states Initialization done successfully\n");
	// default FBO initialization
	// for scene rendering
	fbo = new FBO();
	fillFramebufferColorDepth24Stencil8(fbo);
	// for color inversion
	fboInversion = new FBO();
	fillFramebufferColorDepth24Stencil8(fboInversion);
	// for greyscaling an image
	fboGreyscale = new FBO();
	fillFramebufferColorDepth24Stencil8(fboGreyscale);
	// for edge detection
	fboEdgeDetection = new FBO();
	fillFramebufferColorDepth24Stencil8(fboEdgeDetection);
	// for sharpen edge
	fboSharpenEdge = new FBO();
	fillFramebufferColorDepth24Stencil8(fboSharpenEdge);
	// for blur
	fboBlur = new FBO();
	fillFramebufferColorDepth24Stencil8(fboBlur);
	// for gamma correction
	fboGammaCorrect = new FBO();
	fillFramebufferColorDepth24Stencil8(fboGammaCorrect);
	// for depth map - used for shadows
	fboDepthMap = new FBO();
	fillFramebufferDepthTexture2DAttachment(fboDepthMap);
	// for stopping pipeline
	fboPipelineStopper = new FBO();
	// for HDR
	fboHdr = new FBO();
	fillFramebufferFloatingPointColorDepthTexture2DAttachment(fboHdr);
	// multiple targets
	fboMultipleColorTargets = new FBO();
	fillFramebuffer2ColorDepthTexture2DAttachment(fboMultipleColorTargets);
	// FBO's for Gaussian Blur
	fboPingForGaussianBlur = new FBO();
	fillFramebufferColorOnlyTexture2DAttachment(fboPingForGaussianBlur);
	fboPongForGaussianBlur = new FBO();
	fillFramebufferColorOnlyTexture2DAttachment(fboPongForGaussianBlur);
	

	// create necessary shaders
	inversionFramebufferShader = new Shader("inversionFramebufferShader", "resources/shaders/vertexShaderFramebuffer.vs", "resources/shaders/fsInversion.glsl");
	twoDFramebufferShader = new Shader("twoDFramebufferShader", "resources/shaders/vertexShaderFramebuffer.vs", "resources/shaders/fsModelLoading.glsl");
	greyscaleFramebufferShader = new Shader("greyscaleFramebufferShader", "resources/shaders/vertexShaderFramebuffer.vs", "resources/shaders/fsGreyscale.glsl");
	shaderEdgeDetection = new Shader("shaderEdgeDetection", "resources/shaders/vertexShaderFramebuffer.vs", "resources/shaders/fsEdgeDetection.glsl");
	shaderSharpenEdge = new Shader("shaderEdgeDetection", "resources/shaders/vertexShaderFramebuffer.vs", "resources/shaders/fsSharpenEdge.glsl");
	shaderBlur = new Shader("shaderBlur", "resources/shaders/vertexShaderFramebuffer.vs", "resources/shaders/fsBlur.glsl");
	shaderGammaCorrect = new Shader("shaderGammaCorrect", "resources/shaders/vertexShaderFramebuffer.vs", "resources/shaders/fsGammaCorrect.glsl");
	shaderDebugQuadDepth = new Shader("twoDFramebufferShader", "resources/shaders/vsDebugQuadDepth.glsl", "resources/shaders/fsDebugQuadDepth.glsl");
	
	// initializes world
	worldRenderer->initialize();
}

void RenderMaster::renderScene(Camera* camera)
{
	// render scene
	// shadow mapping renderer
	//worldRenderer->renderWorld(camera, fboPipelineStopper, fboDepthMap);
	// scene renderer:
	worldRenderer->renderWorld(camera, fboMultipleColorTargets);
	// scene renderer: HDR buffer
	//worldRenderer->renderWorld(camera, fboHdr);
	// greyscale an image
	//worldRenderer->renderWorld(greyscaleFramebufferShader, fboGreyscale, fbo->tboColorAttachment);
	// invert colors
	//worldRenderer->renderWorld(inversionFramebufferShader, fboInversion, fboGreyscale->tboColorAttachment);
	// edge detection
	//worldRenderer->renderWorld(shaderSharpenEdge, fboSharpenEdge, fbo->tboColorAttachment);
	// sharpen edge
	//worldRenderer->renderWorld(shaderEdgeDetection, fboEdgeDetection, fbo->tboColorAttachment);
	// blur
	//worldRenderer->renderWorld(shaderBlur, fboBlur, fbo->tboColorAttachment);
	// gamma correct image
	//worldRenderer->renderWorld(shaderGammaCorrect, fboGammaCorrect, fbo->tboColorAttachment);
	// render to quad
	//worldRenderer->renderWorld(twoDFramebufferShader,fboPipelineStopper, fboMultipleColorTargets->tboColorAttachment, true);
	// debug depth map
	//worldRenderer->renderWorld(shaderDebugQuadDepth, fboPipelineStopper, fboDepthMap->tboDepthAttachment);

	// for guassian blur
	worldRenderer->performTwoPassGuassianBlur(twoDFramebufferShader, fboPipelineStopper, fboPingForGaussianBlur, fboPongForGaussianBlur, fboMultipleColorTargets->tboColorAttachment, true);
	// render to screen quad
	//worldRenderer->renderWorld(twoDFramebufferShader, fboPipelineStopper, fboMultipleColorTargets->tboColorAttachment, true);
}

void RenderMaster::uninitialize()
{
	worldRenderer->uninitialize();


}

