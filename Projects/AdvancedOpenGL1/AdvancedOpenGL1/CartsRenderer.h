#pragma once

#include"Base.h"
#include"Camera.h"
#include"Shader.h"
#include"Model.h"
#include"CubemapRenderer.h"
#include"GeometryRenderer.h"

class CartsRenderer
{
public:
	CartsRenderer(glm::mat4 projMat);
	CartsRenderer(glm::mat4 projMat, GeometryRenderer* geometryRenderer);
	void initialize();
	void uninitialize();
	void renderScene(Camera* camera);
	void renderSceneWithWoodenFloor(Camera* camera);
private:
	GeometryRenderer* geometryRenderer;
	Shader* lightingShader;
	Shader* lampShader;
	Shader* modelShader;
	Shader* singleColorShader;
	Shader* shaderPointLighting;
	Shader* shaderPointLightingMRT;
	Shader* shaderLampMRT;
	Model* ourModel;
	Model* cubeModels;
	Model* groundCubeModels;
	Model* floorQuadModels;
	Model* grassQuadModels;
	Model* transperentWindowQuadModels;

	glm::mat4 projectionMatrix;
};