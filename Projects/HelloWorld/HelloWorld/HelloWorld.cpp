//Header Files
#include<stdio.h>
#include<glad\glad.h>
#include<GLFW\glfw3.h>

//Global Variables
FILE * fplogfile = NULL;
//Function Prototype Declaration
void framebuffer_size_callback(GLFWwindow* window, int width, int height);

void Display(void);

//
void Processinput(GLFWwindow* window);
//Entrypoint Function
int main()
{
	// opening file for loging
	int fopenresult  = fopen_s(&fplogfile, "log.txt", "w");
	if (fopenresult != 0)
	{
		return -1;
	}
	else
	{
		fprintf(fplogfile, "Log File Is Created Successfully \n");
	}

	glfwInit();
	fprintf(fplogfile, "GLFW initialized successfully \n");
	//OPENGL Version declaration
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR , 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	
	//WE ARE USING OPENGL CORE PROFILE VERSION
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	fprintf(fplogfile, "Using Opengl version core profile version 3.3 \n");

	
	GLFWwindow* window = glfwCreateWindow(800, 600, "Hello Window", NULL, NULL);
	if (window == NULL)
	{
		fprintf(fplogfile, "Window is not created successfully so exiting program \n");
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	glfwSetFramebufferSizeCallback(window , framebuffer_size_callback);
	//initializing glad
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		fprintf(fplogfile, "Failed to initialize GLAD \n");
		return -1;
	}


	glViewport(0, 0, 800, 600);


	//Game Loop
	while (!glfwWindowShouldClose(window))
	{
		Processinput(window);
		Display();
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	//closing file
	fprintf(fplogfile, "Closing Log File \n");
	fclose(fplogfile);
	glfwTerminate();
	return 0;
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
}

void Processinput(GLFWwindow* window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(window, true);
	}
}

void Display(void)
{
	glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);
}