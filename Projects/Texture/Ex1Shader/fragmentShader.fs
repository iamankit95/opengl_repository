#version 440 core


in vec3 outcolor;
in vec2 outtexcoord;
out vec4 FragColor;
uniform sampler2D u_sampler;

void main()
{
FragColor = texture(u_sampler , outtexcoord) * vec4(outcolor, 1.0);
};
